/**
  * @module controller/AIController
  */

define([
  "controller/clientController",
  "model/game",
  "model/tileBag",
  "shared/exception"
], function (ClientController, Game, TileBag, Exception) {

  // todo -duplicated from tilebag
  /**
   * Creates a random integer between 0 and max, inclusive
   */
  function randomInt(max) {
    return Math.floor(Math.random() * (max + 1));
  };

  /**
   * Local Controller.
   * Controller for an non-networked game (i.e. pass to play).
   */
  var AIController = ClientController.extend({
    init: function (game, view, key, delay) {
      this._super(game, view);
      this._key = key;
      this._player = game.player(key);
      this._delay = delay | 0;

      // todo - throw if no key
      
      if (this._shouldTakeAction()) {
        this._takeAIAction();
      }
    },

    // todo - kind of hacky.  return a bogus key if we don't recognize
    pkey: function () {
      return this._key;
    },

    onActionSucceeded: function (source, action) {
      this._super(source, action);

      if (this._shouldTakeAction()) {
        this._takeAIAction();
      }
    },

    /**
     * todo
     */
    _shouldTakeAction: function () {
      var game = this._game;

      var activeKey = game.currentPhase() === "resolving" ?
        game.resolvingPlayer().key() : game.currentPlayer().key();

      if (activeKey === this._key) {        
        return true;
      }

    },

    /**
     * todo
     */
    _takeAIAction: function () {      
      var self = this;
      setTimeout(function () {
        var game = self._game;
        var startPhase = game.currentPhase();
        switch (game.currentPhase()) {
          case "placing":            
            var tile = null;
            var nonEmptyTile = null;
            var legalTiles = [];

            _.each(self._player.tiles(), function (t) {
              if (t !== TileBag.EmptyMarker) {
                nonEmptyTile = t;
              }
              if (game.legalPlacement(t)) {
                legalTiles.push(t);
              }
            });

            if (nonEmptyTile === null) {
              tile = "None";
            } else if (legalTiles.length > 0) {
              //tile = legalTiles[randomInt(legalTiles.length - 1)];
              tile = legalTiles[0];              
            }                       

            if (tile !== null) {
              if (game.endGameReady()) {
                self.doEndGame();
              }

              self.doPlaceTile(tile);
            } else {
              self.doSwapTiles();
            }

            break;
          case "founding":
            // todo - randomize
            var index = -1;
            for (var i = 0; i < Game.HOTELS.length; i++) {
              if (game.chainSize(Game.HOTELS[i]) === 0) {
                index = i;
                break;
              }
            }

            if (index === -1) {
              throw new Exception("No valid hotels");
            }

            self.doFoundChain(Game.HOTELS[index]);
            break;
          case "buying":
            // todo
            self.doEndTurn();
            break;
          case "merging":
            // todo private accessor
            var buyer = self._game._board._conflict.buyers[0];
            var seller = _.last(self._game._board._conflict.sellers);
            self.doMerge(buyer, seller);
            break;
          case "resolving":
            var merge = self._game._merge; // todo - private accessor
            var stock = self._player.stockCount(merge.target);
            // todo - currently sell all.  make smarter
            self.doResolveMerge(0, stock, 0);
            break;
          case "gameOver":
            // do nothing
            break;
          default:
            console.log("NYI(" + self._player.name() + "): takeAIAction for " +
              game.currentPhase());
            break;
        }
      }, self._delay);
    }
  });

  return AIController;
});
