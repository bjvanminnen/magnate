// todo - comment what data is for each function

/**
  * @module controller/clientController
  */
define(['class', 'model/game', 'shared/exception'], function (Class, Game, Exception) {
  /**
   * Client Controller.
   * Base client side controller for all games, whether they be local (LocalController)
   * or networked (NetworkController).
   */
  var ClientController = Class.extend({
    init: function (game, view) {
      this._game = game;
      this._view = view;

      game.actionSucceeded.subscribe(this.onActionSucceeded, this);
      // todo - should this have any different behavior than the above?
      // todo - is this specific to network controller? 
      game.recreated.subscribe(this.onActionSucceeded, this);
      
      view.playerTileClicked.subscribe(this.onPlayerTileClicked, this);
      view.hotelClicked.subscribe(this.onHotelClicked, this);
      view.endGameClicked.subscribe(this.onEndGameClicked, this);
      view.swapClicked.subscribe(this.onSwapClicked, this);

      view.buyDialogCompleted.subscribe(this.onBuyDialogCompleted, this);
      view.mergeSelectDialogCompleted.subscribe(this.onMergeSelectDialogCompleted, this);
      view.mergeResolveDialogCompleted.subscribe(this.onMergeResolveDialogCompleted, this);
    },

    pkey: function () {
      throw new Exception("Abstract Method", "Must be implemented by descendant");
    },

    /**
     * Tell game to perform placeTile action
     */
    doPlaceTile: function (tile) {
      this._game.performAction(Game.placeAction(this.pkey(), tile));
    },

    /**
     * Tell game to perform buy action
     */
    doBuyStock: function (hotel) {
      this._game.performAction(Game.buyAction(this.pkey(), hotel));
    },

    /**
     * Tell game to perform found action
     */
    doFoundChain: function (hotel) {
      this._game.performAction(Game.foundAction(this.pkey(), hotel));
    },

    /**
     * Tell game to perform merge action
     */
    doMerge: function (buyer, seller) {
      this._game.performAction(Game.mergeAction(this.pkey(), buyer, seller));
    },

    /**
     * Tell game to perform resolve merge action
     */
    doResolveMerge: function (hold, sell, tradein) {
      this._game.performAction(Game.resolveMergeAction(this.pkey(), hold, sell, tradein));
    },

    /**
     * Tell game to perform swap action
     */
    doSwapTiles: function () {
      this._game.performAction(Game.swapAction(this.pkey()));
    },

    /**
     * Tell game to perform endTurn action
     */
    doEndTurn: function () {
      this._game.performAction(Game.endTurnAction(this.pkey()));
    },

    /**
     * Tell game to perform endGame action
     */
    doEndGame: function () {
      this._game.performAction(Game.endGameAction(this.pkey()));
    },

    /**
    * Listener for game's actionSucceeded
    *
    * Currently, we'll just update the view.
    */
    onActionSucceeded: function (source, action) {
      var view = this._view;
      var game = this._game;

      view.update(this.pkey());
      
      var expectedKey = game.currentPhase() === "resolving" ?
        game.resolvingPlayer().key() : game.currentPlayer().key();

      if (game.currentPhase() === "gameOver") {
        this._view.showGameOverDialog();
      }

      if (this.pkey() !== expectedKey) {
        return;
      }

      // If we're transitioning to buying show our purchase dialog
      if (action.verb !== "buy" && source.currentPhase() === "buying") {
        view.showPurchaseDialog();
      }

      // todo - this isnt quite precise enough, but will do for now
      // todo - unit test for this
      if (game.currentPhase() === "merging") {
        view.showMergeSelectDialog();
      }
        
      if (game.currentPhase() === "resolving") {
        view.showMergeResolveDialog();
      }

      if (game.currentPhase() === "placing" &&
        action.verb !== "endGame" && action.verb !== "swap" &&
        this._game.currentPlayer().key() === this.pkey()) {
        this._view.showEndTurnDialog();
      }

      if (action.verb === "endGame") {
        this._view.endingGame();
      }

      
    },

    /**
     * Listener for view's playerTileClicked
     */
    onPlayerTileClicked: function (src, data) {
      var game = this._game;

      if (data === undefined || data["tile"] === undefined) {
        // todo
        throw new Exception();
      }

      var tile = data["tile"];
      if (game.currentPhase() !== "placing" ||
        game.currentPlayer().key() !== this.pkey()) {
        console.log("player tile click ignored");
        return;
      }

      // todo - make sure i have unit tests for errors being thrown
      try {
        this.doPlaceTile(tile);
      } catch (exception) {
        console.log("doPlaceTile failed with exception: " + exception);
      }
    },

    /**
     * Listener for view's hotelClicked
     */
    onHotelClicked: function (src, data) {
      var game = this._game;

      // todo - test where wrong player clicks

      if (game.currentPhase() !== "founding") {
        console.log("hotel click ignore");
        return;
      }

      function hotelFromData(data) {
        // todo - validate input better?
          if (data === undefined || data["hotel"] === undefined) {
            //todo
            throw new Exception();
          }
          return data["hotel"];
      }

      // todo - in game, handle case where all hotels have been founded?
      //   also have a unit test
      try {
        this.doFoundChain(hotelFromData(data));
      } catch (exception) {
        console.log("doFoundChain failed with exception: " + exception);
      }
    },

    /**
     * Listener for view's endGameClicked
     */
    onEndGameClicked: function (src, data) {
      if (this._game.currentPlayer().key() !== this.pkey() ||
        !this._game.endGameReady()) {
        // todo - ultimately potentially want this disabled when you cant use
        // it, or give an error to the user when they try and cant
        console.log("end game click ignored");
        return;
      }
      
      this.doEndGame();
      console.log("doing end game");
    },

    /**
     * Listener for view's swapClicked
     */
    onSwapClicked: function (src, data) {
      this._game.performAction(Game.swapAction(this.pkey()));
    },

    /**
     * Listener for view's buyDialogCompleted
     */
    onBuyDialogCompleted: function (src, data) {
      var game = this._game;

      if (game.currentPhase() !== "buying") {
        console.log("end turn ignored");
        return;
      }

      var hotels = data ? data.hotels : undefined;
      for (var i = 0; hotels && i < hotels.length; i++) {
        var hotel = hotels[i];
        if (hotel) {
          this.doBuyStock(hotel);
        }
      }

      this.doEndTurn();
    },

    /**
     * Listener for view's mergeSelectDialogCompleted
     */
    onMergeSelectDialogCompleted: function (src, data) {
      this.doMerge(data.buyer, data.target);      
    },

    /**
     * Listener for view's mergeResolveDialogCompleted
     */
    onMergeResolveDialogCompleted: function (src, data) {
      this.doResolveMerge(data.hold, data.sell, data.tradein);
    }    
  });

  return ClientController;
});
