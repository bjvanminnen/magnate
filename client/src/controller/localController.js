/**
  * @module controller/localController
  */

define([
  "controller/clientController",
  "model/game"
], function (ClientController, Game) {
  /**
   * Local Controller.
   * Controller for an non-networked game (i.e. pass to play).
   */
  var LocalController = ClientController.extend({
    init: function (game, view, keys) {
      this._super(game, view);
      this._keys = keys;
    },

    // todo - kind of hacky.  return a bogus key if we don't recognize
    // todo - gets us into trouble when giving domView.update a pkey of nokey
    pkey: function () {
      var key;
      if (this._game.currentPhase() === "resolving") {
        key = this._game.resolvingPlayer().key();
      } else {
        key = this._game.currentPlayer().key();
      }

      if (this._keys && !_.contains(this._keys, key)) {
        key = "nokey";
      }
      return key;
    }
  });

  return LocalController;
});
