/**
  * @module controller/networkController
  */
define([
  "controller/clientController",
  "model/game",
  "shared/exception"
], function (ClientController, Game, Exception) {
  
  /**
   * Network Controller.
   * Controller for an networked game
   */
  var NetworkController = ClientController.extend({    

    /**
     * Create our controller.  Controller is specific to a particular player,
     * specified by pkey.
     */
    init: function (game, view, gameId, socket, pkey) {
      var self = this;

      this._super(game, view);
      this._gameId = gameId;
      this._socket = socket;
      this._pkey = pkey;

      // todo - if we do a full game query and end up with a game waiting for info,
      // we should throw

      if (pkey === undefined) {
        throw new Exception("InvalidArgument", "NetworkController requires a pkey");
      }

      socket.on("successful-action", function (gameId, action) {
        self._onServerActionSuccess(action);
      });
    },

    _onServerActionSuccess: function (action) {
      // todo - unit test this.  had a bug where this was the socket

      if (action.pkey !== this.pkey() || action.verb === "update") {
        console.log("nc performing " + action.verb);
        this._game.performAction(action);
      } else {
        console.log("nc ignoring already performed " + action.verb);
      }
    },

    pkey: function () {
      return this._pkey;
    },

    /**
    * Listener for game's actionSucceeded
    */
    onActionSucceeded: function (source, action) {
      this._super(source, action);
      
      // if we performed this action, inform the server (unless it's an update
      // in which case it originated from the server
      if (action.pkey === this.pkey() && action.verb !== "update") {
        this._socket.emit("perform-action", this._gameId, action);
      }
    }
  });

  return NetworkController;
});