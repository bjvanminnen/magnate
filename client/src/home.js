define(['text!templates/home.html'], function (html) {
  $("body").empty();
  $("body").append(html);

  require(["client/identity"]);

  $("#local-game").click(function () {
    require(["client/localGame"]);
  });

  $("#network-game").click(function () {
    require(["client/networkGame"]);
  });

  $("#instructions").click(function () {
    console.log("instruction");
  });
});