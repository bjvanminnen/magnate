define([
  "wl",
  "shared/notifier",
  "sha512",
  "testutils/debugIdentities"
], function (wl, Notifier, jsSHA, debugIdentities) {
  var debugIdentityList = [
    {name: "Bob", id: "00000000"},
    {name: "Mary", id: "11111111"},
    {name: "Abdullah", id: "22222222"},
    {name: "Michaelangelo", id: "33333333"},
    {name: "MrPoopyPants", id: "44444444"},
    {name: "Randy", id: "55555555"}
  ];

  // todo - share this with debuggame somehow
  // Read a page's GET URL variables and return them as an associative array.
  var urlvars = function ()
  {
    var vars = [], hash;
    var hashes = window.location.href.slice(
      window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  }();

  var user = null;

  var identity = {
    createSigninButton: createSigninButton,
    onLogin: new Notifier(this),
    onLogout: new Notifier(this), 
    loggedIn: function () {
      return user !== null;
    },
    getUser: function () {
      if (!identity.loggedIn()) {
        return null;
      }
      return user;
    }
  };

  var dbgIdIndex = urlvars.id || -1;
  if (dbgIdIndex !== -1) {
    console.log("debugIdentity: " + dbgIdIndex);
    onLogin();
  }

  // todo - think about WL cookie.  would like to avoid sending it to server if possible

  var APP_CLIENT_ID = "000000004010CB24";
  // todo - can we use a relpath?
  //var REDIRECT_URL = "http://lowcalhost.com:8080/client/callback.html";
  var href = window.location.href;
  var REDIRECT_URL = href.substring(0, href.lastIndexOf("/")) + "/callback.html";
  console.log("REDIRECT_URL = " + REDIRECT_URL);

  // todo - hack
  if (href.indexOf("ec2") === 7) {
    APP_CLIENT_ID = "000000004010C956";
  }
  
  
  // todo - rationalize whats need in W8
  WL.Event.subscribe("auth.login", onLogin);
  WL.Event.subscribe("auth.logout", onLogout);
  WL.init({
    client_id: APP_CLIENT_ID,
    redirect_uri: REDIRECT_URL,
    scope: "wl.signin",
    response_type: "token"
  });

  function onLogin (session) {
    if (dbgIdIndex >= 0) {
      var dbgIdent = debugIdentities.getIdentity(dbgIdIndex);
      logUserIn(dbgIdent.name, dbgIdent.id);
      return;
    }

    if (!session.error) {
      WL.api({
        path: "me",
        method: "GET"
      }).then(
        function (response) {
          logUserIn(response.first_name, response.id);          
        },
        function (responseFailure) {
          // todo: api failure
          console.log("responseFailure: " + responseFailure);
        }
      );
    } else {
      // todo - login failure
      console.log("login failure");
    }
  }

  function logUserIn(name, id) {
    var shaObj = new jsSHA(id, "TEXT");
    // could make this an hmac using the last name as the secret, but
    // then a change in the last name means a change in the pkey, with
    // no way for the user to recover the old one
    var hash = shaObj.getHash("SHA-512", "HEX");
    
    user = {
      name: name,
      id: id,
      pkey: hash
    };

    identity.onLogin.notify(user);
  }

  function onLogout () {
    identity.onLogout.notify();
  }

  function createSigninButton (parentId) {
    WL.ui({
      name: "signin",
      element: parentId
    });
  }

  return identity;
});
