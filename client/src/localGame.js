var NAMES = ["Bob", "Mary", "John", "Matt", "Jennifer", "Chris"];

var SEED = 99;
var NUM_PLAYERS = 4;
var DEFAULT_HUMAN = false;
var AI_TIME = 10;

require([  
  'model/game',
  'view/domview',
  'testutils/TestView',
  'controller/localController',
  'controller/aiController',
  'testlib/seedrandom',
  'text!templates/localLobby.html',
  'text!templates/game.html'
], function (Game, DomView, TestView, LocalController, AIController, seedrandom,
  lobbyHtml, gameHtml) {
  
  $("body").empty();
  $("body").append(lobbyHtml);

  // todo - name of player from WL
  var players = [];
  for (var i = 0; i < 6; i++) {
    players.push({
      name: NAMES[i],
      key: i, // todo: something more key like?
      human: DEFAULT_HUMAN,
      enabled: i < NUM_PLAYERS 
    });
  }

  function update() {
    $(".local-player").each(function (index) {
      $(this).toggle(players[index].enabled);
    });
    $("#add-player").toggle(!players[players.length - 1].enabled);

    $(".player-name").each(function (index) {    
      $(this).val(players[index].name);
    });

    $(".player-type").each(function (index) {
      $(this).text(players[index].human ? "Human" : "Computer");
    });
  }

  $("#start-game").click(function () {
    var enabled = _.where(players, {enabled: true});

    // todo - right place for this?
    // todo - way to get seed as a url var
    var seed = Math.floor(Math.random() * 1000);
    seed = SEED;
    Math.seedrandom(seed);
    console.log("SEED: " + seed);
    
    var game = new Game(_.map(enabled, function (player, index) {
      return {
        name: player.name,
        pkey: player.key 
      };
    }));

    $("body").empty();
    $("body").append(gameHtml);

    game.start();

    // todo - should i always have view creation in controller? probably
    var view = new DomView(game);

    // todo - restructure how views work (perhaps a base that dom/test extend
    var testView = new TestView(game);
      
    // todo: this is a hack so that i can access game globally for debugging
    // purposes.  it should be removed
    window.__Game = Game;
    window.__game = game;
    window.__view = view;
    
    var lc = new LocalController(game, view,
      _.pluck(_.where(enabled, {human: true}), "key"));

    _.each(_.pluck(_.where(enabled, {human: false}), "key"), function (key) {
      var ac = new AIController(game, testView, key, AI_TIME);
    });
  });

  $(".remove-player").each(function (index) {
    $(this).click(function() {      
      players[index].enabled = false;
      var thisPlayer = players[index];
      for (var i = index; i < players.length - 1; i++) {
        players[i] = players[i + 1];
      }
      players[players.length - 1] = thisPlayer;

      update();
    });
  });

  $("#add-player").click(function () {
    for (var i = 0; i < players.length; i++) {
      if (!players[i].enabled) {
        players[i].enabled = true;
        break;
      }
    }
    update();
  });

  $(".move-up").each(function (index) {
    $(this).click(function () { 
      if (index > 0) {
        var tmp = players[index - 1];
        players[index - 1] = players[index];
        players[index] = tmp;
        update();
      }
    });
  });

  $(".move-down").each(function (index) {
    $(this).click(function () {
      if (index + 1 < players.length) {
        var tmp = players[index + 1];
        players[index + 1] = players[index];
        players[index] = tmp;
        update();
      }
    });
  });

  $(".player-type").each(function (index) {
    $(this).click(function () {
      var currentType = players[index].type;
      players[index].human = !players[index].human;
      update();
    });
  });

  update();

  
});
