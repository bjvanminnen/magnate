require.config({
  baseUrl: "../.."
});

require(["requirePaths"], function () {
  // load some global libs
  require([
    'lib/lodash.min',
    'shared/utils'
  ], function () {

    require(["testutils/debugGame"], function (debugGame) {
      if (debugGame.created) {
        return;
      }

      // todo - could use async to make these a series
      require(["client/home"], function () {
        background_load_images(
          // todo - all images
          "ui-bg_inset-soft_25_000000_1x100.png"
        );
      });
    });
  });
});

function background_load_images(images) {
  var image_loader = function (path) {
    return function () {
      console.log("loading " + path);
      var img = new Image();
      img.src = path;
    }
  }
  
  for (var i = 0; i < arguments.length; i++) {
    setTimeout(image_loader("/client/css/images/" + arguments[i]), 1);
  }
}

function background_load_requirements(requirePaths) {
  var requirer = function (path) {
    return function () {
      console.log("preload: " + path);
      require([path]);
    }
  }
  for (var i = 0; i < arguments.length; i++) {
    setTimeout(requirer(arguments[i]), 1);
  }
}