// todo - somewhere we should send up client version

// and now the stuff we want by name
require([
  'model/game',
  'shared/notifier',
  'client/identity',
  'view/domview',
  'controller/networkController',
  'client/socket',
  'text!templates/networkLobby.html',
  'text!templates/game.html'
], function (Game, Notifier, identity, DomView, NetworkController,
  socket, lobbyHtml, gameHtml) {

  var view = {
    init: function () {
      var self = this;

      $("body").empty();  
      $("body").append(lobbyHtml);

      identity.createSigninButton("wl-signin");

      if (!identity.loggedIn()) {
        $(".requires-login").hide();
      }

      this.startClicked = new Notifier(this);
      this.createClicked = new Notifier(this);
      this.refreshClicked = new Notifier(this);
      this.playGameClicked = new Notifier(this);
      this.joinGameClicked = new Notifier(this);

      $("#create-game").click(function () {
        self.createClicked.notify();
      });

      $("#refresh-games").click(function () {
        self.refreshClicked.notify();
      });
    },

    loginUser: function (user) {
      $(".requires-login").show();
      $("#player-name").text(user.name);
    },

    startGame: function () {
      $("body").empty();
      $("body").append(gameHtml);
    },

    updateUserPendingGames: function (games) {
      this._userPendingGames = games;

      $("#user-games-pending").empty();
      for (var i = 0; i < games.length; i++) {
        var game = JSON.parse(games[i].data);
        var text = "Game: " + games[i].id + "\t" + "Players: " +
          game.players.length + " / " + game.totalPlayers;
        
        var div = $("<div>")
          .attr("id", games[i].id)
          .text(text);
        $("#user-games-pending").append(div);
      }
    },

    updateOtherPendingGames: function (games) {
      var self = this;
      this._otherPendingGames = games;

      $("#all-games-pending").empty();
      _.each(games, function (item) {
        var game = JSON.parse(item.data);
        var gameId = item.id;
        var text = "Game: " + gameId + "\t" + "Players: " +
          game.players.length + " / " + game.totalPlayers;
        
        var div = $("<div>")
          .attr("id", gameId)
          .text(text);
        var button = $("<button>")
          .text("Join")
          .click(function () {
            self.joinGameClicked.notify({id: gameId});
          });
        div.append(button);

        $("#all-games-pending").append(div);
      });
    },

    // todo - have this update when a game changes
    updateUserRunningGames: function (games) {
      var self = this;
      this._userRunningGames = games; // todo - do i need this?

      $("#user-games-running").empty();
      _.each(games, function (game) {
        var gameSummary = game.data;
        // do we want to count num turns to show here?
        var text = "Game: " + game.id + "\t" + "Current Player: " +
          gameSummary.currentPlayer;
        
        var div = $("<div>")
          .attr("id", game.id)
          .text(text);
        var button = $("<button>")
          .text("Play")
          .click(function () {
            self.playGameClicked.notify({id: game.id});
          });
        div.append(button);
        $("#user-games-running").append(div);
      });
    }
  };

  // todo - various actions should force refresh

  var controller = {
    init: function (view) {
      var self = this;
      this._view = view;

      view.startClicked.subscribe(function (src, data) {
        self.onPlayClick(src, {id: 0});
      });
      view.createClicked.subscribe(this.onCreateClick, this);
      view.refreshClicked.subscribe(this.onRefreshClick, this);
      view.playGameClicked.subscribe(this.onPlayClick, this);
      view.joinGameClicked.subscribe(this.onJoinGame, this);
      identity.onLogin.subscribe(this.onUserLogin, this);

      if (identity.loggedIn()) {
        this.onUserLogin(null, identity.getUser());
      }
    },

    onPlayClick: function (src, data) {
      this._view.startGame();
      var gameId = data.id;
      socket.emit("get-game", gameId, function (err, json) {
        if (err) {
          console.log("err in get-game: " + err);
          return;
        }

        var game = Game.createFromJSON(json);      
        var pkey = identity.getUser().pkey;
        var view = new DomView(game, pkey);
        var controller = new NetworkController(game, view, gameId, socket, pkey);

        // todo: this is a hack so that i can access game globally for debugging
        // purposes.  it should be removed      
        window.__game = controller._game;
        window.__view = controller._view;
        window.__controller = controller;
      });
    },

    onCreateClick: function (src, data) {
      var self = this;
      // todo: options
      socket.emit("create-game", {numPlayers: 3}, function (err, result) {
        if (!err) {
          self._getAllGames();
        }
      });
    },

    onUserLogin: function (src, user) {
      socket.emit("user-signin", user);
      this._view.loginUser(user);

      this._getAllGames();
    },

    onRefreshClick: function (src, data) {
      this._getAllGames();
    },

    _getAllGames: function () {
      socket.emit("get-pending-games", function (err, myGames, otherGames) {
        if (!err) {
          view.updateUserPendingGames(myGames);
          view.updateOtherPendingGames(otherGames);
        }
      });

      // todo - belongs elsewhere?
      socket.emit("get-running-games", function (err, games) {
        if (!err) {
          view.updateUserRunningGames(games);
        }
      });
    },

    onJoinGame: function (src, data) {
      var self = this;
      var gameId = data.id;
      socket.emit("join-game", gameId, function (err, results) {
        if (!err) {
          self._getAllGames();
        }
      });
    }

  };

  view.init();
  controller.init(view);
});
