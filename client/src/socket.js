define([
  'lib/socket.io',
  'shared/notifier'
], function (io, Notifier) {
  // todo - rename this file and make it accept socket.io or TestIO

  if (window.io) {
    window.io = undefined; // remove global
  }

  var SERVER = "ec2-54-226-3-136.compute-1.amazonaws.com:8080";  

  // todo : handle errors on initial connection
  var socket = io.connect(SERVER);
  var user;

  socket.on("reconnect", function () {
    if (user) {
      userLogin(user);
    }
  });

  socket.on("disconnect", function () {
    
  });

  function userLogin(user) {
    // todo - do i want this to have a callback?
    socket.emit("user-signin", user);
  }

  function on(event, fn) {    
    socket.on(event, function () {
      fn.apply(this, arguments);
    });
  }

  function emit(event) {
    if (event === "user-signin") {
      console.assert(arguments.length === 2,
        "unexpected num args for user-signin");
      user = arguments[1];
      userLogin(user);
    } else {
      socket.emit.apply(socket, arguments);
    }
  };
  
  return {
    on: on,
    emit: emit
  };

});