define(['class', 'model/game', 'shared/notifier', 'text!templates/buy.html'],
  function (Class, Game, Notifier, html) {  

  // todo - exception when we click button with no shares queued
  // todo - should show price of current purchase somewhere
  // todo - replace PM appropriately

  /**
  * @module view/BuyDialog
  */
  var BuyDialog = Class.extend({

    /**
     * Create our dialog.  This should be done exactly once for the view.  Each
     * time we want to display it, we will call .show().
     *
     * @param {Object} options Any options to override in the dialog
     */
    init: function (options) {
      var self = this;
      options = options || {};

      var dialogElement = $("#buyer-dialog", $(html));

      this.completePurchase = new Notifier(this);

      this._dialog = dialogElement;      

      // todo: not too sure i entirely like how i have this options override
      // only used right now for unit testing purposes

      this._dialog.dialog(_.extend({
        dialogClass: "no-close",
        autoOpen: false,
        closeOnEscape: false,
        open: function () {
          // todo - may want to think about what focus looks like for children
          $(this).parent().focus(); // set focus to the dialog container
        },
        close: function (){
          // jquery automatically focus's the button that opened the dialog.
          // i don't want that, since it will be a different tile at that point

          // todo - move this out of BuyDialog? right now it's the only element
          // we muck with not in the parent element
          $(".player-tile").blur();
        },
        buttons: [
          {
            text: "0",
            type: "button",
            "class": "purchased-stock stock-count",
            click: function () {
              self._onClickUndoBuy(0)
            }
          },
          {
            text: "0",
            type: "button",
            "class": "purchased-stock stock-count",
            click: function () {
              self._onClickUndoBuy(1)
            }
          },
          {
            text: "0",
            type: "button",
            "class": "purchased-stock stock-count",
            click: function () {
              self._onClickUndoBuy(2)
            }
          },
          {
            text: "Ok",
            type: "button",
            id: "buydialog-ok",
            click: function () {
              $(this).dialog("close");
              self.completePurchase.notify(self._purchases);
            }
          }
        ]
      }, options));

      $(".jqbutton").button();
      $(".stock-count").width(this.stockCountWidth());

      $(".purchase-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        $(this)
          .addClass(hotel)
          .click(function () {
            self._onClickBuy(hotel);
          });
      });

      // move buttons around
      var buttonset = this._dialog.siblings().find(".ui-dialog-buttonset");
      var tbl = $("<table>");
      var row =$("<tr>");      
      buttonset.children().each(function () {        
        $(this).detach();
        var col = $("<td>");
        col.append($(this));
        row.append(col);
      });
      tbl.append(row);
      buttonset.append(tbl);
    },

    /**
     * Handle the UI update for the dialog, given the state created/modified
     * in show/_onClickBuy/_onClickUndoBuy
     */
    update: function () {
      var self = this;

      var cantPurchase = true;

      var costs = $(".purchase-cost");

      $(".purchase-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        var info = self._hotelInfo[hotel];
        
        var disabled = (info.shares === 0 || info.size === 0 ||
          self._money < info.cost);
        $(this).button("option", "label", info.shares);
        $(this).button("option", "disabled", disabled);

        costs.eq(index).toggle(!disabled);
        if (!disabled) {
          cantPurchase = false;
          costs.eq(index).text("$" + info.cost);
        }
      });
      $("#buyer-dialog-warn").toggle(cantPurchase);

      $("#buyer-money").text("$" + this._money);

      $(".purchased-stock").each(function (index) {
        if (self._purchases[index]) {
          $(this)
            .removeClass(Game.HOTELS.toString().replace(/,/g, " "))
            .addClass(self._purchases[index])
            .css("background", "")
            .button("option", "label", "1");
            
        } else {
          $(this)
            .removeClass(Game.HOTELS.toString().replace(/,/g, " "))
            .css("background", "none")
            .button("option", "label", "0");
        }
      });
    },

    /**
     * Show the dialog.  Start by grabbing any state we need from the game,
     * and then call update.  Deals only with the state, leaving update to
     * deal with UI.  This should be the only place we access the model.
     *
     * @param {Game} game The game we want to grab state from
     */
    show: function (game) {
      this._money = game.currentPlayer().money();
      this._hotelInfo = {};
      
      for (var i = 0; i < Game.HOTELS.length; i++) {
        var hotel = Game.HOTELS[i];
        this._hotelInfo[hotel] = {
          cost: game.cost(hotel),
          shares: game._hotels[hotel].stock, // todo - private accessor
          size: game.chainSize(hotel)
        }
      }
      this._purchases = [null, null, null];      

      this.update();
      this._dialog.dialog("open");            
    },

    /**
     * Handle a click where we're trying to buy a stock.  Update the state
     * and call update, which will take care of the UI.
     */
    _onClickBuy: function (hotel) {
      // Find first empty slot and fill it
      for (var i = 0; i < this._purchases.length; i++) {
        if (this._purchases[i] === null) {
          this._purchases[i] = hotel;
          this._hotelInfo[hotel].shares -= 1;
          this._money -= this._hotelInfo[hotel].cost;
          this.update();
          return;
        }
      }
    },

    /**
     * Handle a click where we're trying to undo the pending purchase of a stock.
     * Update the state and call update, which will take care of the UI.
     */
    _onClickUndoBuy: function (purchaseIndex) {
      var hotel = this._purchases[purchaseIndex];      
      this._hotelInfo[hotel].shares += 1;
      this._money += this._hotelInfo[hotel].cost;
      this._purchases[purchaseIndex] = null;
      this.update();
      return;
    },

    /**
     * The width of a two digit stock count button.
     */
    stockCountWidth: function () {
      if (this._stockCountWidth === undefined) {
        var label = $(".hotel-button").eq(0).button("option", "label");
        $(".hotel-button").eq(0).button("option", "label", "00");
        this._stockCountWidth = $(".hotel-button").eq(0).width();
        $(".hotel-button").eq(0).button("option", "label", label);
      }
      return this._stockCountWidth;
    }
  });

  return BuyDialog;
});