// todo - ability to only show historical actions on the click of a button
//   instead of always at the beginning of turn
// todo - unit tests
// todo - should be able to move tiles around (and have it persist)
// todo - upon selecting a tile, it highlights it match on the board
// todo - in fact, in a private game shouldnt move to other tab

// todo - button allowing placement of "None" when appropriate

/**
  * @module view/domView
  */
define([
  'class',
  'model/game',
  'shared/notifier',
  'shared/exception',
  'model/player',
  'view/buy',
  'view/mergeSelect',
  'view/mergeResolve',
  'view/endTurn',
  'view/endGame'
], function (Class, Game, Notifier, Exception, Player, BuyDialog, MergeSelectDialog,
  MergeResolveDialog, EndTurnDialog, EndGameDialog) {
  
  // todo - better UI for merging tile.  right now it shows as orange and loses
  // its color upon mouseover
  var COLOR_CLASS = {
    "none": "CC-none",
    "unclaimed": "CC-unclaimed",
    "merging": "CC-merging"
  };
  COLOR_CLASS[Game.HOTELS[0]] = Game.HOTELS[0];
  COLOR_CLASS[Game.HOTELS[1]] = Game.HOTELS[1];
  COLOR_CLASS[Game.HOTELS[2]] = Game.HOTELS[2];
  COLOR_CLASS[Game.HOTELS[3]] = Game.HOTELS[3];
  COLOR_CLASS[Game.HOTELS[4]] = Game.HOTELS[4];
  COLOR_CLASS[Game.HOTELS[5]] = Game.HOTELS[5];
  COLOR_CLASS[Game.HOTELS[6]] = Game.HOTELS[6];

  // space separated list of color classes
  var ALL_COLORS = _.values(COLOR_CLASS).toString().replace(/,/g, " ");

  // todo - figure out what is taking so long to load
  var DomView = Class.extend({
    init: function (game, pkey) {      
      var self = this;

      // Notifiers
      this.gridTileClicked = new Notifier(this);
      this.playerTileClicked = new Notifier(this);
      this.hotelClicked = new Notifier(this);
      this.endGameClicked = new Notifier(this);
      this.swapClicked = new Notifier(this);

      this.buyDialogCompleted = new Notifier(this);
      this.mergeSelectDialogCompleted = new Notifier(this);
      this.mergeResolveDialogCompleted = new Notifier(this);
      this.endTurnDialogCompleted = new Notifier(this);
      
      
      this._game = game;
      this._domInit();

      this._cache = {
        pkey: null,
        tiles: []
      };

      // todo - unit test using currentPlayer key
      this.update(pkey || game.currentPlayer().key());
      $("#main_div").removeClass("ui-helper-hidden-accessible");
    },

    /**
     * Do all our initialization.  Should only be called once per game load.
     */
    _domInit: function () {
      var self = this;
      var game = self._game;

      var players = game.players();
      
      // buy dialog
      this._buyDialog = new BuyDialog();
      this._buyDialog.completePurchase.subscribe(function (src, data) {
        self.buyDialogCompleted.notify({hotels: data});
      });

      this._mergeSelectDialog = new MergeSelectDialog();
      this._mergeSelectDialog.completeMergeSelect.subscribe(function (src, data) {
        self.mergeSelectDialogCompleted.notify(data);
      });

      this._mergeResolveDialog = new MergeResolveDialog(null);
      this._mergeResolveDialog.completeMergeResolve.subscribe(function (src, data) {
        self.mergeResolveDialogCompleted.notify(data);
      });

      this._endTurnDialog = new EndTurnDialog(null);
      this._endTurnDialog.completeEndTurn.subscribe(function (src, data) {
        // todo - right place for this?
        $("#player-tiles").show();
        self.endTurnDialogCompleted.notify(data);
      });

      this._endGameDialog = new EndGameDialog(null);      

      // jquery ui buttons
      $(".jqbutton").button();

      // todo - not sure i like how keyboard works with this
      $("#player-tabs").tabs({
        beforeActivate: function (event, ui) {          
          var players = game.players();
          var currentPlayer = self._game.currentPlayer();
          var updated = false;
          // todo - i dont really like depending on names being the same here
          var tabName = ui.newTab.text();
          for (var i = 0; i < players.length; i++) {
            if (tabName === players[i].name()) {
              if (!players[i].hiddenAssets()) {
                self._updatePlayerInformation(players[i]);
                updated = true;
              }
            }
          }

          if (!updated) {
            event.preventDefault();
          }
        }
      });
      
      // set player names
      $(".player-tab").each(function (i) {
        if (i < players.length) {
          $(this).children('a').text(players[i].name());
        } else {
          $(this).hide();
        }
      });

      // todo - want click to do popup ultimately, instead of notify
      $(".hotel-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        // todo - private accessor
        var stock = self._game._hotels[hotel].stock
        $(this).button("option", "label", stock)
          .click(function () {
            self.hotelClicked.notify({
              hotel: Game.HOTELS[index]
            });
          })
          .addClass(Game.HOTELS[index]);
      });
     
      $(".stock-count").width(this.stockCountWidth());

      // Now the player tiles
      $(".player-tile").click(function () {
        self.playerTileClicked.notify({
          tile: $(this).text()
        });
        // trigger a mouseout so that when we update the tiles it doesn't think
        // we're still hovering
        $(this).trigger("mouseout")
      });

      // the grid
      $(".grid-tile").click(function () {
        self.gridTileClicked.notify({
          tile: $(this).text()
        });
      });
          
      // want all grid tiles to be the same size
      var maxWidth = _.max(_.map($(".grid-tile"),
        function (button) { return $(button).width(); }));
      $(".grid-tile").width(maxWidth);

      // player tiles should also be that size
      $(".player-tile").width(maxWidth);

      $("#end-game").click(function () {
        self.endGameClicked.notify({});
      });

      $("#swap-tiles").click(function () {
        self.swapClicked.notify({});
      });
    },

    /**
     * Redraw our UI, querying the game for necessary info.
     *
     * @param pkey Pkey of player for view
     */
    update: function (pkey) {
      var game = this._game;

      var player = _.find(game.players(), function (player) {
        return player.key() === pkey;
      });

      if (pkey === "nokey") {
        // todo - temp hack
        player = game.currentPlayer();
      }
      if (!player) {
        throw new Exception("InvalidArgument",
          "DomView.update called with unrecognized pkey");
      }
      
      // todo - disabling cache logic for now, as it has some bugs
      this._cache.tiles = player.tiles().slice()

      // cache tiles of player so that when they click a tile, we leave a blank
      // space rather than shifting left
      //if (pkey !== this._cache.pkey) {
      //  this._cache.pkey = pkey;
      //  this._cache.tiles = player.tiles().slice(); // copy tiles
      //} else {
      //  // todo - maybe a unit test for this, since ive had a few bugs
      //  var tiles = player.tiles();
      //  var j = 0;
      //  for (var i = 0; i < this._cache.tiles.length; i++) {
      //    if (this._cache.tiles[j] !== tiles[i]) {
      //      if (this._cache.tiles[j] === "") {
      //        this._cache.tiles[j] = tiles[i];
      //      } else {
      //        this._cache.tiles[j] = "";
      //      }
      //      j++;
      //    }
      //    j++;
      //  } 
      //}

      var playerTiles = this._cache.tiles;

      $(".player-tile").each(function(index) {
        if (index < playerTiles.length) {
          $(this).button("option", "label", playerTiles[index]).show();
          $(this).css("visibility", playerTiles[index] === "" ? "hidden" : "visible");
        } else {
          $(this).hide();
        }        
      });

      // todo - private accessor
      $("#swap-tiles").toggle(game._noTilesPlayable);

      $(".hotel-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        // todo - private accessor
        var stock = game._hotels[hotel].stock
        $(this).button("option", "label", stock);
        $(this).button("option", "disabled", game.chainSize(hotel) !== 0);
      });
      
      $(".grid-tile").each(function (index) {
        var tileState = game.tileState($(this).text());
        $(this)
          .removeClass(ALL_COLORS)
          .addClass(COLOR_CLASS[tileState]);
      });

      var currentPlayer = game.currentPlayer();
      $(".player-tab").each(function (index) {
        // todo - danger: name comparison (should use some sort of id?)
        if ($(this).children('a').text() === currentPlayer.name()) {
          $(this).addClass("ui-state-highlight"); // todo
          $("#player-tabs").tabs("option", "active", index);
        } else {
          $(this).removeClass("ui-state-highlight");
        }
      });

      this._updatePlayerInformation(currentPlayer);
    },

    _updatePlayerInformation: function (player) {
      $("#player-money").text("$" + player.money());

      $(".player-stock").each(function (index) {
        $(this).button("option", "label", player.stockCount(Game.HOTELS[index]))
          .addClass(Game.HOTELS[index]);
      });
    },

    /**
     * The width of a two digit stock count button.
     */
    stockCountWidth: function () {
      if (this._stockCountWidth === undefined) {
        var label = $(".hotel-button").eq(0).button("option", "label");
        $(".hotel-button").eq(0).button("option", "label", "00");
        this._stockCountWidth = $(".hotel-button").eq(0).width();
        $(".hotel-button").eq(0).button("option", "label", label);
      }
      return this._stockCountWidth;
    },

    /**
     * Dialog for player to purchase stock after placing a tile
     */
    showPurchaseDialog: function () {
      this._buyDialog.show(this._game);
    },

    /**
     * Dialog for player to choose what hotels to merge when needed
     */
    showMergeSelectDialog: function () {
      this._mergeSelectDialog.show(this._game);
    },

    /**
     * Dialog for player to choose resolve what to do with stock during a merge
     */
    showMergeResolveDialog: function () {
      this._mergeResolveDialog.show(this._game);
    },

    showEndTurnDialog: function () {
      // todo - also have to hide player info if private
      $("#player-tiles").hide();
      this._endTurnDialog.show(this._game);
    },

    showGameOverDialog: function () {
      this._endGameDialog.show(this._game);
    },

    endingGame: function () {
      // todo - make this better.  at the very least, dont allow change in size
      $("#end-game").button("option", "label", "Last Turn")
      $("#end-game").button("disable");
    }

    
  });

  return DomView;
});
