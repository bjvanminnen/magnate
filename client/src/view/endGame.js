// todo - unit test me

/**
  * @module view/endGame
  */
define([
  'class',
  'shared/notifier',
  'text!templates/endGame.html'
], function (Class, Notifier, html) {
  
  var EndGameDialog = Class.extend({

    /**
     * Create our dialog.  This should be done exactly once for the view.  Each
     * time we want to display it, we will call .show().
     *
     * @param {Object} options Any options to override in the dialog
     */
    init: function (options) {
      var self = this;
      options = options || {};

      var dialogElement = $("#end-game-dialog", $(html));

      this.completeEndTurn = new Notifier(this);

      this._dialog = dialogElement;      

      // todo: not too sure i entirely like how i have this options override
      // only used right now for unit testing purposes

      this._dialog.dialog(_.extend({
        dialogClass: "no-close",
        modal: true,
        autoOpen: false,
        closeOnEscape: false,
        maxHeight: $(window).height() * 2 / 3,
        width: "auto",
        open: function () {
          // todo - may want to think about what focus looks like for children
          $(this).parent().focus(); // set focus to the dialog container
        }
      }, options));

      $(".jqbutton").button();
    },    

    /**
     * todo - make sure comment is correct
     * Show the dialog.  Start by grabbing any state we need from the game,
     * and then call update.  Deals only with the state, leaving update to
     * deal with UI.  This should be the only place we access the model.
     *
     * @param {Game} game The game we want to grab state from
     */
    show: function (game) {
      this._dialog.dialog("option", "title", "Game Over");

      var contents = $("#end-game-dialog-contents");

      var playerMoney = _.sortBy(_.map(game.players(), function (player) {
        return { name: player.name(), money: player.money() };
      }), "money").reverse();

      contents.empty();
      _.each(playerMoney, function (val) {
        contents.append($("<div>").text(val.name + ": " + val.money));
      });
      

      this._dialog.dialog("open");
    },
  });

  return EndGameDialog;
});