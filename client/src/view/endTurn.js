// todo - unit test me
// todo - include merge, resolve in action log

/**
  * @module view/endTurn
  */
define([
  'class',
  'shared/notifier',
  'text!templates/endTurn.html'
], function (Class, Notifier, html) {
  
  var EndTurnDialog = Class.extend({

    /**
     * Create our dialog.  This should be done exactly once for the view.  Each
     * time we want to display it, we will call .show().
     *
     * @param {Object} options Any options to override in the dialog
     */
    init: function (options) {
      var self = this;
      options = options || {};

      var dialogElement = $("#end-turn-dialog", $(html));

      this.completeEndTurn = new Notifier(this);

      this._dialog = dialogElement;      

      // todo: not too sure i entirely like how i have this options override
      // only used right now for unit testing purposes

      this._dialog.dialog(_.extend({
        dialogClass: "no-close",
        modal: true,
        autoOpen: false,
        closeOnEscape: false,
        maxHeight: $(window).height() * 2 / 3,
        width: "auto",
        open: function () {
          // todo - may want to think about what focus looks like for children
          $(this).parent().focus(); // set focus to the dialog container
        },
        buttons: [
          {
            text: "Go!",            
            type: "button",
            id: "next-button",
            click: function () {
              $(this).dialog("close");
              self.completeEndTurn.notify();
            }
          }
        ]
      }, options));

      $(".jqbutton").button();
    },    

    /**
     * todo - make sure comment is correct
     * Show the dialog.  Start by grabbing any state we need from the game,
     * and then call update.  Deals only with the state, leaving update to
     * deal with UI.  This should be the only place we access the model.
     *
     * @param {Game} game The game we want to grab state from
     */
    show: function (game) {
      this._dialog.dialog("option", "title",
        game.currentPlayer().name());

      // clear contents
      var contents = $("#end-turn-dialog-contents");
      contents.empty();

      // todo - make sure this gets unit tested as it's semi-complex
      // group our historical actions
      var pairs = game.historicalActions();
      var grouped = [];
      var lastVerb, lastPlayer;
      for (var i = 0; i < pairs.length; i++) {
        var action = pairs[i].action;
        var player = pairs[i].player;

        if (action.verb === lastVerb && player === lastPlayer) {
          grouped[grouped.length - 1].argList.push(action.arguments);
        } else if (action.verb !== "endTurn" && action.verb !== "update") {
          grouped.push({
            name: player,
            verb: action.verb,
            argList: [action.arguments]
          });
        }

        lastVerb = action.verb;
        lastPlayer = player;
      }

      // how create our html elements
      for (var i = 0; i < grouped.length; i++) {
        var group = grouped[i];
        contents.append(this.actionElement(group.name, group.verb, group.argList));
      }

      this._dialog.dialog("open");
    },

    /**
     *
     * @param {string} player
     * @param {string} verb
     * @param {obj[]} argList
     * @returns div element
     */
    actionElement: function (player, verb, argList) {
      var element = $("<li>");
      // todo: finish
      switch (verb) {
        case "place":
          element.text(player + " placed " + argList[0].tile);
          break;
        case "found":
          element.text(player + " founded " + argList[0].hotel);
          break;
        case "buy":
          var text = player + " bought ";
          var hotels = _.countBy(argList, "hotel");
          _.each(_.keys(hotels), function (hotel) {
            text += hotels[hotel] + " " + hotel + " ";
          });
          element.text(text);
          break;
        case "swap":
          element.text(player + " swapped tiles.");
          break;
        default:
          element.text(player + "|" + verb + "|" + argList);
          break;
      }

      return element;
    }


  });

  return EndTurnDialog;
});