// todo - unit test me
// todo - should show majority/minority owners and payouts
// todo - names instead of One Two
// todo - show something indicating we're waiting on other player resolves

/**
  * @module view/mergeResolve
  */
define([
  'class',
  'model/game',
  'shared/notifier',
  'shared/exception',
  'text!templates/mergeResolve.html'
], function (Class, Game, Notifier, Exception, html) {
  
  var MergeResolveDialog = Class.extend({

    /**
     * Create our dialog.  This should be done exactly once for the view.  Each
     * time we want to display it, we will call .show().
     *
     * @param {Object} options Any options to override in the dialog
     */
    init: function (options) {
      var self = this;
      options = options || {};
      
      this._info = [];
      this._resolvingPlayerIndex = -1;
      this._stockPrice = 0;

      var dialogElement = $("#merge-resolve-dialog", $(html));

      this.completeMergeResolve = new Notifier(this);

      this._dialog = dialogElement;      

      // todo: not too sure i entirely like how i have this options override
      // only used right now for unit testing purposes

      this._dialog.dialog(_.extend({
        dialogClass: "no-close",
        autoOpen: false,
        width: "auto",
        closeOnEscape: false,
        open: function () {
          // todo - may want to think about what focus looks like for children
          $(this).parent().focus(); // set focus to the dialog container
        },
        buttons: [
          {
            text: "Resolve",
            type: "button",
            id: "merge-resolve-resolve",
            click: function () {
              $(this).dialog("close");
              self.completeMergeResolve.notify({
                hold: self._info[self._resolvingPlayerIndex].hold,
                sell: self._info[self._resolvingPlayerIndex].sell,
                tradein: self._info[self._resolvingPlayerIndex].tradein
              });
            }
          }
        ]
      }, options));

      $(".jqbutton").button();
      $(".stock-count").width(this.stockCountWidth());

      $(".hold-resolve").click(function () {
        var resolvingInfo = self._info[self._resolvingPlayerIndex];
        resolvingInfo.hold = resolvingInfo.hold + resolvingInfo.sell +
          resolvingInfo.tradein;
        resolvingInfo.sell = 0;
        resolvingInfo.tradein = 0;
        self.update();
      });

      $(".sell-resolve").click(function () {
        var resolvingInfo = self._info[self._resolvingPlayerIndex];
        if (resolvingInfo.hold > 0) {
          resolvingInfo.hold -= 1;
          resolvingInfo.sell += 1;
        } else if (resolvingInfo.tradein > 0) {
          resolvingInfo.tradein -= 2;
          resolvingInfo.hold += 1;
          resolvingInfo.sell += 1
        } else {
          return;
        }

        self.update();
      });

      $(".trade-in-resolve, .trade-out-resolve").click(function () {
        var resolvingInfo = self._info[self._resolvingPlayerIndex];
        if (resolvingInfo.hold >= 2) {
          resolvingInfo.hold -= 2;
          resolvingInfo.tradein += 2;
        } else if (resolvingInfo.sell >= 2) {
          resolvingInfo.sell -= 2;
          resolvingInfo.tradein += 2;
        }
        self.update();
      });
    },

    /**
     * todo - fix comment
     * Handle the UI update for the dialog, given the state created/modified
     * in show/_buyerClicked/_targetClicked
     */
    update: function () {
      var self = this;      

      $(".hold-resolve").each(function (index) {
        if (index >= self._info.length) {
          return;
        }
        $(this)
          .button("option", "label", self._info[index].hold)
          .button("option", "disabled", self._info[index].disabled);
      });

      $(".sell-resolve").each(function (index) {
        if (index >= self._info.length) {
          return;
        }
        $(this)
          .button("option", "label", self._info[index].sell)
          .button("option", "disabled", self._info[index].disabled);
      });

      $(".trade-in-resolve").each(function (index) {
        if (index >= self._info.length) {
          return;
        }
        $(this)
          .button("option", "label", self._info[index].tradein)
          .button("option", "disabled", self._info[index].disabled);
      });

      $(".trade-out-resolve").each(function (index) {
        if (index >= self._info.length) {
          return;
        }
        $(this)
          .button("option", "label", self._info[index].tradein / 2)
          .button("option", "disabled", self._info[index].disabled);
      });
      
      $(".money-resolve").each(function (index) {
        if (index >= self._info.length) {
          return;
        }
        
        $(this).text("$" + self._info[index].sell * self._stockPrice);
      });
    },

    /**
     * todo - make sure comment is correct
     * Show the dialog.  Start by grabbing any state we need from the game,
     * and then call update.  Deals only with the state, leaving update to
     * deal with UI.  This should be the only place we access the model.
     *
     * @param {Game} game The game we want to grab state from
     */
    // todo - we want to show what other players have done, so a query of current
    // stock count for each player is incorrect
    show: function (game) {
      var self = this;
      var players = game.players();

      // todo - private accessor
      var merge = game._merge;

      this._info = [];
      this._stockPrice = merge.payout.per;

      var currentPlayerIndex = -1;
      this._resolvingPlayerIndex = -1;
      for (var i = 0; i < players.length; i++) {
        // todo: private accessor
        var pinfo = merge.resolves[i];

        this._info.push({
          name: players[i].name(),
          hold: pinfo ? pinfo.hold : players[i].stockCount(merge.target),
          sell: pinfo ? pinfo.sell : 0,
          tradein: pinfo ? pinfo.tradein : 0,
          disabled: game.resolvingPlayer() !== players[i]
        });
        if (game.currentPlayer() === players[i]) {
          currentPlayerIndex = i;
        }
        if (game.resolvingPlayer() === players[i]) {
          this._resolvingPlayerIndex = i;
        }
      }
      if (currentPlayerIndex === -1) {
        throw new Exception("Unexpected", "current player doesnt match any player?");
      }
      if (this._resolvingPlayerIndex === -1) {
        throw new Exception("Unexpected", "resolving player doesnt match any player?");
      }

      // reorder info so that currentPlayer (who played the merge tile) is first
      this._info = _.flatten([this._info.slice(currentPlayerIndex),
        this._info.slice(0, currentPlayerIndex)]);
      this._resolvingPlayerIndex = (this._resolvingPlayerIndex - 
        currentPlayerIndex + players.length) % players.length;
      
      this._dialog.dialog("option", "title", "Resolve Merger: " +
        self._info[self._resolvingPlayerIndex].name);

      $("#buyer-resolve").addClass(merge.buyer);
      // todo - want the size from before the merge
      $("#buyer-resolve").button("option", "label",
        "-" + game.chainSize(merge.buyer) + "-");
      $("#target-resolve").addClass(merge.target);
      $("#target-resolve").button("option", "label",
        "-" + game.chainSize(merge.target) + "-");

      $(".resolve-row").each(function (index) {
        $(this).toggle(index < players.length);
      });

      $(".hold-resolve").addClass(merge.target);
      $(".sell-resolve").addClass(merge.target);
      $(".trade-in-resolve").addClass(merge.target);
      $(".trade-out-resolve").addClass(merge.buyer);

      $(".name-resolve").each(function (index) {
        if (index < self._info.length) {
          $(this).text(self._info[index].name);
        }
      });
      
      this.update();
      this._dialog.dialog("open");   
    },

    /**
     * The width of a two digit stock count button.
     */
    stockCountWidth: function () {
      if (this._stockCountWidth === undefined) {
        var label = $(".hotel-button").eq(0).button("option", "label");
        $(".hotel-button").eq(0).button("option", "label", "00");
        this._stockCountWidth = $(".hotel-button").eq(0).width();
        $(".hotel-button").eq(0).button("option", "label", label);
      }
      return this._stockCountWidth;
    }
    
  });

  return MergeResolveDialog;
});