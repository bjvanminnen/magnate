// todo - unit test me
// todo - dialog shows all stock rather than all stock involved in conflict

/**
  * @module view/mergeSelect
  */
define([
  'class',
  'model/game',
  'shared/notifier',
  'text!templates/mergeSelect.html'
], function (Class, Game, Notifier, html) {
  
  var MergeSelectDialog = Class.extend({

    /**
     * Create our dialog.  This should be done exactly once for the view.  Each
     * time we want to display it, we will call .show().
     *
     * @param {Object} options Any options to override in the dialog
     */
    init: function (options) {
      var self = this;
      options = options || {};

      var dialogElement = $("#merge-select-dialog", $(html));

      this.completeMergeSelect = new Notifier(this);

      this._dialog = dialogElement;      

      // todo: not too sure i entirely like how i have this options override
      // only used right now for unit testing purposes

      this._dialog.dialog(_.extend({
        dialogClass: "no-close",
        autoOpen: false,
        width: "auto",
        closeOnEscape: false,
        open: function () {
          // todo - may want to think about what focus looks like for children
          $(this).parent().focus(); // set focus to the dialog container
        },
        close: function (){
          // todo - get rid of
          console.log("close merge select");
        },
        buttons: [
          {
            text: "Merge",
            type: "button",
            id: "merge-select-merge",
            click: function () {
              $(this).dialog("close");
              self.completeMergeSelect.notify({
                buyer: Game.HOTELS[self._buyerIndex],
                target: Game.HOTELS[self._targetIndex]
              });
            }
          }
        ]
      }, options));

      $(".jqbutton").button();

      $(".buyer-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        $(this)
          .addClass(hotel)
          .click(function () {
            self._buyerClicked(index);
          });
      });

      $(".target-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        $(this)
          .addClass(hotel)
          .click(function () {
            self._targetClicked(index);
          });
      });

      this._buyerIndex = -1;
      this._targetIndex = -1;
      
      this._buyerSize = undefined;
      this._targetSize = undefined;
    },

    /**
     * Handle the UI update for the dialog, given the state created/modified
     * in show/_buyerClicked/_targetClicked
     */
    update: function () {
      var self = this;

      // todo - should much of this be in update instead of show since the only
      // things changing are merge button being disabled and buttons being
      // highlighted
      

      $(".buyer-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        var info = self._hotelInfo[hotel];
        $(this)
          .button("option", "label", "-" + info.size + "-")
          .button("option", "disabled", info.size !== self._buyerSize)
          .toggleClass("highlighted", index === self._buyerIndex);
      });

      $(".target-button").each(function (index) {
        var hotel = Game.HOTELS[index];
        var info = self._hotelInfo[hotel];
        $(this)
          .button("option", "label", "-" + info.size + "-")
          .button("option", "disabled", info.size !== self._targetSize)
          .toggleClass("highlighted", index === self._targetIndex);
      });

      $("#merge-select-merge").button("option", "disabled",
        self._buyerIndex === -1 || self._targetIndex === -1);
      
    },

    /**
     * todo - make sure comment is correct
     * Show the dialog.  Start by grabbing any state we need from the game,
     * and then call update.  Deals only with the state, leaving update to
     * deal with UI.  This should be the only place we access the model.
     *
     * @param {Game} game The game we want to grab state from
     */
    show: function (game) {
      console.log("show merge select");

      // todo - current seems to ignore whether or not hotel is actually
      // involved in the conflict
      this._hotelInfo = {};
      for (var i = 0; i < Game.HOTELS.length; i++) {
        var hotel = Game.HOTELS[i];
        this._hotelInfo[hotel] = {
          cost: game.cost(hotel),
          shares: game._hotels[hotel].stock, // todo - private accessor
          size: game.chainSize(hotel)
        }
      }

      this._buyerIndex = -1;
      this._sellerIndex = -1;

      // sizes sorted in descending
      var sizes = _.pluck(this._hotelInfo, "size").sort().reverse();
      this._buyerSize = sizes[0];
      this._targetSize = sizes[1];

      this.update();
      this._dialog.dialog("open");   
      
      // todo - hack alert.  is there a better way to accomplish this?
      // have to do this after open, else we dont yet have a valid width
      var width = 0;
      $(".buyer-button, .target-button").each(function (index) {
        if ($(this).button("option", "label").length === "-00-".length) {
          width = $(this).width();
        }
      });      
      if (width === 0) {
        var btn = $(".buyer-button").eq(0);
        var label = btn.button("option", "label");
        btn.button("option", "label", "-00-");
        width = btn.width();
        btn.button("option", "label", label);
      }
      
      $(".buyer-button, .target-button").width(width);

      // todo - better way to do this? trying to account for future highlighting
      // 8 is 2 times the difference between standard border (1px) and
      // highlighted border (5px);
      var rowHeight = $(".buyer-button").eq(0).parent().parent().height();
      $(".buyer-button").eq(0).parent().parent().height(rowHeight + 8);
      $(".target-button").eq(0).parent().parent().height(rowHeight + 8);
    },

    /**
     * todo
     */
    _buyerClicked: function (index) {
      this._buyerIndex = index;
      // todo - if only two options, toggle instead of disable?
      if (this._buyerIndex === this._targetIndex) {
        this._targetIndex = -1;
      }
      this.update();
    },

    /**
     * todo
     */
    _targetClicked: function (index) {
      this._targetIndex = index;
      if (this._buyerIndex === this._targetIndex) {
        this._buyerIndex = -1;
      }
      this.update();
    }
  });

  return MergeSelectDialog;
});