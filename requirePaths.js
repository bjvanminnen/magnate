// todo - have client also use this (currently only unit tests do)
var req = require;
if (typeof exports !== "undefined") {
  // node
  // todo - non hard coded path (thought just requirejs should work, but didnt)
  req = require(process.env.HOME + "/magnate/server/node_modules/requirejs");
}
  
req.config({
  paths: {
    text: "client/lib/text",
    model: "shared/model",
    view: "client/src/view",
    client: "client/src",
    templates: "client/templates",
    controller: "client/src/controller",
    lib: "client/lib",
    // todo - move unit testy things into own file?
    clientTest: "unittest/client/tests",
    serverTest: "unittest/server/tests",
    sharedTest: "unittest/shared/tests",
    testutils: "unittest/utils",
    testlib: "unittest/lib",
    "class": "shared/lib/class",
    shared: "shared",
    server: "server", // todo - do i really want this and shared? (which do nothing)
    // todo - the path for this was wrong - apparently no unit tests hit sha currently
    sha512: "shared/lib/sha512", // todo - does this really need its own path?
    wl: "//js.live.net/v5.0/wl.debug" // todo - non debug
  }
});
