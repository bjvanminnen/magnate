var express = require('express'),
  app = express(),
  server = require('http').createServer(app),
  io = require('socket.io').listen(server),
  requirejs = require('requirejs');

//todo : want to get rid of global underscore?
global._ = require('underscore');

requirejs.config({
  baseUrl: process.env.HOME + "/magnate" 
});
requirejs(process.env.HOME + "/magnate/requirePaths.js");

server.listen(8080);
console.log("Listening on 8080");
io.set("log level", 2);

requirejs([
  "server/src/MagnateServer",
  "server/src/GameStore"
], function (MagnateServer, GameStore) {
  new MagnateServer(io, GameStore);
});
