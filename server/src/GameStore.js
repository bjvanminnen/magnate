define([
  "class",
  "shared/notifier",
  "shared/exception"
], function (Class, Notifier, Exception) {

var mongo = require('mongodb');
var async = require('async');
var assert = require('assert');

var Server = mongo.Server;
var Db = mongo.Db;
var ObjectID = mongo.ObjectID;

var GameStore = Class.extend({
  init: function (db, collection) {
    var self = this;
    
    var server = new Server('localhost', 27017, {auto_reconnect: true});
    this._db = new Db(db, server, {w:1});
    assert.notEqual(this._db, undefined);

    this._collection = null;
    this.name = collection;

    this.dbOpen = new Notifier(this);
    
    this._db.open(function (err, db) {
      if (err) {
        throw new Exception("Fatal", "Failed to open db");
      }
      self._collection = db.collection(self.name);
      self.dbOpen.notify({collection: self._collection});
    });
  },

  _getCollection: function (callback) {
    var self = this;

    if (this._collection) {
      callback(null, this._collection);
      return;
    }
    
    this.dbOpen.subscribe(function (src, result) {
      callback(null, result.collection);
    });
  },

  /**
   * @returns {number} id via callback
   */
  addItem: function (type, data, callback) {
    var self = this;

    // todo - cleaner to allow objects and just stringify them here?
    if (_.isObject(data)) {
      throw new Exception("InvalidArgument", "addItem expects json");
    }

    if (!callback) {
      throw new Exception("InvalidArgument", "addItem requires callback");
    }

    async.waterfall([
      function (cb) {
        self._getCollection(cb);
      },
      function (collection, cb) {
        var doc = {
          _id: new ObjectID(),
          type: type,
          waterline: 1,
          data: data
        };
        collection.insert(doc, {w:1}, cb);
      },
    ], function (err, results) {
      if (err) {
        console.log(" err = " + err);
      }
      var id = (results && results.length === 1) ? results[0]._id : null;
      callback(err, id); 
    });
  },

  /**
   * 
   */
  // callback returns itemInfo
  getItemInfo: function (id, callback) {
    var self = this;

    if (!(id instanceof ObjectID)) {
      id = new ObjectID(id);
    }

    async.waterfall([
      function (cb) {
        self._getCollection(cb);
      },
      function (collection, cb) {
        collection.findOne({_id: id}, cb);
      },
      function (result, cb) {
        var err = null
        if (result === null) {
          err = "NoItem";
        }
        cb(err, result);
      }
    ], callback);
  },

  // callback returns numUpdated, <something else> if write concern is set i think
  updateItem: function (id, expectedWaterline, type, data, callback) {
    var self = this;
    
    if (_.isObject(data)) {
      throw new Exception("InvalidArgument", "updateItem expects json");
    }

    if (!(id instanceof ObjectID)) {
      id = new ObjectID(id);
    }

    async.waterfall([
      function (cb) {
        self._getCollection(cb);
      },
      function (collection, cb) {
        // todo - unit test where expected waterline doesnt match
        var update = {
          $inc: {waterline: 1},
          $set: {data: data}          
        };
        if (type) {
          update.$set.type = type;
        }
        // todo - validate multiple sets
        collection.update(
          {
            _id: id,
            waterline: expectedWaterline
          },
          update, cb);
      }
    ], callback);
  },

  /**
   * @returns [{id, waterline, type, data}]
   */
  getAllItems: function (callback) {
    var self = this;
    async.waterfall([
      function (cb) {
        self._getCollection(cb);
      },
      function (collection, cb) {
        collection.find({}).toArray(cb);
      }
    ], callback);
  },

  // debug purposes only
  _empty: function (callback) {
    var self = this;
    async.waterfall([
      function (cb) {
        self._getCollection(cb);
      },
      function (collection, cb) {
        collection.remove(cb);
      }
    ], callback);
  },

  ItemType: {
    Pending: "pending-game",
    Running: "running-game"
  }

});

return GameStore;
});
