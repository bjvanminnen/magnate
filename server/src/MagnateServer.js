define([
  "class",
  "model/game",
  "server/src/serverController",
  "server/src/userManager",  
  "testlib/async",
  "server/src/lobby"
], function (Class, Game, ServerController, userManager, async, lobby) {
  
  return function (io, Store) {
    var store = new Store("magnate", "games");
    lobby.attach(store);

    var controller = new ServerController(store);

    io.sockets.on("connection", function (socket) {
      console.log("connected");

      controller.addSocket(socket);

      // inject user as the first argument, which we get from the socket id
      // ignore the event if user hasn't logged in
      // todo - ignoring will give us problems where we have a callback
      function lobbyFn(event, fn) {
        socket.on(event, function () {
          var user = userManager.getUser(socket.id);
          if (!user) {
            console.log("create game ignored - no user");
            return;
          }

          console.log(event + ": " + user.name + "(" + socket.id + ")");
        
          fn.apply(null, [user].concat(Array.prototype.slice.call(arguments)));
        });
      }
    
      // todo - i think create-game should also have a callback
      lobbyFn("create-game", function (user, options, callback) {
        lobby.createPendingGame(user, options, callback);
      });
      lobbyFn("get-pending-games", function (user, callback) {
        lobby.getPendingGames(user, callback);
      });
      lobbyFn("get-running-games", function (user, callback) {
        lobby.getRunningGames(user, callback);
      });
      lobbyFn("join-game", function (user, gameId, callback) {
        lobby.joinGame(gameId, user, callback);
      });

    });
  }
});

