define([
  "model/game",
  "testlib/async", // todo - shouldnt be in testlib
  "shared/notifier"
], function (Game, async, Notifier) {

  var ItemType;

  var lobby = {
  };

  lobby.attach = function (store) {
    this._store = store;
    ItemType = store.ItemType;
  },

  // todo - rename to createPendingGame?
  lobby.createPendingGame = function (user, options, callback) {
    console.log("creating game");

    // todo - validate inputs and _store
    var callback = callback || function () { };

    var data = {
      players: [{
        name: user.name,
        pkey: user.pkey
      }],
      totalPlayers: options.numPlayers || 5,
      options: _.omit(options, "numPlayers")
    };

    this._store.addItem(ItemType.Pending, JSON.stringify(data), callback);
  };

  // callback returns two lists of pending games
  lobby.getPendingGames = function (user, callback) {
    var callback = callback || function () { };

    this._store.getAllItems(function (err, items) {
      if (err) {
        return callback(err, null);
      }

      var myGames = [];
      var otherGames = [];

      _.each(items, function (item) {
        if (item.type !== ItemType.Pending) {
          return;
        }

        if (playerInGame(user, JSON.parse(item.data))) {
          myGames.push({id: item._id, data: item.data});
        } else {
          otherGames.push({id: item._id, data: item.data});
        }
      });

      callback(null, myGames, otherGames);
    });
  },

  lobby.getRunningGames = function (user, callback) {
    var callback = callback || function () { };

    // todo - could do a more efficient filtered query
    this._store.getAllItems(function (err, items) {
      if (err) {
        return callback(err, null);
      }
      
      var games = [];
      _.each(items, function (item) {
        // todo - filter out games we're not in
        if (item.type !== ItemType.Running || !playerInGame(user, JSON.parse(item.data))) {
          return;
        }
        var game = Game.createFromJSON(item.data); 
        // todo - private accessor sorta
        games.push({
          id: item._id,
          data: {
            // todo - doesnt account for merge
            currentPlayer: game.currentPlayer().name()
          }
        });
      });

      callback(null, games);
    });
  },


  lobby.joinGame = function (gameId, user, callback) {
    var self = this;
    var callback = callback || function () { };
    // todo - eventually check that we havent reached max games here and in create

    var pendingGame;
    async.waterfall([
      function (cb) {
        self._store.getItemInfo(gameId, cb);
      },
      function (itemInfo, cb) {
        if (itemInfo.type !== ItemType.Pending) {
          return cb("game running", null);
        }
        
        pendingGame = JSON.parse(itemInfo.data);
        if (playerInGame(user, pendingGame)) {
          return cb("already in game", null);
        }

        if (pendingGame.players.length === pendingGame.totalPlayers) {
          return cb("game full", null);
        }

        pendingGame.players.push(user);
        if (pendingGame.players.length === pendingGame.totalPlayers) {
          console.log("run game");
          var game = new Game(pendingGame.players, pendingGame.options);
          game.start();
          self._store.updateItem(gameId, itemInfo.waterline, ItemType.Running,
            JSON.stringify(game), cb);
        } else {
          self._store.updateItem(gameId, itemInfo.waterline,
            null, JSON.stringify(pendingGame), cb);
        }
      }
    ], function (err, result) {
      callback(err);
    });
  };

  function playerInGame(user, game) {
    if (game.players) {
      return _.some(game.players, function (player) {
        return player.pkey === user.pkey;
      });
    } else {
      // todo - private accesor
      return _.some(game._players, function (player) {
        return player._key === user.pkey;
      });_
    }
  }
 
  return lobby;
});
