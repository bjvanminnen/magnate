define([
  "class",
  "model/game",
  "shared/notifier",
  "server/src/userManager"
], function (Class, Game, Notifier, userManager) {

  /**
   * todo
   */
  var controller = Class.extend({
    init: function (store) {
      this._store = store;
    },

    /**
     * todo
     */
    addSocket: function (socket) {
      var self = this;

      // todo - give this a callback?
      socket.on("user-signin", function (userInfo) {
        userManager.setUser(socket.id, userInfo);
      });
      socket.on("get-game", function (gameId, callback) {
        self._getGame(socket, gameId, callback);
      });

      // todo - give this a callback?
      socket.on("perform-action", function (gameId, action) {
        self._performAction(socket, gameId, action);
      });
    },

    /**
     * todo
     */
    _getGame: function (socket, gameId, callback) {
      console.log("on get game");
      var user = userManager.getUser(socket.id);
      if (!user) {
        callback("no user", null);
        return;
      }

      this._store.getItemInfo(gameId, function (err, itemInfo) {      
        if (err) {
          console.log("err = " + err);
          return;
        }

        if (itemInfo === null) {
          console.log("no game of id " + gameId);
          return;
        }
        
        var game = Game.createFromJSON(itemInfo.data);
      
        var userPlayer = _.find(game.players(), function (player) {
          return player.key() === user.pkey;
        })
        if (userPlayer === undefined) {
          callback("no user in game", null);
          return;
        }

        socket.join("game" + gameId); // todo - less hacky way of joining room?
      
        var hideAssets = true; // todo - should be configured per game

        if (callback) {        
          callback(null, JSON.stringify(game.privatized(user.pkey, hideAssets)));
        }
      });
    },

    /**/
    /**
     * todo
     */
    _performAction: function (socket, gameId, action) {
      var self = this;
      console.log("performing " + action.verb); 

      // todo - should i start using async instead of callback hell?
      this._store.getItemInfo(gameId, function (err, itemInfo) {
        console.log("got game info");
        var game = Game.createFromJSON(itemInfo.data);
        try {
          game.performAction(action);
          console.log("performed");
        } catch (exc) {
          console.log("exc: " + exc);
          self._reportError(socket, gameId, exc);
          return;
        }

        self._store.updateItem(gameId, itemInfo.waterline, null, JSON.stringify(game),
          function (err, numUpdated, item) {
            console.log("update callback");
            if (err) {
              self._reportError(socket, gameId, err);
              return;
            }

            self._reportSuccess(socket, gameId, action);

            self._respondWithUpdate(socket, gameId, game, action);
          }
        );
      });
    },

    /**
     * todo
     */
    _respondWithUpdate: function (socket, gameId, game, action) {
      switch (action.verb) {
        case "endTurn":
          var updateAction = Game.drawUpdateAction(action.pkey, game.lastDrawnTile());
          this._reportSuccess(socket, gameId, updateAction, false);

          if (game.currentPhase() === "gameOver") {
            var endGameUpdate = Game.endGameUpdateAction(action.pkey,
              _.map(game.players(), function (player) {
                return player.money();
              }));
            this._reportSuccess(socket, gameId, endGameUpdate);
          }
          break;
        case "merge":
          // todo - private accessor
          // todo - only need to do this if private, right?
          var updateAction = Game.mergeUpdateAction(action.pkey,
            game._merge.stockCounts, game._merge.playerIndices);
          this._reportSuccess(socket, gameId, updateAction);
          break;
        case "swap":
          var tiles = game.currentPlayer().tiles();
          var updateAction = Game.swapUpdateAction(action.pkey, tiles);
          this._reportSuccess(socket, gameId, updateAction, false);
          break;
      }
    },

    /**
     * Broadcast successful-action to client sockets involved in this game.
     * Default behavior is to broadcast to all clients, but can be told to
     * respond only to the client who performed the action via broadcast.
     *
     * @param {socket} socket Socket with client that sent us action
     * @param {number} gameId Id of the game
     * @param {action} action The action that was performed on the server.
     * @param {boolean} broadcast If false, responds only to originating client
     */
    _reportSuccess: function (socket, gameId, action, broadcast) {
      // todo - probably need to inform client what game id

      console.log("success: " + action.verb);

      // broadcast only hits other socks, we want to emit to ourself as well
      socket.emit("successful-action", gameId, action);

      if (broadcast === undefined || broadcast) {
        socket.broadcast.to("game" + gameId).emit("successful-action",
          gameId, Game.privatizedAction(action));
      }
    },

    /**
     * todo
     */
    // todo - not sure if this is exactly what this should look like, but it's a start
    _reportError: function (socket, gameId, err) {
      console.log("reporting error: " + err);
      socket.emit("server-error", gameId, err);
    }
  });

  return controller;
});
