// todo - unit tests
define([
], function () {
  var map = {};
  
  var manager = {};
  
  manager.setUser = function (uniqueId, userInfo) {
    // todo - validate inputs, including that hash matches
    // todo - remove items from map on disconnect?
    // todo - throw if already set?
    map[uniqueId] = userInfo;
    console.log("set user (pkey = " + userInfo.pkey + ")");
  },  

  manager.getUser = function (uniqueId) {
    if (!map[uniqueId]) {
      return null;
    }

    return map[uniqueId];
  }

  return manager;
});
