/**
  * @module Exception
  */

define(["shared/notifier"], function (Notifier) {
  /**
   * Exception class.  Takes a name and a message
   */
  function Exception (name, message) {
    this.name = name;
    this.message = message || "";

    Exception.onCreate.notify(this);
  }

  Exception.prototype.toString = function () {
    return this.name + ": " + this.message;
  };

  Exception.onCreate = new Notifier();

  return Exception;
});
