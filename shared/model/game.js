// big(ish) todos:
// todo - somewhere i need to check for no valid moves.  probably also need a
//   swap tiles action
// todo - account for special rules for two players?


/**
  * @module model/Game
  */
define([
  'class',
  'model/tilebag',
  'model/gameboard',
  'model/player',
  'shared/notifier',
  'shared/exception'
], function (Class, TileBag, GameBoard, Player,  Notifier, Exception) {
  "use strict";

  function throwOnUndefinedField(object, field) {
    if (object[field] === undefined) {
      throw new Exception("InvalidFormat",
        "throwOnUndefinedField: Expected field " + field);
    }
  }

  /**
   * Used to track changes to model format.
   */
  var MAJOR_VERSION = 1;
  var MINOR_VERSION = 0;

  var SAFE_SIZE = 11;

  var fromJSON = false;

  var Game = Class.extend({
    /**
     * Creates an instance of Game
     *
     * @constructor
     * @this {Game}
     * @param {Object[]} players A list of player name/pkey pairs for the game.
     * @param {Object} options Extendable options for the game
     // todo - as of right now, options arent being used at all
     */
    init: function (players, options) {
      this.actionSucceeded = new Notifier(this, "Game.actionSucceeded");
      this.recreated = new Notifier(this, "Game.recreated");

      if (fromJSON) {
        return;
      }
      
      if (players === undefined || players[0].name === undefined ||
        players[0].pkey === undefined) {
        throw new Exception("InvalidArgument",
          "Must specify list of player names/pkeys for Game");
      }

      // todo - throw if duplicated names

      _.extend(this._options, options);

      this._players = [];
      this._actionLog = [];
      this._tilebag = new TileBag();
      this._board = new GameBoard();

      this._hiddenAssets = false;

      this._hotels = {};
      this._hotels[Game.HOTELS[0]] =  { stock: 25 };
      this._hotels[Game.HOTELS[1]] =  { stock: 25 };
      this._hotels[Game.HOTELS[2]] =  { stock: 25 };
      this._hotels[Game.HOTELS[3]] =  { stock: 25 };
      this._hotels[Game.HOTELS[4]] =  { stock: 25 };
      this._hotels[Game.HOTELS[5]] =  { stock: 25 };
      this._hotels[Game.HOTELS[6]] =  { stock: 25 };

      for (var i = 0; i < players.length; i++) {
        var player = new Player(players[i].name, players[i].pkey);
        this._players.push(player);
      }

      this._merge = null;
      this._currentPlayerIndex = -1;
      this._phase = "none";
      this._stockPurchasedThisTurn = 0;
      this._lastDrawn = null;
      this._gameEnding = false;
      this._noTilesPlayable = false;

      // currently mostly just used for debug purposes
      this._turnNumber = 0;

      this._version = {
        major: MAJOR_VERSION,
        minor: MINOR_VERSION
      };
    },    

    /**
     * Starts the game.  Should only be called once
     * 
     * @returns nothing
     */
    start: function () {
      if (this._phase !== "none") {
        throw new Exception("InvalidOperation", "Game has already started");
      }

      for (var i = 0; i < this._players.length; i++) {
        for (var j = 0; j < 6; j++) {
          this._players[i].addTile(this._tilebag.getTile());
        }
      }

      this._currentPlayerIndex = 0;
      this._phase = "placing";
    },

    /**
     * Gets the size of the given hotel chain
     *
     * @param {String} hotel The chain we want the size of
     * @returns {Number} The chain size
     */
    chainSize: function (hotel) {
      if (this._hotels[hotel] === undefined) {
        throw new Exception("InvalidArgument", "Chain " + hotel + " doesn't exist");
      }
      return this._board.chainSize(hotel);
    },

    /**
     * Get at list of player objects.
     *
     * @returns {Player[]} list of player objects
     */
    //todo - unit test
    players: function () {
      return this._players.slice();
    },

    /**
     * todo: unit test and comment
     */
    player: function (key) {
      var ret = _.find(this._players, function (player) {
        return player.key() === key;
      });

      if (ret === undefined) {
        throw new Exception("InvalidArgument", "No Player for key " + key);
      }

      return ret;
    },

    /**
     * Transfers shares of stock from the bank to the player
     * 
     * @private
     * @param {string} hotel Hotel in which shares are being transferred
     * @param {Player} player Player getting the shares
     * @param {number} amount Number of shares to give to player.
     * @returns undefined
     */
    _givePlayerShares: function (hotel, player, amount) {
      if (amount > this._hotels[hotel].stock) {
        throw new Exception("InvalidOperation", "Don't have " + amount +
          "shares of " + hotel + " to give");
      }

      if (amount < 0) {
        throw new Exception("InvalidArgument", "can only give positive shares");
      }

      player.addStock(hotel, amount);
      this._hotels[hotel].stock -= amount;
    },

    /**
     * Transfers shares of stock from the player to the bank
     * 
     * @private
     * @param {string} hotel Hotel in which shares are being transferred
     * @param {Player} player Player giving the shares
     * @param {number} amount Number of shares to take from
     * @returns undefined
     */
    _takePlayerShares: function (hotel, player, amount) {
      if (amount < 0) {
        throw new Exception("InvalidArgument", "can only take positive shares");
      }
      
      if (!player.removeStock(hotel, amount)) {
        throw new Exception("InvalidOperation", "Player has insufficient shares");
      }
      this._hotels[hotel].stock += amount;
    },

    /**
     * Gets the current player
     * 
     * @returns {Player} The player whose turn it currently is
     */
    currentPlayer: function () {
      return this._players[this._currentPlayerIndex];
    },


    /**
     * Gets the player who is currently resolving a merge
     * 
     * @returns {Player} The player who is resolving
     */
    resolvingPlayer: function () {
      if (this.currentPhase() !== "resolving") {
        throw new Exception("InvalidOperation", "Not in resolving phase");
      }
      return this._players[this._merge.playerIndices[0]];
    },

    /**
     * The payout for the merge in progress
     * 
     * @returns {Player} The player who is resolving
     */
    mergePayout: function () {
      if (this.currentPhase() !== "resolving") {
        throw new Exception("InvalidOperation", "Not in resolving phase");
      }
      return this._merge.payout;
    },

    /**
     * Gets the current phase
     * Phases:
     * - placing: Waiting on user to place a tile
     * - founding: The tile placed may put the user in a phase where they
     *   must found a chain
     * - merging: The tile placed may put the user in a phase where they
     *   need to make a decision on the order in which chains are merged
     * - resolving: After doing a merge, players must make decisions about
     *   whether to sell/trade/keep stock.  We remain in the resolving
     *   phase while that happens.
     * - buying: The player is buying stock
     * - awaitingMerge: Waiting on server for merge information
     * - awaitingDraw: Waiting on server for draw tile
     * - awaitingSwap: Waiting on server for swap of all tiles
     * - awaitingEndGame: Waiting on server for end game info
     * - endingGame: in the process of ending the game
     * - gameOver: Game is over
     * 
     * @returns {String} The current phase
     */
    currentPhase: function () {
      return this._phase;
    },

    /**
     * todo comment and unit test
     */
    lastDrawnTile: function () {
      return this._lastDrawn;
    },

    /**
      * Returns the cost of a single share of the given hotel, based on the
      * current game state
      *
      * @param {string} hotel The hotel chain we're interested in.
      */ 
    cost: function (hotel) {
      // todo - unittests?
      // todo - could cache these if we find that we're querying too much
      var chainSize = this.chainSize(hotel);
      if (chainSize === 0) {
        return 0;
      }

      return Game.payout(hotel, chainSize).per;
    },

    /**
     * Gets the state of a given tile
     * tileStates of: "none", "unclaimed", "merging", <hotel>
     *
     * @param {String} tile The tile that we want to get the state of
     * @returns {String} the state of the tile
     */
    tileState: function (tile) {
      return this._board.tileState(tile);
    },

    /**
     * todo
     */
    historicalActions: function () {
      return this._actionLog;
    },    

    /**
     * Pays out majority and minority bonuses to players
     * todo - params
     * todo - unit test
     */
    _calculateBonuses: function (hotel, hotelSize, stockCounts) {
      if (this.currentPhase() !== "resolving" &&
        this.currentPhase() !== "endingGame") {
        throw new Exception("InvalidOperation",
          "Not in resolving/endingGame phase");
      }

      var payout = Game.payout(hotel, hotelSize);

      var bonus = {
        majority: {
          amount: 0,
          players: []
        },
        minority: {
          amount: 0,
          players: []
        }
      };

      // group players by how much stock they have
      var groupedPlayers = _.groupBy(this._players, function (player, index) {
        return stockCounts[index];
      }, this);      

      // get rid of 0
      delete groupedPlayers["0"];

      var highestCount = _.max(_.keys(groupedPlayers));
      if (!groupedPlayers[highestCount]) {
        return bonus;
      }

      bonus.majority.amount = payout.majority;
      bonus.majority.players = groupedPlayers[highestCount].slice();

      // if no minority, majority gets both
      if (groupedPlayers[highestCount].length > 1 ||
        _.keys(groupedPlayers).length === 1) {
          
        bonus.majority.amount += payout.minority;
        // if there's a tie, they split majority and minority
        bonus.majority.amount /= groupedPlayers[highestCount].length;
        // round up to nearest 100
        if (bonus.majority.amount % 100) {
          bonus.majority.amount += 100 - (bonus.majority.amount % 100);
        }
      }

      // no minority if there were multiple majority or only 1 stock holder
      if (bonus.majority.players.length > 1 ||
        _.keys(groupedPlayers).length === 1) {
        return bonus;
      }

      var secondHighest = _.keys(groupedPlayers).sort().reverse()[1];
      if (groupedPlayers[secondHighest]) {
        bonus.minority.amount = payout.minority;
        if (groupedPlayers[secondHighest].length > 1) {
          bonus.minority.amount /= groupedPlayers[secondHighest].length;
          if (bonus.minority.amount % 100) {
            bonus.minority.amount += 100 - (bonus.minority.amount % 100);
          }
        }

        bonus.minority.players = groupedPlayers[secondHighest].slice();
      }

      return bonus;
    },

    /**
     * todo: unit test
     */
    _payBonuses: function (bonusInfo) {
      _.each(bonusInfo.majority.players, function (player) {
        player.addMoney(bonusInfo.majority.amount);
      });
      _.each(bonusInfo.minority.players, function (player) {
        player.addMoney(bonusInfo.minority.amount);
      });
    },

    /**
      * Is this tile a legal placement
      * There are two scenarios when placing a tile is illegal       
      * (1) all chains are founded and placement would cause another founding
      * (2) placement would cause merger between two safe hotels     
      *
      * @param {string} tile The tile to be placed
      * todo - do i need to return why placement is illegal?
      * @returns {boolean} True if placement is legal, otherwise false
      */
    // todo - not entirely sure how i feel about some of this logic being
    // essentially duplicated here and in gameboard
    // todo - have controller query this
    legalPlacement: function (tile) {
      try {
        var board = this._board;
        var neighborStates = _.uniq(_.map(board.neighborsOf(tile),
          board.tileState, board));

        if (neighborStates.length === 1 && neighborStates[0] === "unclaimed") {
          var availableHotel = _.find(Game.HOTELS, function (hotel) {
            return this.chainSize(hotel) === 0;
          }, this);
          if (availableHotel === undefined) {
            return false;
          }
        }
      
        neighborStates = _.without(neighborStates, "unclaimed");
        if (neighborStates.length > 1) {
          var safeNeighbors = _.filter(neighborStates, function (hotel) {
            return this.chainSize(hotel) >= SAFE_SIZE;
          }, this);
          if (safeNeighbors.length >= 2) {
            return false
          }
        }
      } catch (exc) {
        return false;
      }

      return true;
    },

    /**
     * todo
     */
    endGameReady: function () {
      return this._board.endGameReady();
    },

    /**
     * Attemps to place the given tile on the game board.  Throws if unsuccessful.
     *
     * @param {string} tile The tile to be placed
     * @returns undefined
     */
    _placeTileInternal: function (tile) {
      var currentPlayer = this.currentPlayer();

      if (this._phase !== "placing") {
        throw new Exception("InvalidOperation", "Not in placing phase");
      }

      if (tile === "None") {
        if (!_.isEqual([TileBag.EmptyMarker], _.uniq(this.currentPlayer().tiles()))) {
          throw new Exception("InvalidArgument", "Can only refrain from " +
            "playing a tile if you have no tiles and the bag is empty");
        }

        // remove one of our empty tiles
        currentPlayer.removeTile(TileBag.EmptyMarker);
      } else {
        // Make sure we have the tile in question
        if (!currentPlayer.hiddenTiles() && !currentPlayer.hasTile(tile)) {
          throw new Exception("InvalidOperation",
            "Trying to play tile that player doesn't have (" + tile + ")");
        }

        // Try moving the tile onto the board
        currentPlayer.removeTile(tile);
        if (!this._board.placeTile(tile)) {
          currentPlayer.addTile(currentPlayer.hiddenTiles() ? Game.PrivateMarker : tile);
          throw new Exception("IllegalPlacement", "Tile (" + tile + ")  can't be placed there");
        }
      }

      // reset some state
      this._lastDrawn = null;
      this._stockPurchasedThisTurn = 0;

      // It's on the board now
      switch (this._board.state()) {
        case "founding":
          this._phase = "founding";
          break;
        case "merging":
          this._phase = "merging";
          break;
        case "ready":
          this._phase = "buying";
          break;
        default:
          throw new Exception("Unexpected", "Unknown board state");
      }
    },

    /**
     * Merges two hotel chains, with buyer acquiring seller. Throws if
     * unsuccessful.
     *
     * @param {string} buyer The hotel chain that is expanding
     * @param {string} target The hotel chain being acquired
     * @returns undefined
     */
    _mergeInternal: function (buyer, target) {
      if (this._phase !== "merging") {
        throw new Exception("InvalidOperation", "Not in resolving phase");
      }

      // cache relevant info
      var targetSize = this._board.chainSize(target)
      if (targetSize === 0) {
        throw new Exception("InvalidOperation",
          "Can't merge with unfounded target");
      }      

      // do the merge
      this._board.mergeChains(buyer, target);

      this._merge = {
        buyer: buyer,
        target: target,
        targetSize: targetSize,
        stockCounts: _.map(this._players, function (player) {
          return player.stockCount(target);
        }),
        playerIndices: [],
        resolves: []
      };

      if (this._hiddenAssets) {
        this._phase = "awaitingMerge";
      } else {
        // players that have stock will need to resolve merge
        for (var i = 0; i < this._players.length; i++) {
          // starting from current player
          var pindex = (this._currentPlayerIndex + i) % this._players.length;
          if (this._players[pindex].stockCount(target) > 0) {
            this._merge.playerIndices.push(pindex);
          }
        }

        this._phase = "resolving";

        // pay for majority/minority
        var bonus = this._calculateBonuses(this._merge.target, this._merge.targetSize,
          this._merge.stockCounts);
        this._payBonuses(bonus);        
      }
    },

    /**
     * After a merge happens, players owning shares in the acquired hotel chain
     * must each make a decision, in turn, about how many shares to hold, sell
     * or trade in.  This resolves a single player's decision.  Throws if
     * unsuccessful
     *
     * @param {Player} player Player resolving merge
     * @param {number} hold How many shares to hold
     * @param {number} sell How many shares to sell
     * @param {number} tradein How many shares to trade-in at a 2:1 rate
     * @returns undefined
     */
    _resolveMergeInternal: function (hold, sell, tradein) {      
      if (this.currentPhase() !== "resolving") {
        throw new Exception("InvalidOperation", "Not in resolving phase");
      }

      if (this._merge === null) {
        throw new Exception("Unexpected",
          "Should never have null merge in resolving phase");
      }

      var resolvingPlayer = this.resolvingPlayer();
      var buyer = this._merge.buyer;
      var seller = this._merge.target;
      var payout = Game.payout(this._merge.target, this._merge.targetSize);
      
      // todo - ultimately i'd like to perhaps store necessary info in _merge
      // so that we can still validate this (right now it's only validated if
      // we dont have hidden assets)
      if (!resolvingPlayer.hiddenAssets()) {
        if (hold + sell + tradein !== resolvingPlayer.stockCount(seller)) {
          throw new Exception("InvalidOperation",
            "Resolve params don't equal player stock " + hold + " + " + sell +
            " + " + tradein + " != " + resolvingPlayer.stockCount(seller));
        }

        if (hold < 0 || sell < 0 || tradein < 0) {
          throw new Exception("InvalidOperation",
            "Subzero param for resolveMerge");
        }

        if (tradein % 2 !== 0) {
          throw new Exception("InvalidOperation",
            "Only able to trade in even amounts of stock");
        }

        if (tradein / 2 > this._hotels[buyer].stock) {
          throw new Exception("InvalidOperation",
            "Insufficient amount of buyer's stock");
        }
      }

      // handle sell
      if (sell) {
        this._takePlayerShares(seller, resolvingPlayer, sell);
        resolvingPlayer.addMoney(sell * payout.per);
      }

      // handle tradein
      if (tradein) {
        this._takePlayerShares(seller, resolvingPlayer, tradein);
        this._givePlayerShares(buyer, resolvingPlayer, tradein / 2);
      }
      
      // cache resolve 
      this._merge.resolves[this._merge.playerIndices[0]] = {
        hold: hold,
        sell: sell,
        tradein: tradein
      };

      this._merge.playerIndices.splice(0, 1);

      if (this._merge.playerIndices.length === 0) {
        if (this._board.state() === "ready") {
          // we've fully resolved the conflict
          this._merge = null;
          this._phase = "buying";
        } else {
          // doing another merge
          this._phase = "merging";
        }
      }
    },   


    /**
     * Attempts to found the given hotel chain at the given tile.  THrows if
     * unsuccessful.
     *
     * @param {string} hotel The name of the hotel being founded
     * @param {tile} tile The tile where we're founding
     * @returns undefined
     */
    _foundChainInternal: function (hotel) {
      if (this._phase !== "founding") {
        throw new Exception("InvalidOperation", "Not in founding phase");
      }
      if (this._hotels[hotel] === undefined) {
        throw new Exception("BadArgument", "Hotel " + hotel + " doesn't exist");
      }
      
      this._board.foundChain(hotel);

      // Give founder one share
      if (this._hotels[hotel]["stock"] !== 0) {
        this._givePlayerShares(hotel, this.currentPlayer(), 1);
      }

      if (this._board.state() !== "ready") {
        throw new Exception("Unexpected", "Board state is " + board.state() +
        " after founding chain.");
      }
      this._phase = "buying";
    },

    /**
     * Buy a single stock in the given company.  Throws if unsuccessful.
     *
     * @param {string} hotel The name of the hotel being purchased
     * @returns {boolean} True if stock was purchased
     */
    _buyStockInternal: function (hotel) {
      var currentPlayer = this.currentPlayer();

      if (this._phase !== "buying") {
        throw new Exception("InvalidOperation", "Not in buying phase");
      }

      if (this._hotels[hotel] === undefined) {
        throw new Exception("BadArgument", "Hotel doesn't exist");
      }

      if (this._hotels[hotel]["stock"] === 0) {
        throw new Exception("InsufficientStock", "Stock is sold out");
      }
        
      if (this._stockPurchasedThisTurn >= 3) {
        throw new Exception("InsufficientPurchases", "Already purchased three stocks this turn");
      }

      var size = this._board.chainSize(hotel);
      if (size === 0) {
        throw new Exception("ChainInactive", "Can only buy stock in active chains");
      }

      var cost = Game.payout(hotel, size)["per"];
      if (currentPlayer.money() < cost) {
        throw new Exception("InsufficientFunds", "Player doesn't have enough money");
      }

      currentPlayer.subtractMoney(cost);
      this._givePlayerShares(hotel, currentPlayer, 1);

      this._stockPurchasedThisTurn++;
    },

    /**
     * todo: update comment
     *
     * @private
     * @returns undefined
     */
    _endTurnInternal: function () {
      var self = this;
      
      if (this._phase !== "buying") {
        throw new Exception("InvalidOperation",
          "Can only give tile during buying phase");
      }

      if (this._gameEnding) {
        if (this._hiddenAssets) {
          this._phase = "awaitingEndGame";
        } else {
          this._phase = "endingGame"; 
          this._finishGame();
          this._phase = "gameOver";
        }
        
        return;
      }

      this._lastDrawn = this._tilebag.getTile();
      // todo - handle edge case where we run out of tiles somewhere
      
      if (this._lastDrawn === Game.PrivateMarker && 
        !this.currentPlayer().hiddenAssets()) {
        // If tile is hidden, and player assets aren't, we need to wait and be
        // told what tile was
        this._phase = "awaitingDraw";
      } else {
        this.currentPlayer().addTile(this._lastDrawn);

        // move to next player
        this._currentPlayerIndex = ((this._currentPlayerIndex + 1) % this._players.length);               
        this._turnNumber++;

        this._checkForPlayableTiles();

        this._phase = "placing";
      }
    },

    /**
     * Checks whether current player has a playable tile, and sets
     * _noTilesPlayable appropriately.
     */
    _checkForPlayableTiles: function () {
      var self = this;
      // todo - prob needs to be publicly accesible
      this._noTilesPlayable = true;
      
      _.each(this.currentPlayer().tiles(), function (tile) {
        if (self._noTilesPlayable && self.legalPlacement(tile)) {
          self._noTilesPlayable = false;
        }
      });      
    },

    /**
     * Swaps out all of a players tiles
     */
    _swapTilesInternal: function () {
      var self = this;

      if (this.currentPhase() !== "placing" || !this._noTilesPlayable) {
        throw new Exception("InvalidOperation", "Can only swap if no tiles are " +
          "playable and game is in placing phase");
      }

      var player = this.currentPlayer();

      if (player.hiddenTiles()) {
        return;
      }

      // todo - what happens if we run out of tiles? i think i need to modify
      // tilebag to return empties (as well as harden code to deal with empties)
      _.each(player.tiles().slice(), function (tile) {
        player.removeTile(tile);
        player.addTile(self._tilebag.getTile());
      });           

      if (player.tiles()[0] === Game.PrivateMarker) {
        this._phase = "awaitingSwap";
      } else {
        // todo - unit test for case where we draw 6 more unplayable tiles.  also
        // for running out of tiles
        this._checkForPlayableTiles();
      }
    },

    /**
     * todo
     */
    _finishGame: function () {
      var self = this;
      if (this.currentPhase() !== "endingGame") {
        throw new Exception("InvalidOperation", "Not in endingGame phase");
      }

      // sell all hotels
      _.each(Game.HOTELS, function (hotel) {
        var chainSize = self.chainSize(hotel);
        var payout = Game.payout(hotel, chainSize);
        var stockCounts = _.map(self._players, function (player) {
          return player.stockCount(hotel);
        });
        var bonus = self._calculateBonuses(hotel, self.chainSize(hotel), stockCounts);
        self._payBonuses(bonus);

        _.each(self._players, function (player) {
          var numShares = player.stockCount(hotel);
          self._takePlayerShares(hotel, player, numShares);
          player.addMoney(numShares * payout.per);
        });
      });

      console.log("GAME OVER");

      // todo - rank players? if so, do it in the update handler as well

      // todo - at the least, we probably want to also show the breakdown of
      // how people earned money at the end (i.e. majority, minority, num stock)
    },

    /**
     * todo
     */
    _applyUpdateInternal: function (type, info) {
      switch (type) {
        case "mergeUpdate":
          this._applyMergeUpdateInternal(info);
          break;
        case "drawUpdate":
          this._applyDrawUpdateInternal(info);
          break;
        case "swapUpdate":
          this._applySwapUpdateInternal(info);
          break;
        case "endGameUpdate":
          this._applyEndGameUpdateInternal(info);
          break;
        default:
          throw new Exception("InvalidArgument", "Unknown update type " + type);
      }
    },

    /**
     * todo
     */
    _applyMergeUpdateInternal: function (info) {
      if (this._phase !== "awaitingMerge") {
        throw new Exception("InvalidOperation", "Not expecting merge update");
      }
      throwOnUndefinedField(info, "playerIndices");
      throwOnUndefinedField(info, "stockCounts");

      this._merge.stockCounts = info.stockCounts;
      this._merge.playerIndices = info.playerIndices;

      this._phase = "resolving";

      // pay for majority/minority
      var bonus = this._calculateBonuses(this._merge.target, this._merge.targetSize,
        this._merge.stockCounts);
      this._payBonuses(bonus);
    },

    /**
     * todo
     */
    _applyDrawUpdateInternal: function (info) {
      if (this._phase !== "awaitingDraw" && this._phase !== "awaitingEndGame") {
        throw new Exception("InvalidOperation", "Not expecting draw update");
      }
      throwOnUndefinedField(info, "drawnTile");
      
      this.currentPlayer().addTile(info.drawnTile);

      // todo - last two lines are identical to endTurn and could be shared?
      // move to next player
      if (!this._gameEnding) {
        this._currentPlayerIndex = ((this._currentPlayerIndex + 1) % this._players.length);
        this._turnNumber++;
        this._phase = "placing";
      }
    },

    /**
     * todo
     */
    _applySwapUpdateInternal: function (info) {
      if (this._phase !== "awaitingSwap") {
        throw new Exception("InvalidOperation", "Not expecting swap update");
      }
      throwOnUndefinedField(info, "tiles");

      var player = this.currentPlayer();            
      _.each(player.tiles(), function (tile) {
        player.removeTile(Game.PrivateMarker);        
      });
      _.each(info.tiles, function (tile) {
        player.addTile(tile);
      });

      this._phase = "placing";
    },

    /**
     * todo
     */
    // todo - unit test this (better?).  it was throwing in a couple places
    _applyEndGameUpdateInternal: function (info) {
      var self = this;

      if (this._phase !== "awaitingEndGame") {
        throw new Exception("InvalidOperation", "Not expecting end game update");
      }
      throwOnUndefinedField(info, "playerMoney");
      
      if (info.playerMoney.length !== this._players.length) {
        throw new Exception("InvalidArgument",
          "end game update hasn't provided info for correct number of players");
      }

      _.each(info.playerMoney, function (money, index) {
        // todo - private accessor
        self._players[index]._money = money;
      });

      // todo - get rid of play stock

      this._phase = "gameOver";
    },

    /**
     * todo
     */
    _handleEndGameInternal: function () {
      if (!this.endGameReady() ) {
        throw new Exception("InvalidOperation", "End game conditions not met");
      }

      this._gameEnding = true;
    },

    /**
      * Interprets actions to make changes to our model.  Each action will throw
      * an exception if we're unable to perform it.
      *
      * @param {string|Object} action The action in object or JSON form
      * @returns undefined
      */
    performAction: function (action) {
      if (typeof action === "string") {
        // todo- are there concerns in parsing JSON on server?
        action = JSON.parse(action);
      }

      throwOnUndefinedField(action, "pkey");
      throwOnUndefinedField(action, "verb");
      throwOnUndefinedField(action, "arguments");

      var playerExpected = action.verb === "resolveMerge" ?
        this.resolvingPlayer() : this.currentPlayer();

      if (playerExpected.key() !== action.pkey) {
        throw new Exception("ActionOutOfTurn",
          "Wrong player's turn for action " + JSON.stringify(action));
      }

      switch (action.verb) {
        case "place":
          throwOnUndefinedField(action.arguments, "tile");
          this._placeTileInternal(action.arguments.tile);
          break;
        case "found":
          throwOnUndefinedField(action.arguments, "hotel");
          this._foundChainInternal(action.arguments.hotel);
          break;
        case "buy":
          throwOnUndefinedField(action.arguments, "hotel");
          this._buyStockInternal(action.arguments.hotel);
          break;
        case "merge":
          throwOnUndefinedField(action.arguments, "buyer");
          throwOnUndefinedField(action.arguments, "seller");
          this._mergeInternal(action.arguments.buyer, action.arguments.seller);
          break;
        case "resolveMerge":
          throwOnUndefinedField(action.arguments, "hold");
          throwOnUndefinedField(action.arguments, "sell");
          throwOnUndefinedField(action.arguments, "tradein");
          this._resolveMergeInternal(action.arguments.hold,
            action.arguments.sell, action.arguments.tradein);
          break;
        case "swap":
          this._swapTilesInternal();
          break;
        case "endTurn":
          this._endTurnInternal();
          break;
        case "endGame":
          this._handleEndGameInternal();
          break;
        case "update":
          throwOnUndefinedField(action.arguments, "type");
          throwOnUndefinedField(action.arguments, "info");
          this._applyUpdateInternal(action.arguments.type, action.arguments.info);
          break;
        default:
          throw new Exception("BadArgument", "Invalid verb in " + JSON.stringify(action));
      }

      if (action.verb === "endTurn") {
        // after an endTurn action, currentPlayer is the next player.  we want to get
        // rid of all actions through their last one
        // todo - any reason i cant compare using pkey instead?
        var curName = this.currentPlayer().name();
        var lastDraw;
        for (var i = 0; i < this._actionLog.length; i++) {
          // todo - should i be using draw or giveTile?
          if (this._actionLog[i].action.verb === "endTurn" && 
            this._actionLog[i].player === curName) {
            lastDraw = i;
            break;
          }
        }
        if (lastDraw) {
          this._actionLog.splice(0, lastDraw + 1);
        }        
      }

      this._actionLog.push({
        player: playerExpected.name(),
        action: action
      });

      this.actionSucceeded.notify(action);
    },
    
    /**
     * Merge incoming json with an existing game.  Do this using our specialized
     * reviver, such that we recreate class objects.
     *
     * @param (string) json Json for the game we're merging with
     */
    applyJSON: function (json) {
      var obj = JSON.parse(json, createGameReviver(this));
      _.extend(this, obj);
      if (this.recreated) {
        this.recreated.notify();
      }
    },

    /**
     * Returns an equivalent object with any private data filtered out.
     * Specifically, replaces _lastDrawn, privatizes tilebag, and privatizes
     * all other players
     *
     * @memberOf module:model/Player#
     * @param {string} pkey The key of the player doing the privatizing (ie.
     *  the player we won't hide)
     * @param {boolean} hideAssets Whether player money/stock should be hidden
     * @returns {Object} An object structurally similar to a Game, but some
     *   fields hidden.
     */
    privatized: function (pkey, hideAssets) {
      var hidden = Game.PrivateMarker;

      // create a classless clone
      var privateGame = JSON.parse(JSON.stringify(this));

      // hide last drawn tile
      if (privateGame._lastDrawn !== null) {
        privateGame._lastDrawn = hidden;
      }
      
      // hide tilebag info
      privateGame._tilebag = this._tilebag.privatized(Game.PrivateMarker);

      // hide player info as necessary
      var players = this.players();
      for (var i = 0 ; i < players.length; i++) {
        if (players[i].key() !== pkey) {
          privateGame._players[i] = this._players[i].privatized(
            Game.PrivateMarker, hideAssets);
        }
      }

      privateGame._hiddenAssets = hideAssets;

      return privateGame;
    }
  });

  /**
   * Create a placeAction
   */
  Game.placeAction = function (pkey, tile) {
    return {
      "pkey": pkey,
      "verb": "place",
      "arguments": {
        "tile": tile
      }
    };
  };  

  /**
   * Create a foundAction
   */
  Game.foundAction = function (pkey, hotel) {
    return {
      "pkey": pkey,
      "verb": "found",
      "arguments": {
        "hotel": hotel
      }
    };
  };

  /**
   * Create a buyAction
   */
  Game.buyAction = function (pkey, hotel) {
    return {
      "pkey": pkey,
      "verb": "buy",
      "arguments": {
        "hotel": hotel
      }
    };
  };

  /**
    * Create a mergeAction
    */
  Game.mergeAction = function (pkey, buyer, seller) {
    return {
      "pkey": pkey,
      "verb": "merge",
      "arguments": {
        "buyer": buyer,
        "seller": seller
      }
    };
  };

  /**
   * Create a resolveMergeAction
   */
  Game.resolveMergeAction = function (pkey, hold, sell, tradein) {
    return {
      "pkey": pkey,
      "verb": "resolveMerge",
      "arguments": {
        "hold": hold,
        "sell": sell,
        "tradein": tradein
      }
    };
  };

  /**
   * Create a swap action
   */
  Game.swapAction = function (pkey) {
    return {
      "pkey": pkey,
      "verb": "swap",
      "arguments": {}
    };
  };

  /**
   * Create an endTurn action
   */
  Game.endTurnAction = function (pkey) {
    return {
      "pkey": pkey,
      "verb": "endTurn",
      "arguments": {}
    };
  };

  /**
   * Create an update action of type mergeUpdate
   */
  Game.mergeUpdateAction = function (pkey, stockCounts, indices) {
    return {
      "pkey": pkey,
      "verb": "update",
      "arguments": {
        "type": "mergeUpdate",
        "info": {
          "stockCounts": stockCounts,
          "playerIndices": indices
        }
      }
    };
  };

  /**
   * Create an update action of type drawUpdate
   */
  Game.drawUpdateAction = function (pkey, drawnTile) {
    return {
      "pkey": pkey,
      "verb": "update",
      "arguments": {
        "type": "drawUpdate",
        "info": {
          "drawnTile": drawnTile
        }
      }
    };
  };

   /**
   * Create an update action of type drawUpdate
   */
  Game.swapUpdateAction = function (pkey, tiles) {
    return {
      "pkey": pkey,
      "verb": "update",
      "arguments": {
        "type": "swapUpdate",
        "info": {
          "tiles": tiles
        }
      }
    };
  };

  /**
   * Create an update action of type endGameUpdate
   */
  Game.endGameUpdateAction = function (pkey, playerMoney) {
    return {
      "pkey": pkey,
      "verb": "update",
      "arguments": {
        "type": "endGameUpdate",
        "info": {
          "playerMoney": playerMoney
        }
      }
    };
  };


  /**
   * Create an endGame action
   */
  Game.endGameAction = function (pkey) {
    return {
      "pkey": pkey,
      "verb": "endGame",
      "arguments": {}
    };
  };

  /**
   * Wrap our reviver so that we have a closure with game
   *
   * @param {Game} game The game being revived
   */
  function createGameReviver (game) {
    /**
     * Reviver used to create a game model from its json. This is needed so that
     * our sub-models (TileBag, GameBoard, Player) end up as Class objects with
     * their prototype methods.
     *
     * @private
     * @param {string} key
     * @param {Object} value
     * @returns {Object}
    */
    return function reviver(key, value) {
      switch (key) {
        case "_tilebag":
          value = TileBag.createFromJSON(JSON.stringify(value));
          break;
        case "_board":
          value = GameBoard.createFromJSON(JSON.stringify(value));
          break;
        case "_players":
          for (var i = 0; i < value.length; i++) {
            value[i] = Player.createFromJSON(JSON.stringify(value[i]));
          }
          break;
      }
      return value;
    };
  };

  /**
   * Create a game model from it's json representation using gameReviver
   *
   * @param {string} json JSON for the model, created using JSON.stringify
   * @returns {Game}
   */
  Game.createFromJSON = function (json) {
    fromJSON = true;
    var game = new Game();
    fromJSON = false;
    game.applyJSON(json);

    // our created game will have entirely new notifiers, which means a cloned
    // game wont have listeners.
    game.actionSucceeded = new Notifier(game, "Game.actionSucceeded");
    game.recreated = new Notifier(game, "Game.recreated");

    return game;
  };

  /**
   * Returns an action that has filtered out any private info.
   *
   * @param {Action} action The action object to be privatized
   * @returns {Game}
   */
  // todo - unit test
  // todo - may not need this with next set of changes coming
  Game.privatizedAction = function (action) {
    var privateAction = JSON.parse(JSON.stringify(action));
    if (action.verb === "giveTile") {
      privateAction.arguments.tile = Game.PrivateMarker;
    }
    return privateAction;
  },

  Game.HOTELS = [
    "Western", "Holiday", "Sheraton", "Hyatt", "Fairmont", "Marriott", "Ritz"
  ];
  Game.HOTEL_PREMIUMS = {};
  Game.HOTEL_PREMIUMS[Game.HOTELS[0]] =  0;
  Game.HOTEL_PREMIUMS[Game.HOTELS[1]] =  0;
  Game.HOTEL_PREMIUMS[Game.HOTELS[2]] =  1;
  Game.HOTEL_PREMIUMS[Game.HOTELS[3]] =  1;
  Game.HOTEL_PREMIUMS[Game.HOTELS[4]] =  1;
  Game.HOTEL_PREMIUMS[Game.HOTELS[5]] =  2;
  Game.HOTEL_PREMIUMS[Game.HOTELS[6]] =  2;

  /**
   * Returns the per stock, minority and majority payouts for the given
   * chain, based on it's size on the board
   *
   * @static
   * @param {String} hotel The hotel chain we want the payout for
   * @param {Number} size The current size of the hotel chain
   * @returns {Object} Object literal with fields per, majority, and minority.
   */
  Game.payout = function (hotel, size) {
    if (size === undefined) {
      throw new Exception("Unexpected", "Calling payout with size undefined");
    }
      
    var perStock = 200 + Game.HOTEL_PREMIUMS[hotel] * 100;
    if (size === 0) {
      perStock = 0;
    } else if  (size <= 5) {
      perStock += 100 * (size - 2);
    } else if (size <= 10) {
      perStock += 400;
    } else if (size <= 20) {
      perStock += 500;
    } else if (size <= 30) {
      perStock += 600;
    } else if (size <= 40) {
      perStock += 700;
    } else {
      perStock += 800;
    }

    return { per: perStock, majority: perStock * 10, minority: perStock * 5 };
  };
  
  // todo: name these by color instead?
  // Ready To Rest Inn
  // Orange Dream Palace

  Game.PrivateMarker = "PM";

  return Game;
});
