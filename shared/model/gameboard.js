/**
  * @module model/GameBoard
  */
define(['class', 'shared/exception', 'shared/utils'], function (Class, Exception) {
  "use strict";

  var MAX_LETTER = 'I';
  var MAX_NUMBER = 12;
  var SAFE_SIZE = 11;
  var MAX_NUM_CHAINS = 7;

  var fromJSON = false;

  var GameBoard = Class.extend(
    /** @lends module:model/GameBoard */ {

    /**
     * Creates an instance of GameBoard
     *
     * @memberOf module:model/GameBoard#
     * @this {GameBoard}
     */
    init: function () {
      if (fromJSON) {
        return;
      }
      this._grid = {};
      this._chainCounts = {};
      this._boardState = "ready";
      this._conflict = null;

      // initialize our _grid  
      for (var letter = 'A'; letter <= MAX_LETTER; letter = letter.increment(1)) {
        for (var number = 1; number <= MAX_NUMBER; number++) {
          this._grid[number + letter] = "none";
        }
      }

      this._lastPlacedTile = null;
    },

    /**
     * Determines whether or not the given tile is within the bounds of the board
     * 
     * @memberOf module:model/GameBoard#
     * @param {String} tile The tile we are enquiring about
     * @returns {boolean} True if the tile is within the bounds, otherwise false
     */
    tileOnBoard: function (tile) {
      return this._grid[tile] !== undefined;
    },    

    /**
     * Gets the size of the given hotel chain
     *     
     * @memberOf module:model/GameBoard#
     * @param {String} hotel The chain we want the size of
     * @returns {Number} The chain size
     */
    chainSize: function (hotel) {
      return this._chainCounts[hotel] || 0;
    },


    /**
     * Gets the current state of the board
     * values are: "ready", "merging", "founding"
     *
     * @memberOf module:model/GameBoard#
     * @returns {String} The current state
     */
    state: function () {
      return this._boardState;
    },

    /**
     * Gets the state of a given tile
     * tileStates of: "none", "unclaimed", "merging", <hotel>
     *
     * @memberOf module:model/GameBoard#
     * @param {String} tile The tile that we want to get the state of
     * @returns {String} the state of the tile
     */
    tileState: function (tile) {
      if (!this.tileOnBoard(tile)) {
        throw new Exception("InvalidArgument", "Tile " + tile +
          " does not exist on board");
      }

      return this._grid[tile];
    },

    /**
     * Finds the neighbors of a given tile, whose tiles have been placed on the baord
     *
     * @memberOf module:model/GameBoard#
     * @param {String} tile The tile that we want the neighbors of
     */
    neighborsOf: function (tile) {
      if (!this.tileOnBoard(tile)) {
        throw new Exception("InvalidArgument", "Tile " + tile +
          " does not exist on board");
      }

      var number = parseInt(tile.slice(0, -1));
      var letter = tile.slice(-1);

      var neighbors = [];
      neighbors.push((number + 1) + letter);
      neighbors.push((number - 1) + letter);
      neighbors.push(number + letter.increment(1));
      neighbors.push(number + letter.increment(-1));

      function tileCheck(tileToCheck) {
        var state;
        if (!this.tileOnBoard(tileToCheck)) {
          return false;
        }
        state = this.tileState(tileToCheck);
        if (state === "none") {
          return false;
        }
        return true;
      }

      return _.filter(neighbors, tileCheck, this);
    },

    /**
     * Places the given tile on the board.  Returns false if the tile placement
     * is invalid
     * Can change state to merging or founding
     *
     * @memberOf module:model/GameBoard#
     * @param {String} tile The tile that we are placing
     * @returns {boolean} Whether or not the tile was placed
     */
    placeTile: function (tile) {
      switch (this._boardState) {
        case "ready":
          break;
        case "merging":
          throw new Exception("InvalidOperation", "Can't place tile while merging");
        case "founding":
          throw new Exception("InvalidOperation", "Can't place tile while founding");
        default:
          throw new Exception("Unexpected", "Unknown board state");
      }

      if (!this.tileOnBoard(tile)) {
        throw new Exception("InvalidArgument", "Tile  " + tile +
          " does not exist on board");
      }

      if (this.tileState(tile) !== "none") {
        throw new Exception("InvalidOperation", "Tile has already been placed");
      }

      var neighborStates = _.uniq(_.map(this.neighborsOf(tile), this.tileState, this));

      // two different things that can happen that potentially require user feedback
      // (1) founding a chain: a tile was placed next to an unclaimed tile, a chain
      //     must be founded if possible, otherwise it's an illegal move
      // (2) merging: a tile was placed that causes a merge where which chain acquires
      //     which is not clear and requires user input
      // Look here to see if we get into one of those states
      if (neighborStates.length === 0) {
        // tile is an island
        this._setTileState(tile, "unclaimed");
      }
      else if (neighborStates.length === 1 && neighborStates[0] === "unclaimed") {
        // placement would cause founding, but all hotels are founded
        // todo - were counting keys where chain count was 0. unit tests didnt catch
        if (_.filter(this._chainCounts, function (count ) {
          return count > 0;
        }).length >= MAX_NUM_CHAINS) {
          return false;
        } else {
          // move _boardState to founding, disallowing anything but founding to happen next
          this._setTileState(tile, "unclaimed");
          this._boardState = "founding";
        }
      }
      else {
        neighborStates = _.without(neighborStates, "unclaimed");
        if (neighborStates.length === 1) {
          // single neighbor chain, just add to chain
          this._addToChain(neighborStates[0], tile);
        } else {
          
          if (this._createConflict(neighborStates)) {
            this._setTileState(tile, "merging");
            this._boardState = "merging";
          } else {
            return false;
          }
        }
      }

      this._lastPlacedTile = tile;

      return true;
    },

    /**
     * Cache relevant conflict info given the set of hotels neighboring the
     * conflict tile.
     *
     * @private
     * @memberOf module:model/GameBoard#
     * @param {string[]} hotels The set of hotels neighboring our conflict tile
     * @returns {boolean} True if it's valid to create a conflict.
     */
    _createConflict: function (hotels) {
       // cache our conflict info
      this._conflict = {
        hotels: hotels,
        buyers: [],
        sellers: []
      };

      var sizes = [];
      var grouped = _.groupBy(hotels, function (hotel) {
        var size = this.chainSize(hotel);
        sizes.push(size);
        return size;
      }, this);

      var safe = function (size) {
        return size >= SAFE_SIZE;
      };
      if (_.filter(sizes, safe).length > 1) {
        return false;
      }

      // sort descending
      sizes = _.uniq(sizes).sort(function (a, b) { return b - a; });
      this._conflict.buyers = grouped[sizes[0]];
      if (this._conflict.buyers.length > 1) {
        this._conflict.sellers = this._conflict.buyers.slice();
      } else {
        this._conflict.sellers = grouped[sizes[1]];
      }
      return true;
    },

    /**
     * Merge two of our conflicted chains.
     * 
     * @memberOf module:model/GameBoard#
     * @param {string} buyer The chain doing the purchasing/growing
     * @param {string} seller The chain being purchased
     * @returns {boolean} True if successful merged successfully
     */
    mergeChains: function (buyer, seller) {
      if (this._boardState !== "merging") {
        throw new Exception("InvalidOperation",
          "Not in a state where we can merge chains");
      }

      if (buyer === seller) {
        throw new Exception("InvalidArgument",
          "merger buyer and seller cannot be the same");
      }

      var mergeTile = this._lastPlacedTile;
      if (this.tileState(mergeTile) !== "merging") {
        throw new Exception("Unexpected",
          "In merging state without last tile being a merge tile");
      }

      if (!_.contains(this._conflict.buyers, buyer)) {
        throw new Exception("InvalidOperation", "Not a valid buyer");
      }

      if (!_.contains(this._conflict.sellers, seller)) {
        throw new Exception("InvalidOperation", "Not a valid seller");
      }

      // flip the states
      _.each(_.keys(this._grid), function (tile) {
        if (this.tileState(tile) === seller) {
          this._setTileState(tile, buyer);
        }
      }, this);

      this._conflict.hotels = _.without(this._conflict.hotels, seller);
      if (this._conflict.hotels.length > 1) {
        this._createConflict(this._conflict.hotels);
      } else {
        // this will also take care of any unclaimed neighbors
        this._addToChain(buyer, mergeTile);
        this._boardState = "ready";
      }

      return true;
    },


    /**
     * Founds a hotel at the given tile
     *
     * @memberOf module:model/GameBoard#
     * @param {String} hotel The chain we want to found
     * @returns {boolean} True if chain was founded
     */
    foundChain: function (hotel) {
      if (this._boardState !== "founding") {
        throw new Exception("InvalidOperation",
          "Not in a state where we can found");
      }

      if (this.tileState(this._lastPlacedTile) !== "unclaimed") {
        throw new Exception("Unexpected",
          "Last placed tile is already part of a hotel chain");
      }

      if (this._chainCounts[hotel] !== undefined && this._chainCounts[hotel] !== 0) {
        throw new Exception("InvalidOperation", "Chain already exists");
      }

      this._addToChain(hotel, this._lastPlacedTile);
      this._boardState = "ready";

      return true;
    },

    /**
     * Flips the state of the given tile and any neighbors that are unclaimed
     * 
     * @private
     * @memberOf module:model/GameBoard#
     * @param {String} hotel The state/hotel we want to flip to
     * @param {String} tile The tile we want to flip (along with neighbors)
     * @returns undefined
     */
    _addToChain: function (hotel, tile) {
      this._setTileState(tile, hotel);      

      _.each(this.neighborsOf(tile), function (neighbor) {
        if (this.tileState(neighbor) === "unclaimed") {
          this._addToChain(hotel, neighbor);
        }
      }, this);
    },       

    /**
     * Sets the state of a given tile.  Updates chain counts as it does so
     * tileStates of: "none", "unclaimed", "merging", <hotel>
     *
     * @private
     * @memberOf module:model/GameBoard#
     * @param {String} tile The tile that we want to set the state of
     * @param {String} state The state we want to set
     * @returns undefined
     */
    _setTileState: function (tile, state) {
      // intentionally assume tile is on the board, as this is an internal method

      var oldState = this._grid[tile];
      this._grid[tile] = state;

      if (!_.contains(["none", "unclaimed", "merging"], oldState)) {
        this._chainCounts[oldState] = this._chainCounts[oldState] || 0;
        this._chainCounts[oldState] -= 1;
      }
      if (!_.contains(["none", "unclaimed", "merging"], state)) {
        this._chainCounts[state] = this._chainCounts[state] || 0;
        this._chainCounts[state] += 1;
      }
    },

    /**
     * Is the board in a state where the game can end.  This will be true if
     * we have any chain >= size 41, or if all founded chains are safe.
     */
    endGameReady: function () {
      var anyHotel = false;
      var unsafeHotel = false;
      var largeHotel = false;

      _.each(_.values(this._chainCounts), function (count) {
        if (count > 0) {
          anyHotel = true;

          if (count < SAFE_SIZE) {
            unsafeHotel = true;
          } else if (count >= 41) {
            largeHotel = true;
          }
        }
      });

      return largeHotel || (anyHotel && !unsafeHotel);
    }

  });

  /** 
    * Create a GameBoard object from it's json representation.
    *    
    * @func createFromJSON    
    * @memberOf module:model/GameBoard
    * @param {string} json The json to create from
    * @returns {GameBoard}
    */
  GameBoard.createFromJSON = function (json) {
    fromJSON = true;
    var board = new GameBoard(Class);
    fromJSON = false;
    var obj = JSON.parse(json);
    _.extend(board, obj);
    return board;
  };

  return GameBoard;
});
