/**
  * @module model/Player  
  */
define([  
  'class',
  'shared/exception'
], function (Class, Exception) {
  "use strict";

  var fromJSON = false;

  // Maximum number of tiles the player can hold at a given time
  var MAX_TILES = 6;

  // todo - singular place we can have this for here and Game?
  var PRIVATE = "PM";

  var Player = Class.extend(
    /** @lends module:model/Player */ {
    
    /**
     * Creates an instance of Player
     * Tracks a player and their assets
     *
     * @memberOf module:model/Player#
     * @param {String} name The player's name
     */
    init: function (name, pkey) {
      if (fromJSON) {
        return;
      }
      this._name = name;
      this._money = 6000;
      this._stocks = {};
      this._tiles = [];
      this._key = pkey;
    },    

    /**
     * @memberOf module:model/Player#
     * @returns {String} Player name
     */
    name: function () {
      return this._name;
    },

    /**
     * @memberOf module:model/Player#
     * @returns {String} Player key
     */
    key: function () {
      return this._key;
    },

    /**
     * @memberOf module:model/Player#
     * @returns {boolean} True if money/stock are hidden
     */
    hiddenAssets: function () {
      return this._money === PRIVATE;
    },

    /**
     * @memberOf module:model/Player#
     * @returns {boolean} True if tiles are hidden
     */
    hiddenTiles: function () {
      return this._tiles[0] === PRIVATE;
    },

    /**
     * @memberOf module:model/Player#
     * @returns {Number} Players money
     */
    money: function () {
      return this._money;
    },

    /**
     * @memberOf module:model/Player#
     * @param {Number} amount How much money to add
     */
    addMoney: function(amount) {
      if (this.hiddenAssets()) {
        return;
      }
      this._money += amount;
    },

    /**
     * @memberOf module:model/Player#
     * @param {Number} amount How much money to subtract
     */
    subtractMoney: function (amount) {
      if (this.hiddenAssets()) {
        return;
      }

      if (amount > this._money) {
        throw new Exception("InvalidOperation", "Player has insufficient funds");
      }
      this._money -= amount;
    },

    /**
     * Give the player stock in a hotel chain.
     *
     * @memberOf module:model/Player#
     * @param {String} stock The name of the stock being given
     * @param {Number} amount How many shares are given
     * @returns {boolean} True
     */
    addStock: function (stock, amount) {
      if (this.hiddenAssets()) {
        return true;
      }

      if (amount === undefined) {
        amount = 1;
      }

      if (this._stocks[stock] === undefined) {
        this._stocks[stock] = 0;
      }
      this._stocks[stock] += amount;
      return true;
    },

    /**
     * Takes the players stock in a hotel chain
     *
     * @memberOf module:model/Player#
     * @param {String} stock The name of the stock being taken
     * @param {Number} amount How many shares are taken
     * @returns {boolean} Whether stock was successfully taken
     */
    removeStock: function (stock, amount) {
      // todo - unit test for removing 0 of stock we dont have
      if (this.hiddenAssets() || amount === 0) {
        return true;
      }

      if (this._stocks[stock] === undefined || this._stocks[stock] < amount) {
        return false;
      }
      this._stocks[stock] -= amount;
      return true;
    },

    /**
     * Checks whether the player has the given tile
     *
     * @memberOf module:model/Player#
     * @param {String} tile Tile we're curious about
     * @returns {boolean} Whether player has tile
     */
    hasTile: function (tile) {
      return _.contains(this._tiles, tile);
    },

    /**
     * Gives the player a tile
     *
     * @memberOf module:model/Player#
     * @param {String} tile Tile to be given to player
     * @returns {boolean} Whether the tile was given
     */
    addTile: function (tile) {
      if (this._tiles.length === MAX_TILES) {
        throw new Exception("InvalidOperation", "Player already has MAX_TILES");
      }
      this._tiles.push(tile);
      return true;
    },

    /**
     * Removes a tile from the player
     *
     * @memberOf module:model/Player#
     * @param {String} tile Tile to be removed
     * @returns {boolean} Whether the tile was removed successfully
     */
    removeTile: function (tile) {
      var index = _.indexOf(this._tiles, tile);

      // if tiles are hidden, just remove last (hidden) tile 
      if (this.hiddenTiles()) {
        index = this._tiles.length - 1;
      }
      if (index === -1) {
        throw new Exception("InvalidOperation", "Can't remove tile as it doesnt exist");
      }
      this._tiles.splice(index, 1);
      return true;
    },

    /**
     * Gets a list of player's tiles
     *
     * @memberOf module:model/Player#
     * @returns {String[]} The player's tiles
     */
    tiles: function () {
      return this._tiles || [];
    },

    /**
     * How many shares of a given stock the player owns
     *
     * @memberOf module:model/Player#
     * @param {String} stock The stock we're enquiring about
     * @returns {Integer} How many shares the player owns
     */
    stockCount: function (stock) {
      return this._stocks[stock] || 0;
    },

    /**
     * Returns an equivalent object with any private data filtered out.
     * Specifically, replaces all tiles with privateMarker, as well as
     * money and stock if hideAssets is true
     *
     * @memberOf module:model/Player#
     * @param {string} privateMarker Marker used to indicate items have been hidden
     * @param {boolean} hideAssets Whether player money/stock should be hidden
     * @returns {Object} An object structurally similar to a Player, but some
     *   fields hidden.
     */
    privatized: function (privateMarker, hideAssets) {
      var privatePlayer = JSON.parse(JSON.stringify(this));
      _.each(privatePlayer._tiles, function (tile, index) {
        privatePlayer._tiles[index] = privateMarker;
      });

      if (hideAssets) {
        privatePlayer._money = privateMarker;
        privatePlayer._stocks = privateMarker;
      }

      return privatePlayer;
    }
  });

  /**
   * Create a Player object from its json representation.
   *
   * @memberOf module:model/Player
   * @param {string} json The json to create from
   * @returns {Player}
   */
  Player.createFromJSON = function (json) {
    fromJSON = true;
    var player = new Player();
    fromJSON = false;
    var obj = JSON.parse(json);
    _.extend(player, obj);
    return player;
  };

  return Player;
});