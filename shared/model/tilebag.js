/**
 * @module model/TileBag
 */
define(['class', 'shared/exception', 'shared/utils'], function (Class, Exception) {
  "use strict";

  /**
   * Creates a random integer between 0 and max, inclusive
   */
  function randomInt(max) {
    return Math.floor(Math.random() * (max + 1));
  };

  var fromJSON = false;

  var TileBag = Class.extend({
    
    /**
     * Creates an instance of TileBag
     * TileBag is just a collection of tile strings (i.e. "1A", "2A", ... "12I")
     * that we randomly draw from until we run out
     *
     * @memberOf module:model/TileBag#
     * @param {String} maxLetter Single character defining how many letters to use, default 'I'
     * @param {String} maxNumber Highest number, default 12
     */
    init: function (maxLetter, maxNumber) {
      if (fromJSON) {
        return;
      }

      this._tiles = [];

      // sanitize inputs
      maxLetter = maxLetter || 'I';
      maxNumber = maxNumber || 12;
      maxLetter = maxLetter.toUpperCase();

      // initialize _tiles
      for (var letter = 'A'; letter <= maxLetter; letter = letter.increment(1)) {
        for (var number = 1; number <= maxNumber; number++) {
          this._tiles.push(number + letter);
        }
      }
    },
    
    /**
     * Gets a random tile from the bag
     *
     * @memberOf module:model/TileBag#
     * @returns {String} random tile
     */
    getTile: function () {
      // todo - do i need to unit test this more thoroughly?
      if (this._tiles.length === 0) {
        return TileBag.EmptyMarker;
      }

      var i = randomInt(this._tiles.length - 1);
      return this._tiles.splice(i, 1)[0];
    },

    /**
     * Returns an equivalent object with any private data filtered out.
     * Specifically, replaces all tiles with privateMarker
     *
     * @memberOf module:model/TileBag#
     * @param {string} privateMarker Marker used to indicate items have been hidden
     * @returns {Object} An object structurally similar to a TileBag, but some
     *   fields hidden.
     */
    privatized: function (privateMarker) {
      var privateBag = JSON.parse(JSON.stringify(this));
      _.each(privateBag._tiles, function (tile, index) {
        privateBag._tiles[index] = privateMarker;
      });

      return privateBag;
    }
  });

  /**
   * Create a TileBag object from its json representation.
   *
   * @memberOf module:model/TileBag
   * @param {string} json The json to create from
   * @returns {TileBag}
   */
  TileBag.createFromJSON = function (json) {
    fromJSON = true;
    var tilebag = new TileBag();
    fromJSON = false;
    var obj = JSON.parse(json);
    _.extend(tilebag, obj);
    return tilebag;
  };

  TileBag.EmptyMarker = "MT";

  return TileBag;
});