/**
  * @module Notifier  
  */
define(function() {
  "use strict";

  var queue = [];

  /**
   * Custom pub/sub class. 
   *
   * @param {object} source The source of the notifier
   * @param {string} name Optional name for notifier, used for loggin
   */
  var Notifier = function(source, name) {    
    // Allow intantation without new keyword
    if (!(this instanceof Notifier)) {
      return new Notifier(source);
    }

    this._source = source
    this._listeners = [];
    this._name = name;
    this._verbose = false;
  };

  /**
   * Event has fired.  Call all listeners.  Currently done syncronously.
   *
   * @param {Object} properties Any data you want passed to the listeners
   */
  // todo - do i need listeners to be able to prioritize themselves?
  // todo - do i need a notifyOnce?
  Notifier.prototype.notify = function (properties) {
    if (this._verbose && this._name && this._listeners.length > 0) {
      console.log("Notifying ("  + this._listeners.length + "): " + this._name);
        
    }
    var self = this;
    _.each(self._listeners, function (listener) {
      queue.push(function () {
        listener.fn.call(listener.ctx, self._source, properties);
      });
    });

    while (queue.length > 0) {
      var fn = queue.splice(0, 1)[0];
      fn();
    }
  };

  /**
   * Add a listener to the event.
   *
   * @param {Object} listener Function to be called when notifier fires
   * @param {Object} context Context in which to call the listener.
   */
  Notifier.prototype.subscribe = function (listener, context) {
    if (this._verbose && this._name) {
      console.log("Add subscriber to: " + this._name);
    }
    this._listeners.push({fn: listener, ctx: context});
  };

  /**
   * Remove the given listener function from our list of subscribers.  If it
   * isn't currently a listener, this is a no-op.
   *
   * @param {Function} listenerFunction Listener function to remove
   */
  Notifier.prototype.unsubscribe = function (listenerFunction) {
    this._listeners = _.reject(this._listeners, function (listener) {
      return listener.fn === listenerFunction;
    });
  };

  /**
   * No need to serialize our notifiers, and doing so can cause circular references
   */
  Notifier.prototype.toJSON = function () {
  }

  return Notifier;
});
