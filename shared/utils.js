// todo - more modular?

/**
  * @module Utils
  */
define(["shared/exception"], function (Exception) {
  /**
  * Extend string to be able to "increment" a single char, for example A->B
  */
  String.prototype.increment = function(amount) {
    amount = amount || 1;

    if (this.length !== 1) {
      throw new Exception("InvalidOperation", "Can only increment single char strings");
    }

    var code = this.charCodeAt(0) + amount;
    return String.fromCharCode(code);
  };  
});