#!/bin/bash

if [ "$(uname)" == "Darwin" ]; then
  APPSERVER=dev_appserver.py
  HOST=localhost
else
  APPSERVER=~/google_appengine/dev_appserver.py
  HOST=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)
fi


echo $HOST

$APPSERVER . --host $HOST --admin_host $HOST --port 8082

