require.config({
  baseUrl: "../.."
});

require(["requirePaths"], function () {
  require([
    'lib/lodash',
    'testlib/async'
  ], function (_, async) {

    // on DOM ready
    $('#testframe').ready(function () {    
   
      // todo - still having issues, at least within viewTest
      // todo - should i start dividing these into folders?
      var tests = [
        'sharedTest/tilebagTest',
        'sharedTest/gameboardTest',
        'sharedTest/playerTest',
        'sharedTest/gameTest',
        'clientTest/mergeTests',
        'sharedTest/actionTest',
        'sharedTest/notifierTest',
        'clientTest/localControllerTest',
        'clientTest/networkControllerTest',
        'sharedTest/utilsTest',
        'clientTest/viewTest',
        'clientTest/buyDialogTest',
        'clientTest/mergeSelectDialogTest',
        'serverTest/storeTest',
        'sharedTest/gameLibraryTest',
        'serverTest/serverControllerTest',
        'sharedTest/integrationTest',
        'serverTest/gameStoreTest',
        'serverTest/lobbyTest',
        'serverTest/magnateServerTest',
        'clientTest/aiTest'
      ];

      // todo - ideally dont show green in qunit until im finished all tests
      async.eachSeries(tests, function (test, callback) {
        require([test], function() { callback(null); });
      });
    });
  });
});
