require([
  "class",
  "model/game",
  "controller/localController",
  "controller/aiController",
  "shared/notifier",
  "testutils/gameCreation",
  "testutils/TestView",
  "testlib/seedrandom"
], function (Class, Game, LocalController, AIController, Notifier, gameCreation,
  TestView, seedrandom) {

  QUnit.module("aiTest");

  test("basic test", function () {
    var game = gameCreation.createGame("AsAndBsInHand");
    var view = new TestView(game);
    equal(game.players().length, 2);

    var lc = new LocalController(game, view, [game.players()[0].key()]);
    var ac = new AIController(game, view, game.players()[1].key());

    view.simulatePlayerTileClick("1A");
    view.simulateBuyDialogCompleted();
  });

  function runAIGame(numPlayers, seed) {
    Math.seedrandom(seed);
    
    var view = new TestView(game);
    var NAMES = ["Bob", "Mary", "John", "Matt", "Jennifer", "Chris"];
    var game = gameCreation.createFromNames(NAMES.slice(0, numPlayers));
    game.start();
    
    game.actionSucceeded.subscribe(function (src, action) {
      if (action.verb === "endGame") {
        ok(true, "finished game");
        start();
      }
    });

    _.each(game.players(), function (player) {
      new AIController(game, view, player.key(), 0);
    });
  }

  asyncTest("run ai", function () {
    runAIGame(4, 100);
  });

  // seed 99 4 players -> case where all tiles are illegal (only hit this if we
  // always take player's first tile instead of a random one)
});
