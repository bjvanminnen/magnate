require([
  "model/game",
  "view/buy",
  "testutils/gameCreation",
  "text!templates/buy.html",
  "testutils/TestUtils",
  "testutils/jqueryContext"
], function (Game, BuyDialog, gameCreation, html, TestUtils, noop) {
  
  QUnit.module("buy dialog tests");

  function createDialogFromHtml () {
    var ctx = $("<div id='buyDialogTest-wrapper'>");
    jQuery.attachContext(ctx);    

    // override appendTo so that we dont end up with the dialog showing up in
    // the body
    var options = {
      appendTo: ctx
    };
    return new BuyDialog(options);
  }

  function validateHotelButton(index, isDisabled, cost) {
    var costVisibility = isDisabled ? "none" : "block";

    equal($(".purchase-button").eq(index).button("option", "disabled"), isDisabled,
      "button" + index + " should be disabled: " + isDisabled);
    equal($(".purchase-cost").eq(index).css("display"), costVisibility);
    if (cost) {
      equal($(".purchase-cost").eq(index).text(), cost);
    }
  }

  function validateCloseDialog(dialog) {
    dialog._dialog.dialog("close");
    equal(dialog._dialog.dialog("isOpen"), false, "dialog is not open");
  }

  // todo - buyer-dialog-warn
  test("validate price and visibility", function () {
    var dialog = createDialogFromHtml();
    equal(dialog._dialog.dialog("isOpen"), false, "dialog is not open");
    
    // Purchase buttons have the right classes to get colored correctly
    for (var i = 0; i < Game.HOTELS.length; i++) {
      ok($(".purchase-button").eq(i).hasClass(Game.HOTELS[i]));      
    }

    // create a simple game, and place a tile
    var game = gameCreation.createGame("AsAndBsInHand");
    var keyBob = game.players()[0].key();
    var keyMary = game.players()[1].key();
    TestUtils.doPlaceTile(game, "1A", keyBob);

    dialog.show(game);
    equal(dialog._dialog.dialog("isOpen"), true, "dialog is open");

    // buttons still have class, but are all disabled since we havent founded
    // anything yet
    for (var i = 0; i < Game.HOTELS.length; i++) {      
      ok($(".purchase-button").eq(i).hasClass(Game.HOTELS[i]));      
      validateHotelButton(i, true);      
    }

    validateCloseDialog(dialog);

    // end bob's turn, have mary place a tile and found a chain
    TestUtils.doEndTurn(game, keyBob);
    TestUtils.doPlaceTile(game, "1B", keyMary);
    TestUtils.doFoundChain(game, Game.HOTELS[0]);

    dialog.show(game);
    equal(dialog._dialog.dialog("isOpen"), true, "dialog is open");
    validateHotelButton(0, false, "$200");

    for (var i = 1; i < Game.HOTELS.length; i++) {
      validateHotelButton(i, true);
    }

    validateCloseDialog(dialog);

    // end mary's turn, have bob place a tile that expands the chain
    TestUtils.doEndTurn(game, keyMary);
    TestUtils.doPlaceTile(game, "2A", keyBob);
    
    // price is bigger since chain is larger
    dialog.show(game);    
    validateHotelButton(0, false, "$300");    

    // get another chain founded
    TestUtils.doEndTurn(game, keyBob);
    TestUtils.doPlaceTile(game, "5B", keyMary);
    TestUtils.doEndTurn(game, keyMary);
    TestUtils.doPlaceTile(game, "5A", keyBob);
    TestUtils.doFoundChain(game, Game.HOTELS[6]);

    dialog.show(game);
    validateHotelButton(0, false, "$300");
    validateHotelButton(6, false, "$400"); 
    validateCloseDialog(dialog);    

    jQuery.detachContext();
  });

  test("pressing buttons", function () {
    var dialog = createDialogFromHtml();
    
    var game = gameCreation.createGame("fiveChains");
    dialog.show(game);

    validateHotelButton(0, false, "$200");
    validateHotelButton(1, false, "$300");
    validateHotelButton(2, false, "$300");
    validateHotelButton(3, false, "$600");
    validateHotelButton(4, true);
    validateHotelButton(5, false, "$400");
    validateHotelButton(6, true);

    var numStocks = _.map($(".purchase-button"), function (button) {
      return $(button).text(); });
    deepEqual(numStocks, ["24", "24", "24", "24", "25", "24", "25"]);

    equal($(".purchased-stock").eq(0).css("background"), "none");
    equal($(".purchased-stock").eq(1).css("background"), "none");
    equal($(".purchased-stock").eq(2).css("background"), "none");

    equal($("#buyer-money").text(), "$6000");

    // now validate clicking
    equal(dialog._purchases[0], null);
    $(".purchase-button").eq(0).click();
    equal(dialog._purchases[0], Game.HOTELS[0], "hotel added to purchases");
    equal($(".purchase-button").eq(0).text(), "23");
    equal($("#buyer-money").text(), "$5800");
    
    notEqual($(".purchased-stock").eq(0).css("background"), "none");
    equal($(".purchased-stock").eq(1).css("background"), "none");
    equal($(".purchased-stock").eq(2).css("background"), "none");

    // now remove it
    $(".purchased-stock").eq(0).click();
    equal(dialog._purchases[0], null);
    equal($(".purchase-button").eq(0).text(), "24");
    equal($("#buyer-money").text(), "$6000");

    equal($(".purchased-stock").eq(0).css("background"), "none");
    equal($(".purchased-stock").eq(1).css("background"), "none");
    equal($(".purchased-stock").eq(2).css("background"), "none");
    
    // now add 3 of HOTELS[5]
    $(".purchase-button").eq(5).click();
    $(".purchase-button").eq(5).click();
    $(".purchase-button").eq(5).click();
    equal($(".purchase-button").eq(5).text(), "21");
    equal($("#buyer-money").text(), "$4800");

    notEqual($(".purchased-stock").eq(0).css("background"), "none");
    notEqual($(".purchased-stock").eq(1).css("background"), "none");
    notEqual($(".purchased-stock").eq(2).css("background"), "none");
    equal(dialog._purchases[0], Game.HOTELS[5]);
    equal(dialog._purchases[1], Game.HOTELS[5]);
    equal(dialog._purchases[2], Game.HOTELS[5]);

    // try buying a 4th - should be ignored
    $(".purchase-button").eq(5).click();
    equal($(".purchase-button").eq(5).text(), "21");
    equal($("#buyer-money").text(), "$4800");
    
    var expectedData; 
    dialog.completePurchase.subscribe(function (source, data) {
      deepEqual(data, expectedData, "data = " + expectedData);
    });
    expectedData = [Game.HOTELS[5], Game.HOTELS[5], Game.HOTELS[5]];
    $("#buydialog-ok").click();

    // do it again but this time buy only two
    dialog.show(game);
    $(".purchase-button").eq(5).click();
    $(".purchase-button").eq(5).click();
    expectedData = [Game.HOTELS[5], Game.HOTELS[5], null];
    $("#buydialog-ok").click();

    // and again, buying nothing
    dialog.show(game);
    expectedData = [null, null, null];
    $("#buydialog-ok").click();
  });

  test("out of money", function () {
    var dialog = createDialogFromHtml();
    var game = gameCreation.createGame("fiveChains");

    // hack player 1 to have only 600 dollars
    game.players()[0]._money = 600;

    dialog.show(game);
    equal($("#buyer-money").text(), "$600", "player has only $600");
    $(".purchase-button").eq(5).click();
    equal($("#buyer-money").text(), "$200", "Down to $200");
    // should no longer be enabled
    validateHotelButton(5, true);
  });

  test("out of stock", function () {
    var dialog = createDialogFromHtml();
    var game = gameCreation.createGame("fiveChains");

    // hack HOTELS[0] to have only 1 share remaining
    game._hotels[Game.HOTELS[0]].stock = 1;    

    dialog.show(game);
    equal($(".purchase-button").eq(0).text(), 1);
    $(".purchase-button").eq(0).click();
    // should no longer be enabled
    validateHotelButton(0, true);
  });
  
});
