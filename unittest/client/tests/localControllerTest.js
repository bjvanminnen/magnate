require([
  "class",
  "model/game",
  "controller/localController",
  "shared/notifier",
  "testutils/Waiter",
  "testutils/TestView",
  "testutils/TestUtils",
  "testutils/gameCreation",
  "testlib/async"],
function (Class, Game, LocalController, Notifier, Waiter, TestView, TestUtils,
  gameCreation, async) {

  QUnit.module("local controller tests");

  var exceptional = TestUtils.exceptional;

  // todo - test some error conditions via controller
  
  test("synchronous controller test", function () {
    var game = gameCreation.createGame("AsAndBsInHand");
    var view = new TestView(game);
    var controller = new LocalController(game, view);

    var actionSuccessList = [];
    game.actionSucceeded.subscribe(function (src, action) {
      actionSuccessList.push(action.verb);
    });

    function resetTrackers() {
      actionSuccessList = [];
      view.updated = false;
      view.showedPurchaseDialog = false;
    }

    // Player1 places 1A tile
    var player1 = game.currentPlayer();
    equal(player1.name(), "Bob");
    resetTrackers();
    view.simulatePlayerTileClick("1A");
    deepEqual(actionSuccessList, ["place"]);
    ok(view.updated);
    ok(view.showedPurchaseDialog);
    equal(game.tileState("1A"), "unclaimed", "1A is unclaimed");        
    equal(game.currentPlayer().tiles().length, 5,
      "player has only 5 tiles");
    equal(game.currentPlayer().hasTile("1A"), false,
      "player no longer has tile");
    equal(game.currentPhase(), "buying",
      "Initial tile placement puts game in buying phase");

    // Player 1 ends turn
    resetTrackers();
    view.simulateBuyDialogCompleted();
    deepEqual(actionSuccessList, ["endTurn"]);
    equal(game.currentPhase(), "placing");
    ok(view.updated);
    ok(!view.showedPurchaseDialog);
    equal(game.currentPlayer().name(), "Mary", "Moved on to second player");
    equal(game.players()[0].tiles().length, 6,
      "Player 1 should have redrawn to replace spent tile");

    // Player 2 places 1B
    resetTrackers();
    view.simulatePlayerTileClick("1B");
    deepEqual(actionSuccessList, ["place"]);
    ok(view.updated);
    ok(!view.showedPurchaseDialog, 
      "Didn't show purchase dialog yet since we have to found");
    equal(game.tileState("1B"), "unclaimed", "1B is unclaimed");        
    equal(game.currentPlayer().tiles().length, 5,
      "player has only 5 tiles");
    equal(game.currentPlayer().hasTile("1B"), false,
      "player no longer has tile");
    equal(game.currentPhase(), "founding",
      "Adjacent placed tile puts us in founding phase");

    // Now found a chain
    resetTrackers();
    view.simulateHotelClick(Game.HOTELS[0]);
    deepEqual(actionSuccessList, ["found"]);
    ok(view.updated);
    ok(view.showedPurchaseDialog);

    equal(game.currentPhase(), "buying", "in buying phase");
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 1);

    // end turn, buying 3 shares of founded hotel
    resetTrackers();
    view.simulateBuyDialogCompleted([Game.HOTELS[0], Game.HOTELS[0], Game.HOTELS[0]]);
    deepEqual(actionSuccessList, ["buy", "buy", "buy", "endTurn"]);
    ok(view.updated);
    equal(game.players()[1].stockCount(Game.HOTELS[0]), 4);
    equal(game.players()[1].money(), 5400);

    // todo - expand this, or create additional test(s) to cover merge

    return;
  });

  test("buy two stocks during buy phase", function () {
    var game = gameCreation.createGame("AsAndBsInHand");
    var view = new TestView(game);
    var controller = new LocalController(game, view);

    view.simulatePlayerTileClick("1A");
    view.simulateBuyDialogCompleted();
    view.simulatePlayerTileClick("1B");

    equal(game.currentPhase(), "founding");

    view.showedPurchaseDialog = false;
    view.simulateHotelClick(Game.HOTELS[0]);
    equal(view.showedPurchaseDialog, true, "show purchase dialog after founding");

    equal(game.currentPhase(), "buying");
    view.showedPurchaseDialog = false
    view.simulateBuyDialogCompleted([Game.HOTELS[0], Game.HOTELS[0]]);
    equal(view.showedPurchaseDialog, false,
      "didn't show a second time after purchasing two shares");
  });

  // I have an async and a sync version that are essentially the same.  the
  // reason is that Notifier was made to be async at one point, and i don't
  // fully remember why.  It is not async currently, but I'm worried I may
  // need to make it so again at some point in the future
  test("basic async controller test", function () {
    
    var game = gameCreation.createGame("AsAndBsInHand");
    var view = new TestView(game);
    var controller = new LocalController(game, view);

    // Player1 places 1A tile
    var player1 = game.currentPlayer();
    equal(player1.name(), "Bob");    
    
    var listenerCount = game.actionSucceeded._listeners.length;
    async.series([
      exceptional(function (callback) {
        // Player 1 Places 1A
        var waiter = new Waiter(callback);
        waiter.waitForNotification(game.actionSucceeded, "action");
        waiter.waitForNotification(view.didShowPurchaseDialog, "purchaseDialog");
        view.simulatePlayerTileClick("1A");
      }),
      exceptional(function (callback) {
        // Validation after placement
        equal(game.tileState("1A"), "unclaimed", "1A is unclaimed");        
        equal(game.currentPlayer().tiles().length, 5,
          "player has only 5 tiles");
        equal(game.currentPlayer().hasTile("1A"), false,
          "player no longer has tile");
        equal(game.currentPhase(), "buying",
          "Initial tile placement puts game in buying phase");
        
        // Player 1 ends turn
        var waiter = new Waiter(callback);
        waiter.waitForNotification(game.actionSucceeded, "action");
        waiter.waitForNotification(view.buyDialogCompleted, "buyDialog");
        view.simulateBuyDialogCompleted();
      }),
      exceptional(function (callback) {
        // Validation after ends turn
        equal(game.currentPlayer().name(), "Mary", "Moved on to second player");

        equal(game.players()[0].tiles().length, 6,
          "Player 1 should have redrawn to replace spent tile");
        
        // Player 2 places 1B
        var waiter = new Waiter(callback);
        waiter.waitForNotification(game.actionSucceeded, "action");
        view.simulatePlayerTileClick("1B");
      }),
      exceptional(function (callback) {
        // Validation after placement

        // todo - validate no notification
        //ok(!view.showedPurchaseDialog,
        //  "Didn't show purchase dialog yet since we have to found");

        equal(game.tileState("1B"), "unclaimed", "1B is unclaimed");        
        equal(game.currentPlayer().tiles().length, 5,
          "player has only 5 tiles");
        equal(game.currentPlayer().hasTile("1B"), false,
          "player no longer has tile");
        equal(game.currentPhase(), "founding",
          "Adjacent placed tile puts us in founding phase");
        
        // Now found a chain
        var waiter = new Waiter(callback);
        waiter.waitForNotification(game.actionSucceeded, "action");
        waiter.waitForNotification(view.didShowPurchaseDialog, "purchaseDialog");        
        view.simulateHotelClick(Game.HOTELS[0]);
        
      }),
      exceptional(function (callback) {
        equal(game.currentPhase(), "buying", "in buying phase");
        equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 1);
        
        // end turn, buying 3 shares of founded hotel
        var waiter = new Waiter(callback);

        // four actions (3 buys + 1 end turn)
        waiter.waitForNotification(game.actionSucceeded, "action", 4);
        waiter.waitForNotification(view.buyDialogCompleted, "buyDialog");
        waiter.waitForNotification(view.didUpdate, "view update");
        view.simulateBuyDialogCompleted([Game.HOTELS[0], Game.HOTELS[0], Game.HOTELS[0]]);
      }),
      exceptional(function (callback) {
        equal(game.players()[1].stockCount(Game.HOTELS[0]), 4);
        equal(game.players()[1].money(), 5400);
        callback(null);
      })
    ], function(err, results) {
      equal(game.actionSucceeded._listeners.length, listenerCount,
          "No extra listeners left around");
      ok(true, "done");
    });
  });

  test("localController with key subset", function () {
    var game = gameCreation.createGame("AsAndBsInHand");
    var view = new TestView(game);
    equal(game.players().length, 2);

    var controller = new LocalController(game, view, [game.players()[0].key()]);
    view.simulatePlayerTileClick("1A");
    equal(game.currentPhase(), "buying");
    view.simulateBuyDialogCompleted();
    equal(game.currentPhase(), "placing");
    equal(game.currentPlayer(), game._players[1]);

    view.simulatePlayerTileClick("1B");
    equal(game.currentPhase(), "placing", "tile click is ignored");

  });

});
