require([
  "model/game",
  "view/mergeSelect",
  "testutils/gameCreation",
  "testutils/TestUtils",
  "text!templates/mergeSelect.html",
  "testutils/jqueryContext"
], function (Game, MergeSelectDialog, gameCreation, TestUtils, html, noop) {
  
  QUnit.module("merge select dialog tests");
  require(["testutils/jqueryContext"]);

  function createDialogFromHtml () {
    var ctx = $("<div id='mergeSelectDialogTest-wrapper'>");
    jQuery.attachContext(ctx);    

    // override appendTo so that we dont end up with the dialog showing up in
    // the body
    var options = {
      appendTo: ctx
    };
    return new MergeSelectDialog(options);
  }

  test("simple merge", function () {
    var mergeSelect = createDialogFromHtml();
    var dialog = mergeSelect._dialog;
    equal(dialog.dialog("isOpen"), false, "dialog is not open");

    // buttons have correct class
    for (var i = 0; i < Game.HOTELS.length; i++) {
      ok($(".buyer-button").eq(i).hasClass(Game.HOTELS[i]));
      ok($(".target-button").eq(i).hasClass(Game.HOTELS[i]));
    }

    var game = gameCreation.createGame("merge22");

    // place our merge tile
    TestUtils.doPlaceTile(game, "2B");
    equal(game.currentPhase(), "merging");

    mergeSelect.show(game);
    equal(dialog.dialog("isOpen"), true, "dialog is open");

    var buttonsDisabled = _.map($(".buyer-button"), function (btn) {
      return $(btn).button("option", "disabled");
    });
    deepEqual(buttonsDisabled, [false, true, true, false, true, true, true]);

    buttonsDisabled = _.map($(".target-button"), function (btn) {
      return $(btn).button("option", "disabled");
    });
    deepEqual(buttonsDisabled, [false, true, true, false, true, true, true]);
    
    var buttonLabels = _.map($(".buyer-button"), function (btn) {
      return $(btn).button("option", "label");
    });
    deepEqual(buttonLabels, ["-2-", "-0-", "-0-", "-2-", "-0-", "-0-", "-0-"]);

    buttonLabels = _.map($(".target-button"), function (btn) {
      return $(btn).button("option", "label");
    });
    deepEqual(buttonLabels, ["-2-", "-0-", "-0-", "-2-", "-0-", "-0-", "-0-"]);

    equal($("#merge-select-merge").button("option", "disabled"), true);

    // now click one of the buyers
    $(".buyer-button").eq(0).click();

    equal($(".buyer-button").eq(0).hasClass("highlighted"), true);
    // todo : since we have only two options, should also highlight relevant target
    //ok($(".target-button").eq(3).hasClass("highlighted"));
    //equal($("#merge-select-merge").button("option", "disabled"), false);

    // click target of same hotel
    $(".target-button").eq(0).click();
    equal($(".buyer-button").eq(0).hasClass("highlighted"), false);
    //equal($(".buyer-button").eq(3).hasClass("highlighted"), true);
    equal($(".target-button").eq(0).hasClass("highlighted"), true);
    equal($(".target-button").eq(3).hasClass("highlighted"), false);
    //equal($("#merge-select-merge").button("option", "disabled"), false);

    // now click buyer of different hotel
    $(".buyer-button").eq(3).click();
    equal($(".buyer-button").eq(0).hasClass("highlighted"), false);
    equal($(".buyer-button").eq(3).hasClass("highlighted"), true);
    equal($(".target-button").eq(0).hasClass("highlighted"), true);
    equal($(".target-button").eq(3).hasClass("highlighted"), false);
    equal($("#merge-select-merge").button("option", "disabled"), false);

    $("#merge-select-merge").click();
    equal(dialog.dialog("isOpen"), false);

  });

  // todo - two hotels, one bigger - should autoselect

  // todo - three hotels, one smallest - still shows right size

  // todo - other cases
  
});
