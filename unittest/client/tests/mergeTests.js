require([
  "model/game",
  "model/player",
  "shared/exception",
  "testutils/TestUtils",
  "testutils/gameCreation"
], function (Game, Player, Exception, TestUtils, gameCreation) {
  QUnit.module("lodash");

  QUnit.module("merge tests");

  var tileChooser = {
      north: ["5E", "4E", "3E", "2E", "1E", "1D"],
      east: ["6F", "6G", "6H", "6I", "5I", "4I"],
      south: ["7E", "8E", "9E", "10E", "11E", "12E"],
      west: ["6D", "6C", "6B", "6A", "5A", "4A"]
    };
  
  /**
    * Creates a chain in the provided direction up to size 6
    *
    * @param {Game} game The game we're modifying
    * @param {string} direction The direction we want the chain
    *   (north, south, east, or west)
    * @param {string} hotel The hotel to make the chain
    * @param {number} size The length of the chain, currently max 6
    */
  function createChainCenteredAround6E(game, direction, hotel, size) {
    if (size > 6 || tileChooser[direction] === undefined) {
      throw new Exception("NotSupported");
    }

    for (var i = 0; i < size; i++) {
      TestUtils.currentPlayerPlaceTile(game, tileChooser[direction][i]);
      if (game.currentPhase() === "founding") {
        TestUtils.doFoundChain(game, hotel);
      }
      TestUtils.doEndTurn(game);
    }

    equal(game.chainSize(hotel), size);
  }

  test("payouts", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();
    var bigHotel = Game.HOTELS[1];
    var smallHotel = Game.HOTELS[0];

    createChainCenteredAround6E(game, "north", bigHotel, 3);
    createChainCenteredAround6E(game, "south", smallHotel, 2);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    function createPlayerWithStock(name, pkey, stock, amount) {
      var player = new Player(name, pkey);
      if (amount) {
        player.addStock(stock, amount);
      }
      player.subtractMoney(player._money); // start with 0 money
      return player;
    }

    function runMajorityMinorityTest(matrix) {
      var players = [];
      for (var i = 0; i < matrix.length; i++) {
        players.push(createPlayerWithStock(matrix[i][0], i, matrix[i][1],
          matrix[i][2]));
      }

      // override game's players
      // note: this means player stocks + game's count of how many stocks remain
      // dont necessarily add up to 25
      var gameClone = TestUtils.clone(game);
      gameClone._players = players;
      
      TestUtils.doMerge(gameClone, bigHotel, smallHotel);      

      for (var i = 0; i < players.length; i++) {
        equal(players[i]._money, matrix[i][3],
          "testing for " + players[i]._name);
      }
    }    

    ok(true, "two owners, one has majority");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 2, 2000],
      ["Mary", smallHotel, 1, 1000],
      ["John", smallHotel, 0, 0],
      ["Jane", smallHotel, 0, 0]
    ]);

    ok(true, "three owners, two share majority");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 2, 1500],
      ["Mary", smallHotel, 2, 1500],
      ["John", smallHotel, 1, 0],
      ["Jane", smallHotel, 0, 0]
    ]);

    ok(true, "three owners, two share minority");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 2, 2000],
      ["Mary", smallHotel, 1, 500],
      ["John", smallHotel, 1, 500],
      ["Jane", smallHotel, 0, 0]
    ]);

    ok(true, "three owners, all the same");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 2, 1000],
      ["Mary", smallHotel, 2, 1000],
      ["John", smallHotel, 2, 1000],
      ["Jane", smallHotel, 0, 0]
    ]);

    ok(true, "four owners, threeway split on minority (doesnt divide evenly)");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 2, 400],
      ["Mary", smallHotel, 3, 2000],
      ["John", smallHotel, 2, 400],
      ["Jane", smallHotel, 2, 400]
    ]);

    ok(true, "single owner");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 0, 0],
      ["Mary", smallHotel, 0, 0],
      ["John", smallHotel, 2, 3000],
      ["Jane", smallHotel, 0, 0]
    ]);

    ok(true, "four owners, ties on majority and minority");
    runMajorityMinorityTest([
      ["Bob", smallHotel, 2, 0],
      ["Mary", smallHotel, 3, 1500],
      ["John", smallHotel, 3, 1500],
      ["Jane", smallHotel, 2, 0]
    ]);
  });

  test("merge two chains, one is bigger", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var bigHotel = Game.HOTELS[0];
    var smallHotel = Game.HOTELS[1];
    var unfoundedHotel = Game.HOTELS[2];
    var foundedHotel = Game.HOTELS[3];

    createChainCenteredAround6E(game, "north", bigHotel, 3);
    createChainCenteredAround6E(game, "south", smallHotel, 2);

    TestUtils.currentPlayerPlaceTile(game, "12I");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "12H");
    TestUtils.doFoundChain(game, foundedHotel);
    TestUtils.doEndTurn(game);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    // merge works
    var game2 = TestUtils.clone(game);
    TestUtils.doMerge(game2, bigHotel, smallHotel);
    equal(game2.chainSize(bigHotel), 6);
    equal(game2.chainSize(smallHotel), 0);
    equal(game2.currentPhase(), "resolving");

    // self merge should fail
    assertThrows(function () {
      var clone = TestUtils.clone(game);
      TestUtils.doMerge(clone, bigHotel, bigHotel);
    }, /InvalidArgument/);

    // merging in inverse order should fail
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), smallHotel, bigHotel);
    }, /InvalidOperation/);

    // trying to merge with an unfounded chain fails
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), bigHotel, unfoundedHotel);
    }, /InvalidOperation/);

    // trying to merge with founded chain not part of the conflict fails    
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), bigHotel, foundedHotel);
    }, /InvalidOperation/);

  });

  test("merge two chains plus unclaimed, one is bigger", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var bigHotel = Game.HOTELS[0];
    var smallHotel = Game.HOTELS[1];

    createChainCenteredAround6E(game, "north", bigHotel, 3);
    createChainCenteredAround6E(game, "south", smallHotel, 2);

    // unclaimed
    TestUtils.currentPlayerPlaceTile(game, "6F");
    TestUtils.doEndTurn(game);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    // merge works
    var game2 = TestUtils.clone(game);
    TestUtils.doMerge(game2, bigHotel, smallHotel);
    equal(game2.chainSize(bigHotel), 7);
    equal(game2.chainSize(smallHotel), 0);
    equal(game2.currentPhase(), "resolving");

    // merging in inverse order should fail
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), smallHotel, bigHotel);
    }, /InvalidOperation/);
  });

  test("merge two chains, same size", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var hotel1 = Game.HOTELS[0];
    var hotel2 = Game.HOTELS[1];

    createChainCenteredAround6E(game, "north", hotel1, 3);
    createChainCenteredAround6E(game, "south", hotel2, 3);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    // merge should work in either direction
    var game1buys = TestUtils.clone(game);
    TestUtils.doMerge(game1buys, hotel1, hotel2);
    equal(game1buys.chainSize(hotel1), 7);
    equal(game1buys.chainSize(hotel2), 0);
    equal(game1buys.currentPhase(), "resolving");

    var game2buys = TestUtils.clone(game);
    TestUtils.doMerge(game2buys, hotel2, hotel1);
    equal(game2buys.chainSize(hotel1), 0);
    equal(game2buys.chainSize(hotel2), 7);
    equal(game2buys.currentPhase(), "resolving");

  });

  test("merge three chains, two biggest are same size", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var big1 = Game.HOTELS[0];
    var big2 = Game.HOTELS[1];
    var small1 = Game.HOTELS[2];

    createChainCenteredAround6E(game, "north", big1, 3);
    createChainCenteredAround6E(game, "south", big2, 3);
    createChainCenteredAround6E(game, "east", small1, 2);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    // small can't acquire a big
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), small1, big1);
    }, /InvalidOperation/);

    // big can't acquire small
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), big1, small1);
    }, /InvalidOperation/);

    // either big can acquire the other
    var game1buys = TestUtils.clone(game);
    TestUtils.doMerge(game1buys, big1, big2);
    equal(game1buys.chainSize(big1), 6);
    equal(game1buys.chainSize(big2), 0);
    equal(game1buys.chainSize(small1), 2);
    equal(game1buys.currentPhase(), "resolving");

    var game2buys = TestUtils.clone(game);
    TestUtils.doMerge(game2buys, big2, big1);
    equal(game2buys.chainSize(big1), 0);
    equal(game2buys.chainSize(big2), 6);
    equal(game2buys.chainSize(small1), 2);
    equal(game2buys.currentPhase(), "resolving");
  });

  test("merge three chains, two smallest are same size", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var big1 = Game.HOTELS[0];
    var small1 = Game.HOTELS[1];
    var small2 = Game.HOTELS[2];

    createChainCenteredAround6E(game, "north", big1, 5);
    createChainCenteredAround6E(game, "south", small1, 2);
    createChainCenteredAround6E(game, "east", small2, 2);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    // small can't acquire small
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), small1, small2);
    }, /InvalidOperation/);

    // small can't acquire big
    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), small1, big1);
    }, /InvalidOperation/);

    // big can acquire either small
    var gamebuys1 = TestUtils.clone(game);
    TestUtils.doMerge(gamebuys1, big1, small1);
    equal(gamebuys1.chainSize(big1), 7);
    equal(gamebuys1.chainSize(small1), 0);
    equal(gamebuys1.chainSize(small2), 2);
    equal(gamebuys1.currentPhase(), "resolving");

    var gamebuys2 = TestUtils.clone(game);
    TestUtils.doMerge(gamebuys2, big1, small2);
    equal(gamebuys2.chainSize(big1), 7);
    equal(gamebuys2.chainSize(small1), 2);
    equal(gamebuys2.chainSize(small2), 0);
    equal(gamebuys2.currentPhase(), "resolving");
  });

  test("merge three chains, all same size", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var hotel1 = Game.HOTELS[0];
    var hotel2 = Game.HOTELS[1];
    var hotel3 = Game.HOTELS[2];

    createChainCenteredAround6E(game, "north", hotel1, 4);
    createChainCenteredAround6E(game, "south", hotel2, 4);
    createChainCenteredAround6E(game, "east", hotel3, 4);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    function testMerge(buyer, seller, other) {
      var testGame = TestUtils.clone(game);    
      TestUtils.doMerge(testGame, buyer, seller);
      equal(testGame.chainSize(buyer), 8);
      equal(testGame.chainSize(seller), 0);
      equal(testGame.chainSize(other), 4);
      equal(testGame.currentPhase(), "resolving");
    }

    // validate all permutations
    testMerge(hotel1, hotel2, hotel3);
    testMerge(hotel1, hotel3, hotel2);
    testMerge(hotel2, hotel1, hotel3);
    testMerge(hotel2, hotel3, hotel1);
    testMerge(hotel3, hotel1, hotel2);
    testMerge(hotel3, hotel2, hotel1);

    // have 1 acquire 2, and do a resolve
    TestUtils.doMerge(game, hotel1, hotel2);
    equal(game.currentPhase(), "resolving");
    equal(game.players()[0].stockCount(hotel2), 0);
    equal(game.players()[1].stockCount(hotel2), 1);
    equal(game.players()[2].stockCount(hotel2), 0);
    TestUtils.doResolveMerge(game, 1, 0, 0, game.players()[1].key());
    equal(game.currentPhase(), "merging");

    // now have 1 acquire 3 and do a resolve
    TestUtils.doMerge(game, hotel1, hotel3);
    equal(game.chainSize(hotel1), 13);
    equal(game.chainSize(hotel2), 0);
    equal(game.chainSize(hotel3), 0);
    equal(game.currentPhase(), "resolving");
    TestUtils.doResolveMerge(game, 1, 0, 0, game.players()[1].key());
    equal(game.currentPhase(), "buying");
  });

  test("merge four chains, biggest two and smallest two are same size", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    // todo - finish this once i can do multiple merges

    expect(0);
  });

  test("merge four chains, all different sizes", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    // todo - finish this once i can do multiple merges

    expect(0);
  });

  test("merge two chains unrelated to the last tile we played", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    // create four separate hotels, then place a tile that would merge hotels
    // three and four.  after entering merge phase, attempt to merge hotels
    // one and two
    var hotel1 = Game.HOTELS[0];
    var hotel2 = Game.HOTELS[1];
    var hotel3 = Game.HOTELS[2];
    var hotel4 = Game.HOTELS[3];

    TestUtils.currentPlayerPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "1B");
    TestUtils.doFoundChain(game, hotel1);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "3A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "3B");
    TestUtils.doFoundChain(game, hotel2);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "5A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "5B");
    TestUtils.doFoundChain(game, hotel3);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "7A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "7B");
    TestUtils.doFoundChain(game, hotel4);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "6A");
    equal(game.currentPhase(), "merging");

    // at this point hotels 3 and 4 can me merged.  make sure others cant be

    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), hotel1, hotel2);
    }, /InvalidOperation/);

    assertThrows(function () {
      TestUtils.doMerge(TestUtils.clone(game), hotel3, hotel2);
    }, /InvalidOperation/);
    
  });


  // resolve - general
  // - all players have stock
  //   - properly cycle
  //   - end with correct player, buying
  // - some players have stock
  //   - properly cycle
  //   - end with correct player, buying
  // - conflict after resolving
  //   - end in merge phase with correct data

  test("resolve - turn order", function () {    
    var baseGame = gameCreation.createGame("merge222");

    var everyoneOwns = Game.HOTELS[0];
    var bobGeorgeOwn = Game.HOTELS[1];
    var georgeMaryOwn = Game.HOTELS[2];

    equal(baseGame.chainSize(Game.HOTELS[0]), 2);
    equal(baseGame.chainSize(Game.HOTELS[1]), 2);
    equal(baseGame.chainSize(Game.HOTELS[2]), 2);
    equal(baseGame.currentPlayer().name(), "Mary");
    ok(_.contains(baseGame.currentPlayer().tiles(), "3A"));
    var keyBob = baseGame.players()[0].key();
    var keyMary = baseGame.players()[1].key();
    var keyGeorge = baseGame.players()[2].key();
    
    TestUtils.doPlaceTile(baseGame, "3A", keyMary);
    equal(baseGame.currentPhase(), "merging");

    ok(true, "merge in which all players own target stock");
    var game = TestUtils.clone(baseGame);
    TestUtils.doMerge(game, bobGeorgeOwn, everyoneOwns);
    equal(game.currentPhase(), "resolving");
    equal(game.resolvingPlayer().name(), "Mary");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyMary);
    equal(game.resolvingPlayer().name(), "George");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyGeorge);
    equal(game.resolvingPlayer().name(), "Bob");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyBob);
    equal(game.currentPhase(), "merging");
    equal(game.currentPlayer().name(), "Mary");

    ok(true, "merge in which some players (George, Mary) own stock")
    var game = TestUtils.clone(baseGame);
    TestUtils.doMerge(game, everyoneOwns, georgeMaryOwn);
    equal(game.currentPhase(), "resolving");
    equal(game.resolvingPlayer().name(), "Mary");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyMary);
    equal(game.resolvingPlayer().name(), "George");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyGeorge);
    equal(game.currentPhase(), "merging");
    equal(game.currentPlayer().name(), "Mary");

    ok(true, "merge in which current player doesnt own stock (George, Bob do)");
    var game = TestUtils.clone(baseGame);
    TestUtils.doMerge(game, everyoneOwns, bobGeorgeOwn);
    equal(game.currentPhase(), "resolving");
    equal(game.resolvingPlayer().name(), "George");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyGeorge);
    equal(game.resolvingPlayer().name(), "Bob");
    equal(game.currentPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game, 1, 0, 0, keyBob);
    equal(game.currentPhase(), "merging");
    equal(game.currentPlayer().name(), "Mary");

    ok(true, "now go through subsequent levels of merge");
    TestUtils.doMerge(game, everyoneOwns, georgeMaryOwn);
    TestUtils.doResolveMerge(game, 1, 0, 0, keyMary);
    TestUtils.doResolveMerge(game, 1, 0, 0, keyGeorge);
    equal(game.currentPhase(), "buying");
  });

  test("resolve - holding", function () {
    var game = gameCreation.createGame("merge222");

    var keyMary = game.players()[1].key();
    
    var buyer = Game.HOTELS[1];
    var target = Game.HOTELS[0];

    TestUtils.doPlaceTile(game, "3A", keyMary);
    equal(game.currentPhase(), "merging");
    TestUtils.doMerge(game, buyer, target);
    equal(game.currentPhase(), "resolving");
    equal(game.players()[1].stockCount(target), 1);

    // cant hold more than you have
    var action = Game.resolveMergeAction(keyMary, 3, 0, 0);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 1, "still have stock");
    equal(game.resolvingPlayer().name(), "Mary");

    // cant hold negative amount
    var action = Game.resolveMergeAction(keyMary, -1, 0, 0);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 1, "still have stock");
    equal(game.resolvingPlayer().name(), "Mary");

    // can hold 1 (if that's what you have)
    var action = Game.resolveMergeAction(keyMary, 1, 0, 0);
    var clone = TestUtils.clone(game);
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].stockCount(target), 1, "still have stock");

    // hack mary to have 3 stock
    game._givePlayerShares(target, game.players()[1], 2);
    equal(game.players()[1].stockCount(target), 3);

    // cant hold fewer than you have (when doing nothing else)
    var action = Game.resolveMergeAction(keyMary, 2, 0, 0);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 3, "still have stock");
    equal(game.resolvingPlayer().name(), "Mary");
    
    // can hold more than 1 (if that's what you have)
    var action = Game.resolveMergeAction(keyMary, 3, 0, 0);
    var clone = TestUtils.clone(game);
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].stockCount(target), 3, "still have stock");
  });

  test("resolve - selling", function () {
    var game = gameCreation.createGame("merge222");

    var keyMary = game.players()[1].key();
    
    var buyer = Game.HOTELS[1];
    var target = Game.HOTELS[0];

    TestUtils.doPlaceTile(game, "3A", keyMary);
    equal(game.currentPhase(), "merging");
    TestUtils.doMerge(game, buyer, target);
    equal(game.currentPhase(), "resolving");
    equal(game.players()[1].stockCount(target), 1);

    var startingMoney = game.players()[1].money();

    // cant sell more than you have
    var action = Game.resolveMergeAction(keyMary, 0, 3, 0);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 1, "still have stock");
    equal(game.players()[1].money(), startingMoney, "money didnt change");
    equal(game.resolvingPlayer().name(), "Mary");

    // cant sell negative amount
    var action = Game.resolveMergeAction(keyMary, 0, -1, 0);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 1, "still have stock");
    equal(game.players()[1].money(), startingMoney, "money didnt change");
    equal(game.resolvingPlayer().name(), "Mary");

    // can sell 1 (if that's what you have)
    var action = Game.resolveMergeAction(keyMary, 0, 1, 0);
    var clone = TestUtils.clone(game);
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].money(), startingMoney + 200, "got paid");
    equal(clone.players()[1].stockCount(target), 0, "lost stock");

    // hack mary to have 3 stock
    game._givePlayerShares(target, game.players()[1], 2);
    equal(game.players()[1].stockCount(target), 3);

    // cant sell fewer than you have (when doing nothing else)
    var action = Game.resolveMergeAction(keyMary, 0, 2, 0);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 3, "still have stock");
    equal(game.players()[1].money(), startingMoney, "money didnt change");
    equal(game.resolvingPlayer().name(), "Mary");
    
    // can sell more than 1 (if that's what you have)
    var action = Game.resolveMergeAction(keyMary, 0, 3, 0);
    var clone = TestUtils.clone(game);
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].money(), startingMoney + 3*200, "got paid");
    equal(clone.players()[1].stockCount(target), 0, "lost stock");
  });

  test("resolve - trading", function () {
    var game = gameCreation.createGame("merge222");

    var keyMary = game.players()[1].key();
    
    var buyer = Game.HOTELS[1];
    var target = Game.HOTELS[0];

    TestUtils.doPlaceTile(game, "3A", keyMary);
    equal(game.currentPhase(), "merging");
    TestUtils.doMerge(game, buyer, target);
    equal(game.currentPhase(), "resolving");
    equal(game.players()[1].stockCount(target), 1);    

    // trading odd amount
    var action = Game.resolveMergeAction(keyMary, 0, 0, 1);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 1);
    equal(game.players()[1].stockCount(buyer), 0);

    // hack mary to have 2 shares
    game._givePlayerShares(target, game.players()[1], 1);
    equal(game.players()[1].stockCount(target), 2);

    // cant trade more than you have
    var action = Game.resolveMergeAction(keyMary, 0, 0, 4);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 2, "still have stock");
    equal(game.players()[1].stockCount(buyer), 0, "didnt gain buyer stock");
    equal(game.resolvingPlayer().name(), "Mary");

    // cant trade negative amount
    var action = Game.resolveMergeAction(keyMary, 0, 0, -2);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 2, "still have stock");
    equal(game.players()[1].stockCount(buyer), 0, "didnt gain buyer stock");
    equal(game.resolvingPlayer().name(), "Mary");

    // can trade 2 (if that's what you have)
    var action = Game.resolveMergeAction(keyMary, 0, 0, 2);
    var clone = TestUtils.clone(game);
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].stockCount(target), 0, "dont have stock");
    equal(clone.players()[1].stockCount(buyer), 1, "got share of buyer stock");

    // hack mary to have four shares
    game._givePlayerShares(target, game.players()[1], 2);
    equal(game.players()[1].stockCount(target), 4);

    // cant trade fewer than you have (when doing nothing else)
    var action = Game.resolveMergeAction(keyMary, 0, 0, 2);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 4, "still have stock");
    equal(game.players()[1].stockCount(buyer), 0, "didnt gain buyer stock");
    equal(game.resolvingPlayer().name(), "Mary");
    
    // can trade multiple (if that's what you have)
    var action = Game.resolveMergeAction(keyMary, 0, 0, 4);
    var clone = TestUtils.clone(game);
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].stockCount(target), 0, "dont have stock");
    equal(clone.players()[1].stockCount(buyer), 2, "got share of buyer stock");

    // hack george to get all shares of buyer
    game._givePlayerShares(buyer, game.players()[2], game._hotels[buyer].stock);
    equal(game._hotels[buyer].stock, 0);

    // trading when insufficient amount of buyer
    var action = Game.resolveMergeAction(keyMary, 0, 0, 4);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.players()[1].stockCount(target), 4, "still have stock");
    equal(game.players()[1].stockCount(buyer), 0, "didnt gain buyer stock");
    equal(game.resolvingPlayer().name(), "Mary");

  });

  test("resolve - mixed", function () {
    var game = gameCreation.createGame("merge222");

    var keyMary = game.players()[1].key();
    
    var buyer = Game.HOTELS[1];
    var target = Game.HOTELS[0];

    TestUtils.doPlaceTile(game, "3A", keyMary);
    equal(game.currentPhase(), "merging");
    TestUtils.doMerge(game, buyer, target);
    equal(game.currentPhase(), "resolving");
    equal(game.players()[1].stockCount(target), 1);

    // hack mary to have eight shares
    game._givePlayerShares(target, game.players()[1], 7);
    equal(game.players()[1].stockCount(target), 8);

    // sum too high
    var action = Game.resolveMergeAction(keyMary, 4, 4, 2);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.resolvingPlayer().name(), "Mary");

    // sum too low
    var action = Game.resolveMergeAction(keyMary, 2, 2, 2);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.resolvingPlayer().name(), "Mary");

    // right sum, but with negatives
    var action = Game.resolveMergeAction(keyMary, 6, 4, -2);
    assertThrows(function () {
      game.performAction(action);
    }, /InvalidOperation/);
    equal(game.resolvingPlayer().name(), "Mary");

    // valid sum works
    var clone = TestUtils.clone(game);
    var action = Game.resolveMergeAction(keyMary, 2, 4, 2);
    var startMoney = clone.players()[1].money();
    clone.performAction(action);
    equal(clone.resolvingPlayer().name(), "George");
    equal(clone.players()[1].stockCount(target), 2, "held stocks");
    equal(clone.players()[1].money(), startMoney + 200 * 4, "held stocks");
    equal(clone.players()[1].stockCount(buyer), 1, "traded stocks");
  });  


  test("simple resolve merge", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var bigHotel = Game.HOTELS[0];
    var smallHotel = Game.HOTELS[1];

    createChainCenteredAround6E(game, "north", bigHotel, 3);
    createChainCenteredAround6E(game, "south", smallHotel, 2);

    // place merge tile
    TestUtils.currentPlayerPlaceTile(game, "6E");
    equal(game.currentPhase(), "merging");

    ok("test1: current player doesnt have stock");
    var game1 = TestUtils.clone(game);
    TestUtils.doMerge(game1, bigHotel, smallHotel);
    equal(game1.currentPhase(), "resolving");
    
    equal(game1.currentPlayer().name(), "Mary");

    // Bob already has 1 share from founding
    var players = game1.players();
    equal(players[0].stockCount(smallHotel), 1);
    equal(players[1].stockCount(smallHotel), 0);
    equal(players[2].stockCount(smallHotel), 0);
    equal(players[3].stockCount(smallHotel), 0);

    equal(game1.resolvingPlayer().name(), "Bob",
      "Resolving player should be Bob since he's the only one with shares");

    TestUtils.doResolveMerge(game1, 1, 0, 0, players[0].key());

    equal(game1.currentPhase(), "buying");

    // Now repeat, but this time give other players shares
    ok("test2: current player, and two other players, have stock");
    var game2 = TestUtils.clone(game);
    players = game2.players();
    
    // hack players to have stock
    players[1].addStock(smallHotel);
    players[3].addStock(smallHotel);

    // do merge
    TestUtils.doMerge(game2, bigHotel, smallHotel);
    
    equal(game2.currentPhase(), "resolving");
    equal(game2.resolvingPlayer().name(), "Mary");
    TestUtils.doResolveMerge(game2, 1, 0, 0, players[1].key());
    ok(true, "mary does her merge resolve");

    equal(game2.currentPhase(), "resolving");
    equal(game2.resolvingPlayer().name(), "Jane",
      "skip john, who has no stock, go to jane");
    TestUtils.doResolveMerge(game2, 1, 0, 0, players[3].key());
    ok(true, "jane does her merge resolve");

    equal(game2.currentPhase(), "resolving");
    equal(game2.resolvingPlayer().name(), "Bob", "now bob's turn");
    TestUtils.doResolveMerge(game2, 1, 0, 0, players[0].key());
    ok(true, "bob does his merge resolve");

    equal(game2.currentPhase(), "buying");

    // todo - resolve tests for selling/trading
  });
  
});