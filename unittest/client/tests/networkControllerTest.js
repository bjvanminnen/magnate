require([
  "class",
  "model/game",
  "controller/networkController",
  "shared/notifier",
  "shared/exception",
  "testutils/TestView",
  "testutils/TestUtils",
  "testutils/gameCreation",
  "testutils/SocketSim"
], function (Class, Game, NetworkController, Notifier, Exception,
  TestView, TestUtils, gameCreation, SocketSim) {

  QUnit.module("network controller Tests");

  test("tile drawing", function () {
    var serverGame = gameCreation.createGame("AsAndBsInHand");
    var gameId = 0;
    
    var view = new TestView(game);    

    var keyBob = serverGame.players()[0].key();
    var keyMary = serverGame.players()[1].key();

    var game = Game.createFromJSON(JSON.stringify(
      serverGame.privatized(keyBob)));

    var socketSim = new SocketSim();
    var controller = new NetworkController(game, view, gameId,
      socketSim.client, keyBob);    

    // Bob successfully takes his turn
    view.simulatePlayerTileClick("1A");
    equal(game.currentPhase(), "buying");
    view.simulateBuyDialogCompleted();
    equal(game.currentPhase(), "awaitingDraw",
      "tile draw should only happen on server with network controller");

    // simulate receiving the update action from server
    var tile = serverGame._tilebag.getTile();
    var updateAction = Game.drawUpdateAction(keyBob, tile);
    controller._onServerActionSuccess(updateAction);
    equal(game.players()[0].tiles()[5], tile, "bob got tile");
    equal(game.currentPhase(), "placing");
    equal(game.currentPlayer().name(), "Mary");    
  });

  test("placing tiles when not our turn", function () {
    var game = gameCreation.createGame("AsAndBsInHand");
    var gameId = 0;
    
    var view = new TestView(game);    

    var keyBob = game.players()[0].key();
    var keyMary = game.players()[1].key();

    var socketSim = new SocketSim();
    var controller = new NetworkController(game, view, gameId,
      socketSim.client, keyMary);

    // we have a controller/view for mary.  it's bob's turn.  make sure we cant
    // place tiles
    view.simulatePlayerTileClick("2A"); // a tile bob has
    equal(game.currentPhase(), "placing");
    view.simulatePlayerTileClick("1B"); // a tile mary has
    equal(game.currentPhase(), "placing");
  });
});