require([
  "model/game",
  "view/domview",
  "text!templates/game.html",
  "testutils/jqueryContext"
], function (Game, DomView, html, noop) {
  
  QUnit.module("view tests");
  require(["testutils/jqueryContext"]);

  test("dom reset works", function () {
    jQuery.attachContext($("<div>" + html + "</div>"));
    equal($("#main_div").length, 1);
    equal($("#main_div").hasClass("foo"), false, "doesnt start with a foo");
    $("#main_div").addClass("foo");
    equal($("#main_div").hasClass("foo"), true, "given a foo");
    jQuery.attachContext($("<div>" + html + "</div>"));
    equal($("#main_div").length, 1);
    equal($("#main_div").hasClass("foo"), false, "no foo after reload");

    jQuery.detachContext();
  });

  // todo - more tests
  
});
