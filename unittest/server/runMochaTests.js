#!/usr/bin/env node

// todo - to be more correct, should probably define mocha and qunit-mocha-ui
// as projects dependencies somewhere, rather than having them installed
// globally


// todo - i'd really like to be able to see an assertion count, and potentially
// even a list of all assertions.  qunit-mocha-ui already wraps and counts
// asserts, so i suspect i would just need to build by own reporter that
// hooks in with that

// mocha and qunit-mocha-ui are installed globally right now
var Mocha = require("mocha");
Mocha.interfaces["qunit-mocha-ui"] = require("qunit-mocha-ui");

// point to node_modules in magnate/server
var node_modules = process.env.HOME + "/magnate/server/node_modules";
var requirejs = require(node_modules + "/requirejs");
global._ = require(node_modules + "/underscore");


// create our mocha
var mocha = new Mocha({
  ui: "qunit-mocha-ui",
  reporter: "spec",
  timeout: 7000,
  debug: true // todo: only if --debug or --debug-brk?
});

// todo - make this a script and have way to run a single file from command line?
// add files
mocha.addFile(__dirname + "/tests/gameStoreTest.js");
mocha.addFile(__dirname + "/tests/lobbyTest.js");

// configure requirejs and load require paths
requirejs.config({
  baseUrl: process.env.HOME + "/magnate"
});

requirejs(["requirePaths"], function () {

  // run tests
  mocha.run(function (failures) {
    process.exit(failures);
  });
  
});
