var requirejs;
var isNode = (typeof exports !== "undefined");

if (isNode) {  
  var node_modules = process.env.HOME + "/magnate/server/node_modules";
  requirejs = require(node_modules + "/requirejs");
} else {
  requirejs = require;
  suite = QUnit.module;
}

suite("GameStore");
var timeoutUnlessDebug = function (ctx, timeout) {
  if (!isNode) {
    return;
  }
  if (_.contains(process.execArgv, "--debug-brk")) {
    ctx.timeout(1000000);
  } else if (timeout) {
    ctx.timeout(timeout);
  }
};

// todo - have tests better cleanup db

// todo - duplicated with lobbyTest currently
function multiStoreTest(description, storeName, fn) {
  test(description + ": TestDb", function () {
    timeoutUnlessDebug(this);
    stop();
    requirejs(["testutils/TestDb"], function (TestDb) {
      var store = new TestDb("testdb", storeName);
      fn(store);
      start();
    });
  });

  test(description + ": GameStore", function () {
    if (!isNode) {
      ok(true, "GameStore depends on node.  Test not being run in browser");
      return;
    }

    timeoutUnlessDebug(this);
    stop();
    requirejs(["server/src/GameStore"], function (GameStore) {
      var store = new GameStore("testdb", storeName);
      fn(store);
      start();
    });
  });
}

multiStoreTest("game store test", "gametest1", function (store) {
  stop();
  requirejs([
    "model/game",
    "testlib/async",
    "testutils/gameCreation",
  ], function (Game, async, gameCreation) {
    var game = gameCreation.createGame("AsAndBsInHand");
    equal(game.currentPhase(), "placing");

    var itemId;
    async.waterfall([
      function (cb) {
        store._empty(cb);
      }, function (numRemoved, cb) {
        store.getAllItems(cb);
      },
      function (items, cb) {
        equal(items.length, 0);
        cb(null);
      },
      function (cb) {
        store.addItem("current-game", JSON.stringify(game), cb);
      },
      function (id, cb) {
        itemId = id;
        store.getItemInfo(itemId, cb);
      },
      function (itemInfo, cb) {
        equal(itemInfo.waterline, 1);
        deepEqual(itemInfo._id, itemId);
        var newGame = Game.createFromJSON(itemInfo.data);
        equal(JSON.stringify(game._board), JSON.stringify(newGame._board),
          "game boards are the same");

        var tile = "1A"; 
        var pkey = game.currentPlayer().key();
        var action = Game.placeAction(pkey, tile);
        newGame.performAction(action);
        equal(newGame.currentPhase(), "buying");

        store.updateItem(itemId, 1, null, JSON.stringify(newGame), cb);
      },
      function (numUpdated, something, cb) {
        store.getItemInfo(itemId, cb);
      },
      function (itemInfo, cb) {
        equal(itemInfo.waterline, 2);
        deepEqual(itemInfo._id, itemId);
        var newGame = Game.createFromJSON(itemInfo.data);
        equal(newGame.currentPhase(), "buying");
        equal(newGame._board._grid["1A"], "unclaimed");

        cb(null);
      }
    ], function (err, results) {
      equal(err, null);
      ok(true, "big finish");
      console.log("finished");
      start();
    });
  });
});

multiStoreTest("get non-existent item", "noitem", function (store) {
  stop();
  requirejs([
    "testlib/async"
  ], function (async) {

    async.waterfall([
      function (cb) {
        store._empty(cb);
      },
      function (numDocsRemoved, cb) {
        store.getItemInfo("52b1dc18ac01da0219000001", function (err, result) {
          console.log("callback");
          equal(err, "NoItem");
          equal(result, null);
          cb(null);
        });
      }
    ], function (err, results) {
      equal(err, null);
      start();
    });
  });
});

multiStoreTest("two quick reads", "twoQuick", function (store) {
  stop();
  
  var waitCount = 2;
  function resumeOnSecondCallback(err, results) {
    equal(err, null);
    waitCount--;
    if (waitCount === 0) {
      ok(true, "finished");
      start();
    }
  };

  store.getAllItems(resumeOnSecondCallback);
  store.getAllItems(resumeOnSecondCallback);
});
