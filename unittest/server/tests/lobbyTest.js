var requirejs;
var isNode = (typeof exports !== "undefined");

if (isNode) {  
  var node_modules = process.env.HOME + "/magnate/server/node_modules";
  requirejs = require(node_modules + "/requirejs");
} else {
  requirejs = require;
  suite = QUnit.module;
}

suite("lobby");


var timeoutUnlessDebug = function (ctx, timeout) {
  if (!isNode) {
    return;
  }
  if (_.contains(process.execArgv, "--debug-brk")) {
    ctx.timeout(1000000);
  } else if (timeout) {
    ctx.timeout(timeout);
  }
};

// todo - duplicated with lobbyTest currently
function multiStoreTest(description, storeName, fn) {
  test(description + ": TestDb", function () {
    timeoutUnlessDebug(this);
    stop();
    requirejs(["testutils/TestDb"], function (TestDb) {
      var store = new TestDb("testdb", storeName);
      fn(store);
      start();
    });
  });

  test(description + ": GameStore", function () {
    if (!isNode) {
      ok(true, "GameStore depends on node.  Test not being run in browser");
      return;
    }

    timeoutUnlessDebug(this);
    stop();
    requirejs(["server/src/GameStore"], function (GameStore) {
      var store = new GameStore("testdb", storeName);
      fn(store);
      start();
    });
  });
}

multiStoreTest("create and get games", "lobby1", function (store) {
  stop();
  requirejs([
    "server/src/lobby",
    "testutils/debugIdentities",
    "testlib/async"
  ], function (lobby, debugIdentities, async) {
       
    lobby.attach(store);

    var user1 = debugIdentities.getIdentity(0);
    var user2 = debugIdentities.getIdentity(1);

    // todo - unit test otherGames portion of getPendingGames
    // todo - ensure getPendingGames doesnt return running games

    async.waterfall([
      function (cb) {
        store._empty(cb);
      },
      function (numRemoved, cb) {
        // create a game for user1
        lobby.createPendingGame(user1, {}, cb);
      },
      function (gameId, cb) {
        lobby.getPendingGames(user1, cb);        
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 1);
        equal(otherGames.length, 0);
        var game = JSON.parse(myGames[0].data);
        equal(game.players[0].name, user1.name);
        equal(game.players[0].pkey, user1.pkey);
        equal(game.totalPlayers, 5, "default game size is 5");

        // create a second game for user1
        lobby.createPendingGame(user1, {numPlayers: 4}, cb);
      },
      function (gameId, cb) {
        lobby.getPendingGames(user1, cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 2);
        var playerCounts = _.map(myGames, function (itemInfo) {
          return JSON.parse(itemInfo.data).totalPlayers;
        });
        ok(_.contains(playerCounts, 4), "added a game with numPlayers = 4");

        // create a game for user 1
        lobby.createPendingGame(user2, {}, cb);
      },
      function (gameId, cb) {
        lobby.getPendingGames(user1, cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 2, "user 1 still only has two games");

        lobby.getPendingGames(user2, cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 1, "user 2 has their game");
        cb(null);
      }
    ], function (err, results) {
      equal(err, null, err);
      ok(true, "finished");
      start();
    });
  });
});

/*
multiStoreTest("join game", "lobby2", function (pendingStore, runningStore) {
  stop();
  requirejs([
    "server/src/lobby",
    "testutils/debugIdentities",
    "testlib/async"
  ], function (lobby, debugIdentities, async) {

    lobby.attach(pendingStore, runningStore);
    
    var user1 = debugIdentities.getIdentity(0);
    var user2 = debugIdentities.getIdentity(1);
    var user3 = debugIdentities.getIdentity(2);

    var gameId;
    async.waterfall([
      function (cb) {
        pendingStore._empty(cb);
      },
      function (numRemoved, cb) {
        runningStore._empty(cb);
      },
      function (numRemoved, cb) {
        lobby.createPendingGame(user1, {numPlayers: 3}, cb);
      },
      function (id, cb) {
        gameId = id;
        lobby.getUserGames(user1, cb);
      },
      function (games, cb) {
        equal(games.length, 1, "user 1 game exists");

        lobby.getUserGames(user2, cb);
      },
      function (games, cb) {
        equal(games.length, 0, "user 2 has no games yet");

        lobby.joinGame(gameId, user2, cb);
      },
      function (cb) {
        lobby.getUserGames(user2, cb);
      },
      function (games, cb) {
        equal(games.length, 1, "user 2 in game now");
        cb(null);
      },
      function (cb) {
        lobby.joinGame(gameId, user3, cb);
      },
      function (cb) {
        // todo - probably want a lobby function
        runningStore.getAllGames(cb);
      },
      function (runningGames, cb) {
        equal(runningGames.length, 1);
        cb(null);
      }
    ], function (err, results) {
      equal(err, null, "err: " + err);
      ok(true, "finished");
      start();
    });    
  });

  

  // todo - test where update fails bc someone else updated between our query
  // and update

  // todo - test where we hit num players

  // todo - scenario where our addItem of the running game fails, leaving us
  // with a full pending game and no running game

});
*/