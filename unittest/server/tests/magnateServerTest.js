require([
  "testutils/TestIO",
  "server/src/MagnateServer",
  "testutils/TestDb",
  "testlib/async",
  "testutils/debugIdentities"
], function (TestIO, MagnateServer, TestDb, async, debugIdentities) {
  
  QUnit.module("MagnateServer");
  
  asyncTest("IO test", function () {
    var io = new TestIO();
    var server = new MagnateServer(io, TestDb);

    var socket1 = io.connect("server");
    var socket2 = io.connect("server");
    var socket3 = io.connect("server");

    var user1 = debugIdentities.getIdentity(0);
    var user2 = debugIdentities.getIdentity(1);
    var user3 = debugIdentities.getIdentity(2);

    socket1.emit("user-signin", user1);
    socket2.emit("user-signin", user2);
    socket3.emit("user-signin", user3);
    
    async.waterfall([
      function (cb) {
        socket1.emit("create-game", {numPlayers:3}, cb);
      },
      function (gameId, cb) {
        socket1.emit("get-pending-games", cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 1);
        equal(otherGames.length, 0);
        
        socket2.emit("get-pending-games", cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 0);
        equal(otherGames.length, 1);
        
        socket2.emit("join-game", otherGames[0].id, cb);
      },
      function (cb) {
        socket2.emit("get-pending-games", cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 1);
        equal(otherGames.length, 0);

        socket3.emit("get-pending-games", cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 0);
        equal(otherGames.length, 1);
        socket3.emit("join-game", otherGames[0].id, cb);
      },
      function (cb) {
        socket3.emit("get-pending-games", cb);
      },
      function (myGames, otherGames, cb) {
        equal(myGames.length, 0);
        equal(otherGames.length, 0);

        socket3.emit("get-running-games", cb);
      },
      function (games, cb) {
        equal(games.length, 1);

        socket2.emit("get-running-games", cb);
      },
      function (games, cb) {
        equal(games.length, 1);

        socket1.emit("get-running-games", cb);
      },
      function (games, cb) {
        equal(games.length, 1);

        cb(null);
      }
      
    ], function (err, results) {
      equal(err, null);
      start();
    });
  });

});