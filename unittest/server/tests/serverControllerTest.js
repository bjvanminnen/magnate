require([
  "server/src/serverController",
  "testutils/TestDb",
  "model/game",
  "testutils/Waiter",
  "shared/notifier",
  "testlib/async",
  "testutils/TestUtils",
  "testutils/SocketSim",
  "testutils/debugIdentities",
  "testutils/gameCreation"
], function (ServerController, TestDb, Game, Waiter, Notifier, async,
  TestUtils, SocketSim, debugIdentities, gameCreation) {

  QUnit.module("serverController");

  var exceptional = TestUtils.exceptional;

  function connectClientSocket(controller, ident) {
    var socket = new SocketSim();
    controller.addSocket(socket.server);
    socket.client.emit("user-signin", ident);
    return socket;
  }

  asyncTest("draw update", function () {
    var bob = {}, mary = {};

    // create our game
    var serverGame = gameCreation.createGame("AsAndBsInHand");
    bob.pkey = serverGame.players()[0].key();
    mary.pkey = serverGame.players()[1].key();

    TestUtils.doPlaceTile(serverGame, "1A", bob.pkey);    

    // create a server with two client controllers
    var testDb = new TestDb("dbName", "collectionName");
    var server = new ServerController(testDb);
    bob.socket = connectClientSocket(server, debugIdentities.getIdentity(0));
    mary.socket = connectClientSocket(server, debugIdentities.getIdentity(1));

    var bobGotUpdate = new Notifier(this);
    bob.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        bobGotUpdate.notify({action: action});
      }
    });

    var maryGotUpdate = new Notifier(this);
    mary.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        maryGotUpdate.notify({action: action});
      }
    });

    var gameId;
    async.waterfall([
      exceptional(function (callback) {
        testDb.addItem(testDb.ItemType.Running, JSON.stringify(serverGame),
          function (err, id) {
            gameId = id;
            callback(null);
          }
        );
      }),
      exceptional(function (callback) {
        // Emit an end turn action, which should prompt the server to send
        // back an update action with the draw tile
        bob.socket.client.emit("perform-action", gameId,
          Game.endTurnAction(bob.pkey));

        maryGotUpdate.subscribe(function (src, data) {
          ok(false, "Mary should not receive an update action");
        });

        bobGotUpdate.subscribe(function (src, data) {
          equal(data.action.verb, "update");
          equal(data.action.arguments.type, "drawUpdate");
          notEqual(data.action.arguments.info.drawnTile, null);
          notEqual(data.action.arguments.info.drawnTile, Game.PrivateMarker);
          callback(null);
        });
        
      })
    ], function (err, results) {
      equal(err, null);
      start();
    });
  });

  asyncTest("merge update", function () {
    var bob = {}, mary = {};

    // create our game
    var serverGame = gameCreation.createGame("AsAndBsInHand");
    bob.pkey = serverGame.players()[0].key();
    mary.pkey = serverGame.players()[1].key();

    TestUtils.doPlaceTile(serverGame, "1A", bob.pkey);
    TestUtils.doEndTurn(serverGame, bob.pkey);
    TestUtils.doPlaceTile(serverGame, "1B", mary.pkey);
    TestUtils.doFoundChain(serverGame, Game.HOTELS[0], mary.pkey);
    TestUtils.doEndTurn(serverGame, mary.pkey);
    TestUtils.doPlaceTile(serverGame, "3A", bob.pkey);
    TestUtils.doEndTurn(serverGame, bob.pkey);
    TestUtils.doPlaceTile(serverGame, "3B", mary.pkey);
    TestUtils.doFoundChain(serverGame, Game.HOTELS[1], mary.pkey);
    TestUtils.doEndTurn(serverGame, mary.pkey);
    TestUtils.doPlaceTile(serverGame, "2A", bob.pkey);
    equal(serverGame.currentPhase(), "merging");

    // create a server with two client controllers
    // todo - idents should be better aligned with gameCreation probably
    var testDb = new TestDb("dbName", "collectionName");
    var server = new ServerController(testDb);
    bob.socket = connectClientSocket(server, debugIdentities.getIdentity(0));
    mary.socket = connectClientSocket(server, debugIdentities.getIdentity(1));

    var bobGotUpdate = new Notifier(this);
    bob.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        bobGotUpdate.notify({action: action});
      }
    });

    var maryGotUpdate = new Notifier(this);
    mary.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        maryGotUpdate.notify({action: action});
      }
    });

    var gameId;
    async.waterfall([
      exceptional(function (callback) {
        testDb.addItem(testDb.ItemType.Running, JSON.stringify(serverGame),
          function (err, id) {
            gameId = id;
            // todo - hack to get mary to join room on server
            mary.socket.client.emit("get-game", gameId, function (err, json) {
            });
            callback(null);
          }
        );
      }),
      exceptional(function (callback) {      
        var bobUpdated = false, maryUpdated = false;

        function onUpdate(data) {
          equal(data.action.verb, "update");
          equal(data.action.arguments.type, "mergeUpdate");
          TestUtils.listsEquivalent(
            data.action.arguments.info.stockCounts, [0, 1]);
          TestUtils.listsEquivalent(
            data.action.arguments.info.playerIndices, [0, 1]);

          if (bobUpdated && maryUpdated) {
            callback(null);
          }
        }

        bobGotUpdate.subscribe(function (src, data) {
          bobUpdated = true;
          onUpdate(data);
        });
        maryGotUpdate.subscribe(function (src, data) {
          maryUpdated = true;
          onUpdate(data);
        });

        // place merge tile
        bob.socket.client.emit("perform-action", gameId,
          Game.mergeAction(bob.pkey, Game.HOTELS[0], Game.HOTELS[1]));
      })
    ], function (err, results) {
      equal(err, null);
      start();
    });
  });

  asyncTest("endGame", function () {
    var bob = {}, mary = {};

    // create our game
    var serverGame = gameCreation.createGame("endableGame");
    bob.pkey = serverGame.players()[0].key();
    mary.pkey = serverGame.players()[1].key();
    TestUtils.doPlaceTile(serverGame, "12I");
    TestUtils.doEndGame(serverGame);
    equal(serverGame.currentPhase(), "buying");
    equal(serverGame._gameEnding, true);
    equal(serverGame.currentPlayer().name(), "Mary");

    // create a server with two client controllers
    // todo - idents should be better aligned with gameCreation probably
    var testDb = new TestDb("dbName", "collectionName");
    var server = new ServerController(testDb);
    bob.socket = connectClientSocket(server, debugIdentities.getIdentity(0));
    mary.socket = connectClientSocket(server, debugIdentities.getIdentity(1));

    var bobGotUpdate = new Notifier(this);
    bob.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        bobGotUpdate.notify({action: action});
      }
    });

    var maryGotUpdate = new Notifier(this);
    mary.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        maryGotUpdate.notify({action: action});
      }
    });

    var gameId;
    async.waterfall([
      function (cb) {
        testDb.addItem(testDb.ItemType.Running, JSON.stringify(serverGame), cb)
      },
      function (id, cb) {
        gameId = id;
        // todo - hack to get bob to join room on server
        bob.socket.client.emit("get-game", gameId, cb);
      },
      function (gameJson, cb) {
        var bobUpdated = 0, maryUpdated = 0;

        function onEndGameUpdate(data) {
          equal(data.action.verb, "update");
          equal(data.action.arguments.type, "endGameUpdate");
          ok(TestUtils.listsEquivalent(
            data.action.arguments.info.playerMoney, [17200, 30000]));

          if (bobUpdated === 1 && maryUpdated === 2) {
            cb(null);
          }
        }

        bobGotUpdate.subscribe(function (src, data) {
          bobUpdated++;
          onEndGameUpdate(data);
        });

        maryGotUpdate.subscribe(function (src, data) {
          maryUpdated++;
          if (maryUpdated === 1) {
            equal(data.action.arguments.type, "drawUpdate",
              "mary will first get a drawUpdate");
          } else {
            onEndGameUpdate(data);
          }
        });

        mary.socket.client.emit("perform-action", gameId,
          Game.endTurnAction(mary.pkey));
      }
    ], function (err, results) {
      equal(err, null);
      start();
    });
  });

  asyncTest("swap update", function () {
    var bob = {}, mary = {};

    // create our game
    var serverGame = gameCreation.createGame("swappable");
    bob.pkey = serverGame.players()[0].key();
    mary.pkey = serverGame.players()[1].key();

    TestUtils.doPlaceTile(serverGame, "12I");
    TestUtils.doEndTurn(serverGame);
    equal(serverGame._noTilesPlayable, true, "All tiles in hand would cause merge of two safe chains");

    // create a server with two client controllers
    var testDb = new TestDb("dbName", "collectionName");
    var server = new ServerController(testDb);
    bob.socket = connectClientSocket(server, debugIdentities.getIdentity(0));
    mary.socket = connectClientSocket(server, debugIdentities.getIdentity(1));

    var maryGotUpdate = new Notifier(this);
    mary.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        maryGotUpdate.notify({action: action});
      }
    });

    var bobGotUpdate = new Notifier(this);
    bob.socket.client.on("successful-action", function (gameId, action) {
      if (action.verb === "update") {
        bobGotUpdate.notify({action: action});
      }
    });

    var gameId;
    async.waterfall([
      exceptional(function (callback) {
        testDb.addItem(testDb.ItemType.Running, JSON.stringify(serverGame),
          function (err, id) {
            gameId = id;
            // todo - hack to get bob to join room on server
            bob.socket.client.emit("get-game", gameId, function (err, json) {
            });
            callback(null);
          }
        );
      }),
      exceptional(function (callback) {
        var count = 0;
        var startTiles = serverGame.players()[1].tiles().slice();
        
        maryGotUpdate.subscribe(function (src, data) {
          equal(data.action.arguments.type, "swapUpdate");
          var newTiles = data.action.arguments.info.tiles;
          equal(_.intersection(startTiles, newTiles).length, 0,
            "none of the same tiles");
          equal(_.intersection([Game.PrivateMarker], newTiles).length, 0,
            "no private tiles");
          callback(null);
        });

        bobGotUpdate.subscribe(function (src, data) {
          ok(false, "Bob shouldn't get update for Mary's swap");
        });

        mary.socket.client.emit("perform-action", gameId,
          Game.swapAction(mary.pkey));

        // todo - make sure bob doesnt get update
      })
    ], function (err, results) {
      equal(err, null);
      start();
    });       

  });
});