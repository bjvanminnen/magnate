// todo - still an issue with notifier test.  in general, i dont think running
// these through node is the way to go

var requirejs = require("requirejs");

// todo - switch to lodash
global._ = require("underscore");

// hack implemented for semi-silly reasons (VS didnt like seeing throws)
global.assertThrows = throws;

requirejs.config({
  baseUrl: process.cwd() + "/.."
});

console.log("cwd: " + process.cwd());


requirejs(["../requirePaths.js"], function () {

  var tests = [
    'sharedTest/tilebagTest',
    'sharedTest/gameboardTest',
    'sharedTest/playerTest',
    'sharedTest/gameTest',
    'clientTest/mergeTests',
    'sharedTest/actionTest',
    'sharedTest/notifierTest',
    'clientTest/localControllerTest',
    'clientTest/networkControllerTest',
    'sharedTest/utilsTest',
    'serverTest/storeTest',
    'sharedTest/gameLibraryTest',
  ];

  requirejs(tests); 
});

