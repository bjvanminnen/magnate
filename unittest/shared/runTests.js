#!/usr/bin/env node

// This is a way to run (a subset) of my unit tests in node using node-qunit.

var file = process.argv[2] || "nodeTests.js";
if (file === "-d") {
  file = process.argv[3] || "nodeTests.js";
}

var debug = process.argv.indexOf("-d") !== -1;
console.log("debug =  " + debug);
console.log("file = " + file);

var runner = require("qunit");
runner.setup({
  log: {
    assertions: false,
    errors: true,
    tests: true,
    summary: true,
    globalSummary: false,
    testing: true
  },
  breakOnAttach: debug
});

runner.run({
  code: "qcode.js",
  tests: file
});
