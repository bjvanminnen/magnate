require([
  "model/game",
  "model/player",
  "testutils/TestTileBag",
  "testutils/gameCreation"
], function (Game, Player, TestTileBag, gameCreation) {
  QUnit.module("action tests");

  var game;
  
  function validateMoney(bobMoney, maryMoney, johnMoney) {
    var players = game.players();
    equal(players[0].money(), bobMoney, "bob's money");
    equal(players[1].money(), maryMoney, "mary's money");
    equal(players[2].money(), johnMoney, "john's money");
  }

  function validateStock(hotel, bobShares, maryShares, johnShares) {
    var players = game.players();
    equal(players[0].stockCount(hotel), bobShares, "bob's shares in " + hotel);
    equal(players[1].stockCount(hotel), maryShares, "mary's shares in " + hotel);
    equal(players[2].stockCount(hotel), johnShares, "john's shares in " + hotel);
  }

  function validateHotelSize(hotel, size) {
    equal(game._board.chainSize(hotel), size);
  }

  test("simple merge via actions", function () {
    game = gameCreation.createFromNames(["Bob", "Mary", "John"]);

    // Override our tilebag to remove randomness
    game._tilebag = new TestTileBag();

    // queue up some tiles
    game._tilebag.enqueue(["1A", "2A", "3A", "4A", "5A", "6A"]); // Bob's
    game._tilebag.enqueue(["1B", "2B", "3B", "4B", "5B", "6B"]); // Mary's
    game._tilebag.enqueue(["1C", "1D", "3C", "4C", "5C", "6C"]); // John's


    game.start();
    var players = game.players();

    var bob = players[0].key();
    var mary = players[1].key();
    var john = players[2].key();

    // bob turn 1
    game.performAction(Game.placeAction(bob, "1A"));
    game.performAction(Game.endTurnAction(bob));
    validateMoney(6000, 6000, 6000);

    // mary turn 1
    game.performAction(Game.placeAction(mary, "1B"));
    game.performAction(Game.foundAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(mary));
    validateMoney(6000, 5400, 6000);
    validateStock(Game.HOTELS[0], 0, 4, 0);

    // john turn 1
    game.performAction(Game.placeAction(john, "1C"));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(john));
    validateMoney(6000, 5400, 5100);
    validateStock(Game.HOTELS[0], 0, 4, 3);

    // bob turn 2
    game.performAction(Game.placeAction(bob, "3A"));
    game.performAction(Game.endTurnAction(bob));
    validateMoney(6000, 5400, 5100);
    validateStock(Game.HOTELS[0], 0, 4, 3);

    // mary turn 2
    game.performAction(Game.placeAction(mary, "3B"));
    game.performAction(Game.foundAction(mary, Game.HOTELS[2]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[2]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[2]));
    game.performAction(Game.endTurnAction(mary));
    validateMoney(6000, 4500, 5100);
    validateStock(Game.HOTELS[0], 0, 5, 3);
    validateStock(Game.HOTELS[2], 0, 3, 0);

    // john turn 2
    game.performAction(Game.placeAction(john, "1D"));
    game.performAction(Game.buyAction(john, Game.HOTELS[2]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(john));
    validateMoney(6000, 4500, 4000);
    validateStock(Game.HOTELS[0], 0, 5, 5);
    validateStock(Game.HOTELS[2], 0, 3, 1);
    
    // bob turn 3
    game.performAction(Game.placeAction(bob, "2A"));
    game.performAction(Game.mergeAction(bob, Game.HOTELS[0], Game.HOTELS[2]));
    validateMoney(6000, 7500, 5500);

    // resolve merge
    game.performAction(Game.resolveMergeAction(mary, 1, 0, 2));
    validateStock(Game.HOTELS[0], 0, 6, 5);
    validateStock(Game.HOTELS[2], 0, 1, 1);
    game.performAction(Game.resolveMergeAction(john, 1, 0, 0));
    validateStock(Game.HOTELS[0], 0, 6, 5);
    validateStock(Game.HOTELS[2], 0, 1, 1);
    
    game.performAction(Game.endTurnAction(bob));
  });

  test("three way merge via actions", function () {
    game = gameCreation.createFromNames(["Bob", "Mary", "John"]);

    // Override our tilebag to remove randomness
    game._tilebag = new TestTileBag();

    // queue up some tiles
    game._tilebag.enqueue(["1A", "2D", "4B", "2B", "1I", "2I"]); // Bob's
    game._tilebag.enqueue(["2A", "2E", "5B", "3I", "4I", "5I"]); // Mary's
    game._tilebag.enqueue(["2C", "3B", "6B", "6I", "7I", "8I"]); // John's

    game.start();
    var players = game.players();

    var bob = players[0].key();
    var mary = players[1].key();
    var john = players[2].key();

    ok(true, "bob turn 1");
    game.performAction(Game.placeAction(bob, "1A"));
    game.performAction(Game.endTurnAction(bob));
    validateMoney(6000, 6000, 6000);

    ok(true, "mary turn 1");
    game.performAction(Game.placeAction(mary, "2A"));
    game.performAction(Game.foundAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(mary));
    validateHotelSize(Game.HOTELS[0], 2);
    validateMoney(6000, 5400, 6000);
    validateStock(Game.HOTELS[0], 0, 4, 0);

    ok(true, "john turn 1");
    game.performAction(Game.placeAction(john, "2C"));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(john));
    validateMoney(6000, 5400, 5400);
    validateHotelSize(Game.HOTELS[0], 2);
    validateStock(Game.HOTELS[0], 0, 4, 3);

    ok(true, "bob turn 2");
    game.performAction(Game.placeAction(bob, "2D"));
    game.performAction(Game.foundAction(bob, Game.HOTELS[2]));
    game.performAction(Game.buyAction(bob, Game.HOTELS[2]));
    game.performAction(Game.buyAction(bob, Game.HOTELS[2]));
    game.performAction(Game.buyAction(bob, Game.HOTELS[2]));
    game.performAction(Game.endTurnAction(bob));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 2);
    validateMoney(5100, 5400, 5400);
    validateStock(Game.HOTELS[0], 0, 4, 3);
    validateStock(Game.HOTELS[2], 4, 0, 0);

    ok(true, "mary turn 2");
    game.performAction(Game.placeAction(mary, "2E"));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[2]));
    game.performAction(Game.endTurnAction(mary));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 3);
    validateMoney(5100, 4600, 5400);
    validateStock(Game.HOTELS[0], 0, 6, 3);
    validateStock(Game.HOTELS[2], 4, 1, 0);

    ok(true, "john turn 2");
    game.performAction(Game.placeAction(john, "3B"));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.buyAction(john, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(john));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 3);
    validateMoney(5100, 4600, 4800);
    validateStock(Game.HOTELS[0], 0, 6, 6);
    validateStock(Game.HOTELS[2], 4, 1, 0);

    ok(true, "bob turn 3");
    game.performAction(Game.placeAction(bob, "4B"));
    game.performAction(Game.foundAction(bob, Game.HOTELS[6]));
    game.performAction(Game.buyAction(bob, Game.HOTELS[2]));
    game.performAction(Game.buyAction(bob, Game.HOTELS[6]));
    game.performAction(Game.buyAction(bob, Game.HOTELS[6]));
    game.performAction(Game.endTurnAction(bob));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 3);
    validateHotelSize(Game.HOTELS[6], 2);
    validateMoney(3900, 4600, 4800);
    validateStock(Game.HOTELS[0], 0, 6, 6);
    validateStock(Game.HOTELS[2], 5, 1, 0);
    validateStock(Game.HOTELS[6], 3, 0, 0); // 

    ok(true, "mary turn 3");
    game.performAction(Game.placeAction(mary, "5B"));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.buyAction(mary, Game.HOTELS[0]));
    game.performAction(Game.endTurnAction(mary));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 3);
    validateHotelSize(Game.HOTELS[6], 3);
    validateMoney(3900, 4000, 4800);
    validateStock(Game.HOTELS[0], 0, 9, 6);
    validateStock(Game.HOTELS[2], 5, 1, 0);
    validateStock(Game.HOTELS[6], 3, 0, 0);

    ok(true, "john turn 3");
    game.performAction(Game.placeAction(john, "6B"));
    game.performAction(Game.buyAction(john, Game.HOTELS[2]));
    game.performAction(Game.buyAction(john, Game.HOTELS[2]));
    game.performAction(Game.buyAction(john, Game.HOTELS[6]));
    game.performAction(Game.endTurnAction(john));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 3);
    validateHotelSize(Game.HOTELS[6], 4);
    validateMoney(3900, 4000, 3400);
    validateStock(Game.HOTELS[0], 0, 9, 6);
    validateStock(Game.HOTELS[2], 5, 1, 2);
    validateStock(Game.HOTELS[6], 3, 0, 1);

    ok(true, "bob turn 4");
    game.performAction(Game.placeAction(bob, "2B"));
    game.performAction(Game.mergeAction(bob, Game.HOTELS[6], Game.HOTELS[2]));
    validateHotelSize(Game.HOTELS[0], 2);
    validateHotelSize(Game.HOTELS[2], 0);
    validateHotelSize(Game.HOTELS[6], 7);
    validateMoney(7900, 4000, 5400);
    
    game.performAction(Game.resolveMergeAction(bob, 1, 0, 4)); 
    validateStock(Game.HOTELS[2], 1, 1, 2);
    validateStock(Game.HOTELS[6], 5, 0, 1);
    
    game.performAction(Game.resolveMergeAction(mary, 0, 1, 0));
    validateMoney(7900, 4400, 5400);
    validateStock(Game.HOTELS[2], 1, 0, 2);

    game.performAction(Game.resolveMergeAction(john, 0, 0, 2));
    validateStock(Game.HOTELS[2], 1, 0, 0);
    validateStock(Game.HOTELS[6], 5, 0, 2);

    game.performAction(Game.mergeAction(bob, Game.HOTELS[6], Game.HOTELS[0]));
    validateHotelSize(Game.HOTELS[0], 0);
    validateHotelSize(Game.HOTELS[2], 0);
    validateHotelSize(Game.HOTELS[6], 10);

    game.performAction(Game.resolveMergeAction(mary, 1, 0, 8));
    validateStock(Game.HOTELS[0], 0, 1, 6);
    validateStock(Game.HOTELS[6], 5, 4, 2);

    game.performAction(Game.resolveMergeAction(john, 0, 0, 6));
    validateStock(Game.HOTELS[0], 0, 1, 0);
    validateStock(Game.HOTELS[6], 5, 4, 5);


  });

  // todo - attempting to do things out of order

  // todo - more tests around bad actions
});
