require([
  "model/game",
  "testutils/gameCreation"
], function (Game, gameCreation) {
  QUnit.module("game library tests");

  test("create all games in our library", function () {
    _.each(_.keys(gameCreation.library), function (gameName) {
      ok(true, gameName);
      gameCreation.createGame(gameName);
    });
  });
});
