require([
  "model/game",
  "model/tileBag",
  "testutils/gameCreation",
  "testutils/TestUtils"
], function (Game, TileBag, gameCreation, TestUtils) {
  QUnit.module("game tests");

  test("construction", function () {
    // some basic validation of a created game
    var playersInit = ["Bob", "Mary", "John", "Jane"];
    var game = gameCreation.createFromNames(playersInit);
    game.start();
    var players = game.players();
    equal(game.currentPhase(), "placing");
    equal(players.length, playersInit.length);

    for (var i = 0; i < playersInit.length; i++) {
      equal(players[i].name(), playersInit[i]);
      equal(players[i].tiles().length, 6);
    }

    equal(_.keys(game._hotels).length, 7);
    _.each(_.values(game._hotels), function (val) {
      equal(val.stock, 25);
    });

    equal(game._currentPlayerIndex, 0);
  });

  test("placeTile", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();
    var players = game.players();

    equal(game.currentPhase(), "placing");
    equal(game._currentPlayerIndex, 0);
    equal(game.currentPlayer()._name, "Bob");

    // can place any one of the tiles they have
    var tiles = players[0].tiles();
    for (var i = 0; i < tiles.length; i++) {
      var clone = TestUtils.clone(game);
      TestUtils.doPlaceTile(clone, tiles[i]);
      equal(clone.currentPhase(), "buying");
      equal(clone.players()[0].tiles().length, tiles.length - 1);
      equal(clone._currentPlayerIndex, 0);
    }

    var unownedTile;
    for (var i = 1; i <= 12; i++) {
      var tile = i + "A";
      if (!_.contains(tiles, tile)) {
        unownedTile = tile;
        break;
      }
    }
    
    var key = game.currentPlayer().key();
    assertThrows(function () {
      game.performAction(Game.placeAction(key, unownedTile));
    }, /InvalidOperation/, "cant place tile we dont have");

    assertThrows(function () {
      var clone = TestUtils.clone(game);
      clone._players[0]._tiles[0] = "MT"; // hack to have MT tile
      clone.performAction(Game.placeAction(key, TileBag.EmptyMarker));
    }, /InvalidArgument/, "Can't place an MT tile");

    assertThrows(function () {
      var clone = TestUtils.clone(game);
      clone._players[0]._tiles = ["PM", "PM", "PM", "PM", "PM", "PM"];
      clone.performAction(Game.placeAction(key, Game.PrivateMarker));
    }, /InvalidArgument/, "Can't place a PM tile");

    

    // place a tile
    TestUtils.doPlaceTile(game, tiles[0]);
    equal(game.currentPhase(), "buying");
    assertThrows(function () {
      TestUtils.doPlaceTile(game, tiles[1]);
    }, /Not in placing phase/);

    // illegalPlacement tested elsewhere

    // placing tiles that cause merges/founding tested elsewhere
  });

  test("placeTile: None", function () {
    
    var game = gameCreation.createGame("swappable");
    assertThrows(function () {
      TestUtils.doPlaceTile(game, "None");
    }, /InvalidArgument/, "Can't place 'None' when we have playable tiles");

    // hack player to have an MT tile
    game._players[0]._tiles[0] = "MT";
    assertThrows(function () {
      TestUtils.doPlaceTile(game, "None");
    }, /InvalidArgument/, "Can't place 'None' when we have playable tiles" +
      "even if we have an MT tile");

    // can place none if out of tiles
    game._players[0]._tiles = ["MT", "MT", "MT", "MT", "MT", "MT"];
    TestUtils.doPlaceTile(game, "None");
    equal(game.currentPhase(), "buying");

  });

  test("endTurn", function () {
    var baseGame = gameCreation.createGame("AsAndBsInHand");
    var bobKey = baseGame.players()[0].key();
    var maryKey = baseGame.players()[1].key();
    var game;
    TestUtils.doPlaceTile(baseGame, "1A", bobKey);    

    // public game
    // - lastDrawn visible
    // - current player gets tile
    // - current player moves
    // - phase becomes placing
    game = TestUtils.clone(baseGame);
    // hack tilebag so that we know what we'll get    
    game._tilebag._tiles = ["12I"];
    TestUtils.doEndTurn(game, bobKey);
    equal(game._lastDrawn, "12I");
    equal(game.players()[0].tiles().length, 6);
    equal(game.currentPlayer().name(), "Mary");
    equal(game.currentPhase(), "placing");

    // private game - different player
    // - lastDrawn hidden
    // - current player gets tile (PM)
    // - current player moves
    // - phase becomes placing
    game = Game.createFromJSON(JSON.stringify(baseGame.privatized(maryKey, true)));
    TestUtils.doEndTurn(game, bobKey);
    equal(game._lastDrawn, Game.PrivateMarker);
    equal(game.players()[0].tiles().length, 6);
    equal(game.currentPlayer().name(), "Mary");
    equal(game.currentPhase(), "placing");

    // private game - this player
    // - last drawn hidden
    // - current player doesnt get tile
    // - current player doesnt change
    // - phase becomes awaiting draw
    game = Game.createFromJSON(JSON.stringify(baseGame.privatized(bobKey, true)));
    TestUtils.doEndTurn(game, bobKey);
    equal(game._lastDrawn, Game.PrivateMarker);
    equal(game.players()[0].tiles().length, 5);
    equal(game.currentPlayer().name(), "Bob");
    equal(game.currentPhase(), "awaitingDraw");
  });

  test("update - draw", function () {
    // todo
    expect(0);
  });

  test("update - merge", function () {
    // todo
    expect(0);
  });

  test("doing things in the wrong phase", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    assertThrows(function () { TestUtils.doEndTurn(game); }, /InvalidOperation/);
    assertThrows(function () { TestUtils.doBuy(game, Game.HOTELS[0]); },
      /InvalidOperation/);
    assertThrows(function () {
      TestUtils.doFoundChain(game, Game.HOTELS[0]);
    }, /InvalidOperation/);

    // todo: mergeChains


    // todo - add more here
  });

  test("basic founding and buying", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    // First player plays 1A
    TestUtils.currentPlayerPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);

    // Second player plays 2A
    TestUtils.currentPlayerPlaceTile(game, "2A");

    equal(game.currentPhase(), "founding");

    TestUtils.doFoundChain(game, Game.HOTELS[0]);
    equal(game._board.chainSize(Game.HOTELS[0]), 2);
    equal(game.currentPhase(), "buying");
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 1);

  });

  test("founding joining multiple unclaimed", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    TestUtils.currentPlayerPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "3A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "2B");
    TestUtils.doEndTurn(game);

    ok(true, "Place a tile that joins three unclaimed tiles");
    TestUtils.currentPlayerPlaceTile(game, "2A");
    equal(game.currentPhase(), "founding");
    TestUtils.doFoundChain(game, Game.HOTELS[0]);
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 1);
    equal(game._board.chainSize(Game.HOTELS[0]), 4);
    equal(game.currentPhase(), "buying");
  });

  test("basic merging", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    // Chain 1
    ok(true, "Chain 1 creation");
    TestUtils.currentPlayerPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "2A");
    TestUtils.doFoundChain(game, Game.HOTELS[0]);
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 1);
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "3A");
    TestUtils.doEndTurn(game);

    // Chain 2
    ok(true, "Chain 2 creation");
    TestUtils.currentPlayerPlaceTile(game, "1C");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "2C");
    TestUtils.doFoundChain(game, Game.HOTELS[1]);
    equal(game.currentPlayer().stockCount(Game.HOTELS[1]), 1);
    TestUtils.doEndTurn(game);

    // Now play our merging tile
    ok(true, "merging tile");
    TestUtils.currentPlayerPlaceTile(game, "1B");
    equal(game.currentPhase(), "merging");

    //    equal(game.currentPhase(), "buying");
    //    equal(game._board.state(), "ready");
    //    equal(game._board.chainSize(Game.HOTELS[0]), 6);

    // todo - finish this up
  });

  test("_payout", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    equal(Game.payout(Game.HOTELS[0], 2)["per"], 200);
    equal(Game.payout(Game.HOTELS[0], 3)["per"], 300);
    equal(Game.payout(Game.HOTELS[0], 4)["per"], 400);
    equal(Game.payout(Game.HOTELS[0], 5)["per"], 500);
    equal(Game.payout(Game.HOTELS[0], 6)["per"], 600);
    equal(Game.payout(Game.HOTELS[0], 8)["per"], 600);
    equal(Game.payout(Game.HOTELS[0], 10)["per"], 600);
    equal(Game.payout(Game.HOTELS[0], 11)["per"], 700);
    equal(Game.payout(Game.HOTELS[0], 20)["per"], 700);
    equal(Game.payout(Game.HOTELS[0], 21)["per"], 800);
    equal(Game.payout(Game.HOTELS[0], 30)["per"], 800);
    equal(Game.payout(Game.HOTELS[0], 31)["per"], 900);
    equal(Game.payout(Game.HOTELS[0], 40)["per"], 900);
    equal(Game.payout(Game.HOTELS[0], 41)["per"], 1000);
    equal(Game.payout(Game.HOTELS[0], 45)["per"], 1000);

    equal(Game.payout(Game.HOTELS[2], 2)["per"], 300);
    equal(Game.payout(Game.HOTELS[2], 3)["per"], 400);
    equal(Game.payout(Game.HOTELS[2], 4)["per"], 500);
    equal(Game.payout(Game.HOTELS[2], 5)["per"], 600);
    equal(Game.payout(Game.HOTELS[2], 6)["per"], 700);
    equal(Game.payout(Game.HOTELS[2], 8)["per"], 700);
    equal(Game.payout(Game.HOTELS[2], 10)["per"], 700);
    equal(Game.payout(Game.HOTELS[2], 11)["per"], 800);
    equal(Game.payout(Game.HOTELS[2], 20)["per"], 800);
    equal(Game.payout(Game.HOTELS[2], 21)["per"], 900);
    equal(Game.payout(Game.HOTELS[2], 30)["per"], 900);
    equal(Game.payout(Game.HOTELS[2], 31)["per"], 1000);
    equal(Game.payout(Game.HOTELS[2], 40)["per"], 1000);
    equal(Game.payout(Game.HOTELS[2], 41)["per"], 1100);
    equal(Game.payout(Game.HOTELS[2], 45)["per"], 1100);

    equal(Game.payout(Game.HOTELS[6], 2)["per"], 400);
    equal(Game.payout(Game.HOTELS[6], 3)["per"], 500);
    equal(Game.payout(Game.HOTELS[6], 4)["per"], 600);
    equal(Game.payout(Game.HOTELS[6], 5)["per"], 700);
    equal(Game.payout(Game.HOTELS[6], 6)["per"], 800);
    equal(Game.payout(Game.HOTELS[6], 8)["per"], 800);
    equal(Game.payout(Game.HOTELS[6], 10)["per"], 800);
    equal(Game.payout(Game.HOTELS[6], 11)["per"], 900);
    equal(Game.payout(Game.HOTELS[6], 20)["per"], 900);
    equal(Game.payout(Game.HOTELS[6], 21)["per"], 1000);
    equal(Game.payout(Game.HOTELS[6], 30)["per"], 1000);
    equal(Game.payout(Game.HOTELS[6], 31)["per"], 1100);
    equal(Game.payout(Game.HOTELS[6], 40)["per"], 1100);
    equal(Game.payout(Game.HOTELS[6], 41)["per"], 1200);
    equal(Game.payout(Game.HOTELS[6], 45)["per"], 1200);

    equal(Game.payout(Game.HOTELS[6], 45)["majority"], 12000);
    equal(Game.payout(Game.HOTELS[6], 45)["minority"], 6000);
  });

  test("buying stock", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    TestUtils.currentPlayerPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);
    TestUtils.currentPlayerPlaceTile(game, "2A");
    equal(game.currentPhase(), "founding");
    TestUtils.doFoundChain(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 24, "One gone from founding");
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 1);

    equal(game.currentPhase(), "buying");
    assertThrows(function () { TestUtils.doBuy(game, "NotARealHotelName"); },
      /BadArgument/);
    assertThrows(function () { TestUtils.doBuy(game, Game.HOTELS[1]); },
      /ChainInactive/);

    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 2);
    equal(game._hotels[Game.HOTELS[0]].stock, 23);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 3);
    equal(game._hotels[Game.HOTELS[0]].stock, 22);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game.currentPlayer().stockCount(Game.HOTELS[0]), 4);
    equal(game._hotels[Game.HOTELS[0]].stock, 21);

    assertThrows(function () { TestUtils.doBuy(game, Game.HOTELS[0]); },
      /InsufficientPurchases/, "Can't purchase more than three in a turn");

    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "12I");
    // hack current player to have only 100 dollars
    game.currentPlayer()._money = 100;
    assertThrows(function () { TestUtils.doBuy(game, Game.HOTELS[0]); },
      /InsufficientFunds/);
    // give them back their money
    game.currentPlayer()._money = 6000;

    // buy three stocks
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 18);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "11H");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 15);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "10I");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 12);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "9H");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 9);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "8I");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 6);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "7H");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 3);
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "6I");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    equal(game._hotels[Game.HOTELS[0]].stock, 0, "Bought last stock");
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "5H");
    assertThrows(function () { TestUtils.doBuy(game, Game.HOTELS[0]); },
      /InsufficientStock/, "Can't buy once we've run out");
  });

  test("placeTile when illegal", function () {
    // two scenarios when placing a tile is illegal       
    
    // (1) all chains are founded and placement would cause another founding
    var game = gameCreation.createGame("sevenChains");
    TestUtils.doPlaceTile(game, "4E");
    TestUtils.doEndTurn(game);

    assertThrows(function () {
      TestUtils.doPlaceTile(game, "5E");
    }, /IllegalPlacement/);

    // (2) placement would cause merger between two safe hotels
    game = gameCreation.createGame("merge13_11");
    assertThrows(function () {
      TestUtils.doPlaceTile(game, "11B");
    }, /IllegalPlacement/);
  });

  test("_givePlayerShares/_takePlayerShares", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John", "Jane"]);
    game.start();

    var player = game.players()[0];
    var hotel = Game.HOTELS[0];

    // establish baselines
    equal(player.stockCount(hotel), 0);
    equal(game._hotels[hotel].stock, 25);

    assertThrows(function () {
      game._takePlayerShares(hotel, player, 1);
    }, /InvalidOperation/, "cant take shares when we have none");

    ok(true, "give player 1 share");
    game._givePlayerShares(hotel, player, 1);
    equal(player.stockCount(hotel), 1);
    equal(game._hotels[hotel].stock, 24);

    assertThrows(function () {
      game._takePlayerShares(hotel, player, 2);
    }, /InvalidOperation/, "cant take more share than we have");

    equal(player.stockCount(hotel), 1);
    equal(game._hotels[hotel].stock, 24);

    ok(true, "take it back");
    game._takePlayerShares(hotel, player, 1);
    equal(player.stockCount(hotel), 0);
    equal(game._hotels[hotel].stock, 25);

    assertThrows(function () {
      game._givePlayerShares(hotel, player, 26);
    }, /InvalidOperation/, "cant give more shares than bank has");
    
    game._givePlayerShares(hotel, player, 25);
    equal(player.stockCount(hotel), 25);
    equal(game._hotels[hotel].stock, 0);

    assertThrows(function () {
      game._givePlayerShares(hotel, player, 1);
    }, /InvalidOperation/, "bank has no shares to give");
  });

  test("actionLog", function () {
    // todo - throw in some update actions

    var game = gameCreation.createGame("AsInHand");
    equal(game.historicalActions().length, 0);
    
    function validateActions(actions, expected, msg) {
      if (msg) {
        ok(true, msg);
      }

      equal(actions.length, expected.length, "expected number of actions");
      for (var i = 0; i < expected.length; i++) {
        equal(actions[i].player, expected[i][0]);
        equal(actions[i].action.verb, expected[i][1]);
      }
    }

    // bob places
    TestUtils.doPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);
    validateActions(game.historicalActions(), [
      ["Bob", "place"],
      ["Bob", "endTurn"]
    ], "validate actions after Bob's first turn");
    
    // mary places, founds and buys twice
    TestUtils.doPlaceTile(game, "2A");
    TestUtils.doFoundChain(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doEndTurn(game);
    var actions = game.historicalActions();
    validateActions(game.historicalActions(), [
      ["Bob", "place"],
      ["Bob", "endTurn"],
      ["Mary", "place"],
      ["Mary", "found"],
      ["Mary", "buy"],
      ["Mary", "buy"],
      ["Mary", "endTurn"],
    ], "validate actions after Mary's first turn");

    // george places, and buys one
    TestUtils.doPlaceTile(game, "3A");
    TestUtils.doBuy(game, Game.HOTELS[0]);
    TestUtils.doEndTurn(game);
    // Bob's drop off, since it's his turn again
    validateActions(game.historicalActions(), [
      ["Mary", "place"],
      ["Mary", "found"],
      ["Mary", "buy"],
      ["Mary", "buy"],
      ["Mary", "endTurn"],
      ["George", "place"],
      ["George", "buy"],
      ["George", "endTurn"],
    ], "validate actions after George's first turn");
  });

  test("privatize", function () {
    // assets unhidden - last drawn tile null
    var game = gameCreation.createGame("AsAndBsInHand");
    var keyBob = game.players()[0].key();
    var keyMary = game.players()[1].key(); 
    
    TestUtils.doPlaceTile(game, "1A");
    equal(game._lastDrawn, null);

    var privGame = game.privatized(keyBob, false);
    equal(privGame._lastDrawn, null);
    equal(privGame._players[0]._tiles[0], "2A");
    equal(privGame._players[1]._tiles[0], Game.PrivateMarker);
    notEqual(privGame._players[0]._money, Game.PrivateMarker);
    notEqual(privGame._players[1]._money, Game.PrivateMarker);
    notEqual(privGame._players[0]._stocks, Game.PrivateMarker);
    notEqual(privGame._players[1]._stocks, Game.PrivateMarker);
    
    privGame = game.privatized(keyMary, false);
    equal(privGame._lastDrawn, null);
    equal(privGame._players[0]._tiles[0], Game.PrivateMarker);
    equal(privGame._players[1]._tiles[0], "1B");
    notEqual(privGame._players[0]._money, Game.PrivateMarker);
    notEqual(privGame._players[1]._money, Game.PrivateMarker);
    notEqual(privGame._players[0]._stocks, Game.PrivateMarker);
    notEqual(privGame._players[1]._stocks, Game.PrivateMarker);

    // assets unhidden - last drawn tile non-null
    TestUtils.doEndTurn(game, keyBob);
    notEqual(game._lastDrawn, null);
    notEqual(game._lastDrawn, Game.PrivateMarker);

    privGame = game.privatized(keyBob, false);
    equal(privGame._lastDrawn, Game.PrivateMarker);
    equal(privGame._players[0]._tiles[0], "2A");
    equal(privGame._players[1]._tiles[0], Game.PrivateMarker);
    notEqual(privGame._players[0]._money, Game.PrivateMarker);
    notEqual(privGame._players[1]._money, Game.PrivateMarker);
    notEqual(privGame._players[0]._stocks, Game.PrivateMarker);
    notEqual(privGame._players[1]._stocks, Game.PrivateMarker);

    privGame = game.privatized(keyMary, false);
    equal(privGame._lastDrawn, Game.PrivateMarker);
    equal(privGame._players[0]._tiles[0], Game.PrivateMarker);
    equal(privGame._players[1]._tiles[0], "1B");
    notEqual(privGame._players[0]._money, Game.PrivateMarker);
    notEqual(privGame._players[1]._money, Game.PrivateMarker);
    notEqual(privGame._players[0]._stocks, Game.PrivateMarker);
    notEqual(privGame._players[1]._stocks, Game.PrivateMarker);

    // assets hidden - last drawn tile non-null
    privGame = game.privatized(keyBob, true);
    equal(privGame._lastDrawn, Game.PrivateMarker);
    equal(privGame._players[0]._tiles[0], "2A");
    equal(privGame._players[1]._tiles[0], Game.PrivateMarker);
    notEqual(privGame._players[0]._money, Game.PrivateMarker);
    equal(privGame._players[1]._money, Game.PrivateMarker);
    notEqual(privGame._players[0]._stocks, Game.PrivateMarker);
    equal(privGame._players[1]._stocks, Game.PrivateMarker);

    privGame = game.privatized(keyMary, true);
    equal(privGame._lastDrawn, Game.PrivateMarker);
    equal(privGame._players[0]._tiles[0], Game.PrivateMarker);
    equal(privGame._players[1]._tiles[0], "1B");
    equal(privGame._players[0]._money, Game.PrivateMarker);
    notEqual(privGame._players[1]._money, Game.PrivateMarker);
    equal(privGame._players[0]._stocks, Game.PrivateMarker);
    notEqual(privGame._players[1]._stocks, Game.PrivateMarker);
  });

  test("private game", function () {
    var serverGame = gameCreation.createGame("AsAndBsInHand");
    var bobKey = serverGame.players()[0].key();
    var maryKey = serverGame.players()[1].key();

    function performActionAllGames(action) {
      bobGame.performAction(action);
      serverGame.performAction(action);
      maryGame.performAction(action);
    };

    function allGamesInPhase(phase) {
      equal(bobGame.currentPhase(), phase, "bobs phase");
      equal(serverGame.currentPhase(), phase, "servers phase");
      equal(maryGame.currentPhase(), phase, "marys phase");
    }

    ok(true, "create private game");
    var bobGame = Game.createFromJSON(JSON.stringify(
      serverGame.privatized(bobKey, true)));
    var maryGame = Game.createFromJSON(JSON.stringify(
      serverGame.privatized(maryKey, true)));

    ok(true, "do a place action for bob");
    equal(bobGame.players()[0].tiles()[0], "1A");
    equal(serverGame.players()[0].tiles()[0], "1A");
    equal(maryGame.players()[0].tiles()[0], Game.PrivateMarker);

    performActionAllGames(Game.placeAction(bobKey, "1A"));
    allGamesInPhase("buying");

    ok(true, "do an end turn action for bob");    
    performActionAllGames(Game.endTurnAction(bobKey));
    equal(bobGame.currentPhase(), "awaitingDraw");
    equal(serverGame.currentPhase(), "placing");
    equal(maryGame.currentPhase(), "placing");

    ok(true, "do an update action to give bob draw info");
    bobGame.performAction(Game.drawUpdateAction(bobKey, "1A"));
    equal(bobGame.currentPhase(), "placing");

    ok(true, "now have mary do a place action");    
    equal(maryGame.players()[1].tiles()[0], "1B")
    equal(serverGame.players()[1].tiles()[0], "1B");
    equal(bobGame.players()[1].tiles()[0], Game.PrivateMarker);

    performActionAllGames(Game.placeAction(maryKey, "1B"));
    allGamesInPhase("founding");

    ok(true, "have mary found a hotel");    
    performActionAllGames(Game.foundAction(maryKey, Game.HOTELS[0]));
    allGamesInPhase("buying");
    
    ok(true, "have mary buy a stock");
    performActionAllGames(Game.buyAction(maryKey, Game.HOTELS[0]));
    allGamesInPhase("buying");

    ok(true, "end mary's turn");
    equal(maryGame._tilebag._tiles[0], Game.PrivateMarker);
    notEqual(serverGame._tilebag._tiles[0], Game.PrivateMarker);
    equal(bobGame._tilebag._tiles[0], Game.PrivateMarker);

    performActionAllGames(Game.endTurnAction(maryKey));
    equal(maryGame.currentPhase(), "awaitingDraw");
    equal(serverGame.currentPhase(), "placing");
    equal(bobGame.currentPhase(), "placing");
    
    ok(true, "do an update action for mary");
    maryGame.performAction(Game.drawUpdateAction(maryKey, "1B"));
    equal(maryGame.currentPhase(), "placing");

    ok(true, "now do a series of actions so that we can set up a merge");
    performActionAllGames(Game.placeAction(bobKey, "3A"));
    performActionAllGames(Game.endTurnAction(bobKey));
    bobGame.performAction(Game.drawUpdateAction(bobKey, "3A"));
    performActionAllGames(Game.placeAction(maryKey, "3B"));
    performActionAllGames(Game.foundAction(maryKey, Game.HOTELS[6]));
    performActionAllGames(Game.endTurnAction(maryKey));
    maryGame.performAction(Game.drawUpdateAction(maryKey, "3B"));
    allGamesInPhase("placing");

    ok(true, "have bob place a tile that creates a conflict");
    performActionAllGames(Game.placeAction(bobKey, "2A"));
    allGamesInPhase("merging");

    ok(true, "have bob kick off merge");
    performActionAllGames(Game.mergeAction(bobKey, Game.HOTELS[0], Game.HOTELS[6]));
    equal(bobGame.currentPhase(), "awaitingMerge");
    equal(serverGame.currentPhase(), "resolving");
    equal(maryGame.currentPhase(), "awaitingMerge");

    ok(true, "do an update action with merge info");
    equal(serverGame._merge.stockCounts[1], 1,
      "stockCounts correctly says mary has a share");
    equal(serverGame._merge.playerIndices.length, 1, "mary only player with stock");
    equal(serverGame._merge.playerIndices[0], 1, "mary player to resolve");

    var updateAction = Game.mergeUpdateAction(bobKey,
      TestUtils.clone(serverGame._merge.stockCounts),
      TestUtils.clone(serverGame._merge.playerIndices));
    bobGame.performAction(updateAction);
    maryGame.performAction(TestUtils.clone(updateAction));
    allGamesInPhase("resolving");
    equal(bobGame._merge.stockCounts[1], 1,
      "stockCounts correctly says mary has a share");
    equal(bobGame._merge.playerIndices.length, 1, "mary only player with stock");
    equal(bobGame._merge.playerIndices[0], 1, "mary player to resolve");
    equal(maryGame._merge.stockCounts[1], 1,
      "stockCounts correctly says mary has a share");
    equal(maryGame._merge.playerIndices.length, 1, "mary only player with stock");
    equal(maryGame._merge.playerIndices[0], 1, "mary player to resolve");

    ok(true, "have mary do resolve (bob has no stock)");
    equal(serverGame.players()[0].stockCount(Game.HOTELS[6]), 0);
    equal(serverGame.players()[1].stockCount(Game.HOTELS[6]), 1);
    performActionAllGames(Game.resolveMergeAction(maryKey, 0, 1, 0));
    allGamesInPhase("buying");
  });

  test("_finishGame", function () {
    var game = gameCreation.createGameFromInputs({
      hotels: [
      {          
          hotel: Game.HOTELS[0],
          tiles: gameCreation.allTiles.slice(0, 11) // size 11
        },
        {
          hotel: Game.HOTELS[1],
          tiles: gameCreation.allTiles.slice(12 * 2, 12 * 2 + 11) // size 11
        },
        {
          hotel: Game.HOTELS[2],
          tiles: gameCreation.allTiles.slice(12 * 4, 12 * 4 + 11) // size 11
        }
      ],
      players: ["Bob", "Mary", "George"],
      otherTilesInHand: ["12I"],
      purchases: [
        [Game.HOTELS[0], Game.HOTELS[0], Game.HOTELS[0], Game.HOTELS[0]],
        [Game.HOTELS[0]],
        [Game.HOTELS[1], Game.HOTELS[1], Game.HOTELS[1]]
      ],
      asserts: function (game) {
        equal(game.players()[0].stockCount(Game.HOTELS[0]), 4);
        equal(game.players()[0].stockCount(Game.HOTELS[1]), 1);
        equal(game.players()[0].stockCount(Game.HOTELS[2]), 0);

        equal(game.players()[1].stockCount(Game.HOTELS[0]), 2);
        equal(game.players()[1].stockCount(Game.HOTELS[1]), 0);
        equal(game.players()[1].stockCount(Game.HOTELS[2]), 0);
        
        equal(game.players()[2].stockCount(Game.HOTELS[0]), 0);
        equal(game.players()[2].stockCount(Game.HOTELS[1]), 3);
        equal(game.players()[2].stockCount(Game.HOTELS[2]), 1);
      }      
    });

    var startMoney = _.map(game.players(), function (player) {
      return player.money();
    });
    ok(TestUtils.listsEquivalent(startMoney, [4200, 5800, 4800]));

    TestUtils.doPlaceTile(game, "12I");
    equal(game.currentPhase(), "buying");

    TestUtils.doEndGame(game);
    equal(game.currentPhase(), "buying");

    TestUtils.doEndTurn(game);
    equal(game.currentPhase(), "gameOver");

    var moneyDifference = _.map(game.players(), function (player, index) {
      return player.money() - startMoney[index];
    });

    var per0 = 700;
    var major0 = per0 * 10;
    var minor0 = major0 / 2;
    var per1 = 700;
    var major1 = per1 * 10;
    var minor1 = major1 / 2;
    var per2 = 800;
    var major2 = per2 * 10;
    var minor2 = major2 / 2;

    ok(TestUtils.listsEquivalent(moneyDifference,[
      major0 + minor1 + 4 * per0 + 1 * per1,
      minor0 + 2 * per0,
      major1 + major2 + minor2 + 3 * per1 + 1 * per2
    ]));
  });

  test("_calculateBonuses", function () {
    var baseGame = gameCreation.createFromNames(["Bob", "Mary", "George", "Henry", "Jane"]);
    baseGame.start();

    // hack game phase
    baseGame._phase = "endingGame";

    function validateBonus(bonus, majority, minority, majorityPlayers, minorityPlayers) {
      equal(bonus.majority.amount, majority);
      // test difference in both directions
      deepEqual(_.difference(bonus.majority.players, majorityPlayers), []);
      deepEqual(_.difference(majorityPlayers, bonus.majority.players), []);
      equal(bonus.minority.amount, minority);
      deepEqual(_.difference(bonus.minority.players, minorityPlayers), []);
      deepEqual(_.difference(minorityPlayers, bonus.minority.players), []);
    }

    // clear minority/majority
    var game = TestUtils.clone(baseGame)
    var bonus = game._calculateBonuses(Game.HOTELS[0], 2, [2, 1, 0, 0, 0]);
    validateBonus(bonus, 2000, 1000, [game._players[0]], [game._players[1]]);
    
    // todo - finish these
    
  });

  test("no playable tiles", function () {
    var inputs = {
      hotels: [
        {
          hotel: Game.HOTELS[0],
          tiles: ["1A", "2A", "3A", "4A", "5A", "6A", "7A", "8A", "9A", "10A", "11A", "12A"]
        },
        {
          hotel: Game.HOTELS[1],
          tiles: ["1C", "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "11C", "12C"]
        },
      ],
      players: ["Bob", "Mary"],
      otherTilesInHand: ["12I", "1B", "2B", "3B", "4B", "5B", "6B", "7B", "8B", "9B", "10B", "11B"]
    };
    var baseGame = gameCreation.createGameFromInputs(inputs);
    TestUtils.doPlaceTile(baseGame, "12I");

    var game = TestUtils.clone(baseGame);
    TestUtils.doEndTurn(game);

    equal(game._noTilesPlayable, true, "All tiles in hand would cause merge of two safe chains");

    // hack tiles to include a valid one
    game = TestUtils.clone(baseGame);
    game._players[1]._tiles[0] = "10I";

    TestUtils.doEndTurn(game);
    equal(game._noTilesPlayable, false, "10I should be playable");
  });

  test("_hiddenAssets", function () {
    var game = gameCreation.createFromNames(["Bob", "Mary", "John"]);
    game.start();
    equal(game._hiddenAssets, false);

    var key = game.players()[0].key();

    equal(game.privatized(key, false)._hiddenAssets, false,
      "privatized game without hidden assets");
    equal(game.privatized(key, true)._hiddenAssets, true,
      "privatized game with hidden assets");
  });

  test("swap tiles", function () {
    var baseGame = gameCreation.createGame("swappable");
    TestUtils.doPlaceTile(baseGame, "12I");

    var game = TestUtils.clone(baseGame);
    TestUtils.doEndTurn(game);

    equal(game._noTilesPlayable, true, "All tiles in hand would cause merge of two safe chains");

    var tiles = game.currentPlayer().tiles().slice();
    TestUtils.doSwapTiles(game);
    
    var newTiles = game.currentPlayer().tiles();
    deepEqual(_.intersection(tiles, newTiles), [],
      "none of the same tiles");
    equal(newTiles.length, 6);

    // todo - try to swap when we cant

    // todo - when draw gives us 6 new unplayable tiles
  });

  test("swap update", function () {
    var baseGame = gameCreation.createGame("swappable");

    var bobKey = baseGame.players()[0].key();
    var maryKey = baseGame.players()[1].key();

    TestUtils.doPlaceTile(baseGame, "12I", bobKey);
    TestUtils.doEndTurn(baseGame, bobKey);

    var game = Game.createFromJSON(JSON.stringify(
      baseGame.privatized(maryKey, false)));
    
    equal(game._noTilesPlayable, true);

    var tiles = game.currentPlayer().tiles().slice();
    TestUtils.doSwapTiles(game, maryKey);
    equal(game.currentPhase(), "awaitingSwap");
    _.each(game.currentPlayer().tiles(), function (tile) {
      equal(tile, Game.PrivateMarker);
    });

    var newTiles = [];
    for (var i = 0; i < 6; i++) {
      newTiles.push(baseGame._tilebag.getTile());
    }

    game.performAction(Game.swapUpdateAction(maryKey, newTiles));
    ok(TestUtils.listsEquivalent(newTiles, game.currentPlayer().tiles()));
    equal(game.currentPhase(), "placing");

    // do the same thing, but have "drawn" tiles also be illegal to place
    game = Game.createFromJSON(JSON.stringify(
      baseGame.privatized(maryKey, false)));
    equal(game._noTilesPlayable, true);

    var originalTiles = game.currentPlayer().tiles().slice();

    TestUtils.doSwapTiles(game, maryKey);

    // swap (with same tiles we started with)
    game.performAction(Game.swapUpdateAction(maryKey, originalTiles));
    equal(game._noTilesPlayable, true);
    equal(game.currentPhase(), "placing");
    
  });
});

