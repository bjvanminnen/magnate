require([
  "model/game",
  "model/tilebag",
  "model/gameboard",
  "testutils/TestUtils",
  "testutils/gameCreation"
], function (Game, TileBag, GameBoard, TestUtils, gameCreation) {

  function create22() {
    var board = new GameBoard();
    board = new GameBoard();
    ok(board.placeTile("4F"));
    ok(board.placeTile("5F"));
    ok(board.foundChain(Game.HOTELS[0]));
    ok(board.placeTile("7F"));
    ok(board.placeTile("8F"));
    ok(board.foundChain(Game.HOTELS[1]));
    return board;
  }

  function create32() {
    var board = create22();
    ok(board.placeTile("3F"));
    return board;
  }

  function create221() {
    var board = create22();
    ok(board.placeTile("6E"));
    return board;
  }
  
  function create222() {
    var board = create221();
    ok(board.placeTile("6D"));
    ok(board.foundChain(Game.HOTELS[2]));
    return board;
  }

  function create322() {
    var board = create222();
    ok(board.placeTile("3F"));
    return board;
  }

  function create332() {
    var board = create322();
    ok(board.placeTile("9F"));
    return board;
  }

  function create3322() {
    var board = create332();
    ok(board.placeTile("6G"));
    ok(board.placeTile("6H"));
    ok(board.foundChain(Game.HOTELS[3]));
    return board;
  }

  function create5432() {
    var board = create3322();
    ok(board.placeTile("2F"));
    ok(board.placeTile("1F"));
    ok(board.placeTile("10F"));
    ok(board.placeTile("6C"));
    return board;
  }

  QUnit.module("gameboard tests");



  test("tileOnBoard", function () {
    function testTile(tile, expected) {
      equal(board.tileOnBoard(tile), expected, tile);
    }

    var board = new GameBoard();
    for (var i = 1; i <= 12; i++) {
      testTile(i + "A", true);
    }
    testTile("0A", false);
    testTile("13A", false);
    testTile("1J", false);
    testTile("1I", true);
    testTile("12I", true);
  });

  test("neighborsOf", function () {
    var board = new GameBoard();
      
    // empty board
    equal(board.neighborsOf("1A").length, 0);
    equal(board.neighborsOf("1A", false).length, 0);

    // set 2A to be unclaimed
    board._setTileState("2A", "unclaimed");
    equal(board.neighborsOf("1A").length, 1);
    equal(board.neighborsOf("1A", false).length, 1);

    // set b1 to be a hotel
    board._setTileState("1B", Game.HOTELS[0]);
    equal(board.neighborsOf("1A").length, 2);

    // set b3 to be a different hotel
    board._setTileState("3B", Game.HOTELS[1]);
    equal(board.neighborsOf("2B").length, 3);
  });

  test("placeTile: simple placement", function () {
    var board = new GameBoard();

    var tile = "1A";
    equal(board.tileState(tile), "none");
    ok(board.placeTile(tile));
    equal(board._lastPlacedTile, tile);
    equal(board.tileState(tile), "unclaimed");
    equal(board.state(), "ready");

    assertThrows(function () { board.placeTile(tile); }, /InvalidOperation/,
      "placing tile that was already placed");

    assertThrows(function () { board.placeTile("13J"); }, /InvalidArgument/,
      "placing tile that doesnt exist");

    // reset
    board = new GameBoard();
    equal(board.tileState(tile), "none");
    ok(board.placeTile("1A"));
    equal(board._lastPlacedTile, "1A");
    ok(board.placeTile("1B"));
    equal(board._lastPlacedTile, "1B");
    equal(board.tileState(tile), "unclaimed");
    equal(board.state(), "founding");

    assertThrows(function () { board.placeTile("1D"); }, /InvalidOperation/,
      "placing while founding");
    equal(board._lastPlacedTile, "1B", "didnt change lastPlacedTile on failure");

    board = new GameBoard();
    board._boardState = "notarealstate";
    assertThrows(function () { board.placeTile("1A"); }, /Unexpected/,
      "placing during corrupted state");
  });

  test("placeTile: fails if would cause founding and all hotels taken", function () {
    var board = new GameBoard();
    function createHotel(tile1, tile2, hotel) {
      ok(board.placeTile(tile1));
      ok(board.placeTile(tile2));
      equal(board.state(), "founding");
      board.foundChain(hotel);
    }

    createHotel("1A", "2A", Game.HOTELS[0]);
    createHotel("1C", "2C", Game.HOTELS[1]);
    createHotel("1E", "2E", Game.HOTELS[2]);
    createHotel("1G", "2G", Game.HOTELS[3]);
    createHotel("1I", "2I", Game.HOTELS[4]);
    createHotel("4A", "5A", Game.HOTELS[5]);
    createHotel("4C", "5C", Game.HOTELS[6]);

    ok(board.placeTile("4E"));
    equal(board.placeTile("4F"), false,
      "Can't place tile as all hotels have already been founded");
  });

  
  test("foundChain", function () {
    var board = new GameBoard();

    var tile1 = "1A";
    var tile2 = "2A";
    var tile3 = "4C";
    var tile4 = "5C";

    ok(board.placeTile(tile1));
    ok(board.placeTile(tile2));
    equal(board.state(), "founding");

    ok(board.foundChain(Game.HOTELS[0]));

    equal(board.tileState(tile1), Game.HOTELS[0]);
    equal(board.tileState(tile2), Game.HOTELS[0]);
    equal(board.chainSize(Game.HOTELS[0]), 2);
    equal(board.state(), "ready");

    // founding the same chain again should fail
    ok(board.placeTile(tile3));
    ok(board.placeTile(tile4));
    equal(board.state(), "founding");
    assertThrows(function () { board.foundChain(Game.HOTELS[0]); }, /InvalidOperation/);
    equal(board.chainSize(Game.HOTELS[0]), 2);
    equal(board.state(), "founding");
  });

  test("placeTile: next to existing chain", function () {
    var board = new GameBoard();

    ok(board.placeTile("1A"));
    ok(board.placeTile("2A"));

    ok(board.foundChain(Game.HOTELS[0]));
    equal(board.chainSize(Game.HOTELS[0]), 2);

    var tiles = ["1B", "3A", "4A", "5A", "6A", "7A", "8A", "9A", "10A", "11A",
      "12A"];

    for (var i = 0; i < tiles.length; i++) {
      board.placeTile(tiles[i]);
      equal(board.chainSize(Game.HOTELS[0]), 3 + i);
    }
  });

  test("placeTile: between chain and island", function () {
    var board = new GameBoard();
    ok(board.placeTile("1A"));
    ok(board.placeTile("2A"));
    ok(board.foundChain(Game.HOTELS[0]));
    ok(board.placeTile("4A"));

    // now play tile between chain and island
    ok(board.placeTile("3A"));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 4);
  });

  test("placeTile: mergers", function () {
    // two hotels
    var board = create22();
    equal(board.chainSize(Game.HOTELS[0]), 2);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    ok(board.placeTile("6F"));
    equal(board.state(), "merging");
    equal(board.tileState("6F"), "merging");

    // two hotels plus island
    var board = create221();
    equal(board.chainSize(Game.HOTELS[0]), 2);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    equal(board.tileState("6E"), "unclaimed");
    ok(board.placeTile("6F"));
    equal(board.state(), "merging");
    equal(board.tileState("6F"), "merging");
    equal(board.tileState("6E"), "unclaimed");

    // four hotels
    board = create5432();
    equal(board.chainSize(Game.HOTELS[0]), 5);
    equal(board.chainSize(Game.HOTELS[1]), 4);
    equal(board.chainSize(Game.HOTELS[2]), 3);
    equal(board.chainSize(Game.HOTELS[3]), 2);
    ok(board.placeTile("6F"));
    equal(board.state(), "merging");
    equal(board.tileState("6F"), "merging");

    // safe hotels
    board = new GameBoard();
    for (var i = 1; i <= 12; i++) {
      ok(board.placeTile(i + "A"));
      if (i === 2) {
        board.foundChain(Game.HOTELS[0]);
      }
    }
    equal(board.chainSize(Game.HOTELS[0]), 12);

    for (var i = 1; i <= 11; i++) {
      ok(board.placeTile(i + "C"));
      if (i === 2) {
        board.foundChain(Game.HOTELS[1]);
      }
    }
    equal(board.chainSize(Game.HOTELS[1]), 11);
    equal(board.placeTile("2B"), false,
      "cant place tile that would result in attempt to merge safe chain");
  });

  test("_createConflict", function () {
    var board, conflict;
    var mergeTile = "6F";

    // two chains, same size
    board = create22();
    equal(board.chainSize(Game.HOTELS[0]), 2);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 2)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 2);
    equal(conflict.buyers.length, 2);
    equal(conflict.sellers.length, 2);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.buyers, Game.HOTELS[1]));
    ok(_.contains(conflict.sellers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));

    // two chains, one bigger
    board = create32();
    equal(board.chainSize(Game.HOTELS[0]), 3);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 2)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 2);
    equal(conflict.buyers.length, 1);
    equal(conflict.sellers.length, 1);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));

    // three chains, all the same size
    board = create222();
    equal(board.chainSize(Game.HOTELS[0]), 2);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    equal(board.chainSize(Game.HOTELS[2]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 3)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 3);
    equal(conflict.buyers.length, 3);
    equal(conflict.sellers.length, 3);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.buyers, Game.HOTELS[1]));
    ok(_.contains(conflict.buyers, Game.HOTELS[2]));
    ok(_.contains(conflict.sellers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));
    ok(_.contains(conflict.sellers, Game.HOTELS[2]));

    // three chains, one biggest
    board = create322();
    equal(board.chainSize(Game.HOTELS[0]), 3);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    equal(board.chainSize(Game.HOTELS[2]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 3)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 3);
    equal(conflict.buyers.length, 1);
    equal(conflict.sellers.length, 2);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));
    ok(_.contains(conflict.sellers, Game.HOTELS[2]));

    // three chains, one smallest
    board = create332();
    equal(board.chainSize(Game.HOTELS[0]), 3);
    equal(board.chainSize(Game.HOTELS[1]), 3);
    equal(board.chainSize(Game.HOTELS[2]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 3)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 3);
    equal(conflict.buyers.length, 2);
    equal(conflict.sellers.length, 2);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.buyers, Game.HOTELS[1]));
    ok(_.contains(conflict.sellers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));

    // four chains, 2 biggest, 2 smallest
    board = create3322();
    equal(board.chainSize(Game.HOTELS[0]), 3);
    equal(board.chainSize(Game.HOTELS[1]), 3);
    equal(board.chainSize(Game.HOTELS[2]), 2);
    equal(board.chainSize(Game.HOTELS[3]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 4)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 4);
    equal(conflict.buyers.length, 2);
    equal(conflict.sellers.length, 2);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.buyers, Game.HOTELS[1]));
    ok(_.contains(conflict.sellers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));

    // four chains, all different sizes
    board = create5432();
    equal(board.chainSize(Game.HOTELS[0]), 5);
    equal(board.chainSize(Game.HOTELS[1]), 4);
    equal(board.chainSize(Game.HOTELS[2]), 3);
    equal(board.chainSize(Game.HOTELS[3]), 2);
    ok(board._createConflict(Game.HOTELS.slice(0, 5)));
    conflict = board._conflict;
    equal(conflict.hotels.length, 5);
    equal(conflict.buyers.length, 1);
    equal(conflict.sellers.length, 1);
    ok(_.contains(conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(conflict.sellers, Game.HOTELS[1]));

    // safe hotels
    board = new GameBoard();
    for (var i = 1; i <= 12; i++) {
      ok(board.placeTile(i + "A"));
      if (i === 2) {
        board.foundChain(Game.HOTELS[0]);
      }
    }
    equal(board.chainSize(Game.HOTELS[0]), 12);

    for (var i = 1; i <= 11; i++) {
      ok(board.placeTile(i + "C"));
      if (i === 2) {
        board.foundChain(Game.HOTELS[1]);
      }
    }
    equal(board.chainSize(Game.HOTELS[1]), 11);

    equal(board._createConflict(Game.HOTELS.slice(0,2)), false, 
      "_createConflict fails when multiple hotels larger than 11");

    // two hotels, one size 12 the other size 2
    // this to reveal a sort bug
    board = new GameBoard();
    for (var i = 1; i <= 12; i++) {
      ok(board.placeTile(i + "A"));
      if (i === 2) {
        board.foundChain(Game.HOTELS[0]);
      }
    }
    equal(board.chainSize(Game.HOTELS[0]), 12);
    ok(board.placeTile("1C"));
    ok(board.placeTile("2C"));
    ok(board.foundChain(Game.HOTELS[1]));
    ok(board._createConflict(Game.HOTELS.slice(0, 2)));
    ok(_.contains(board._conflict.buyers, Game.HOTELS[0]));
    ok(_.contains(board._conflict.sellers, Game.HOTELS[1]));

  });

  test("mergeChains", function () {
    var board;
    var mergeTile = "6F";

    // two chains, same size
    board = create22();
    assertThrows(function () {
      board.mergeChains(Game.HOTELS[0], Game.HOTELS[1]);
    }, /InvalidOperation/, "cant merge when not in merging state");
    ok(board.placeTile(mergeTile));
    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[1]));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 5);
    equal(board.chainSize(Game.HOTELS[1]), 0);
    equal(board.tileState(mergeTile), Game.HOTELS[0]);

    board = create22();
    ok(board.placeTile(mergeTile));
    ok(board.mergeChains(Game.HOTELS[1], Game.HOTELS[0]));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 0);
    equal(board.chainSize(Game.HOTELS[1]), 5);
    equal(board.tileState(mergeTile), Game.HOTELS[1]);

    // two chains that are the same size, and an extra unclaimed
    board = create221();
    ok(board.placeTile(mergeTile));

    assertThrows(function () {
      board.mergeChains(Game.HOTELS[1], Game.HOTELS[1])
    }, /InvalidArgument/, "cant have same hotel as buyer and seller");

    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[1]));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 6);
    equal(board.chainSize(Game.HOTELS[1]), 0);
    equal(board.tileState("6E"), Game.HOTELS[0]);
    equal(board.tileState(mergeTile), Game.HOTELS[0]);

    // two chains, one bigger
    board = create32();
    ok(board.placeTile(mergeTile));
    
    assertThrows(function () {
      board.mergeChains(Game.HOTELS[1], Game.HOTELS[0])
    }, /InvalidOperation/, "cant have smaller chain buy larger");
    equal(board.state(), "merging");
    equal(board.chainSize(Game.HOTELS[0]), 3);
    equal(board.chainSize(Game.HOTELS[1]), 2);
    equal(board.tileState(mergeTile), "merging");

    assertThrows(function () {
      board.mergeChains(Game.HOTELS[2], Game.HOTELS[1])
    }, /InvalidOperation/, "cant have random hotel buy");

    assertThrows(function () {
      board.mergeChains(Game.HOTELS[0], Game.HOTELS[2])
    }, /InvalidOperation/, "cant have random hotel sell");
    
    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[1]));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 6);
    equal(board.chainSize(Game.HOTELS[1]), 0);
    equal(board.tileState(mergeTile), Game.HOTELS[0]);
    
    // three chains, all same size
    board = create222();
    ok(board.placeTile(mergeTile));
    // 0 buys 1
    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[1]));
    equal(board.state(), "merging");
    equal(board.chainSize(Game.HOTELS[0]), 4);
    equal(board.chainSize(Game.HOTELS[1]), 0);
    equal(board.chainSize(Game.HOTELS[2]), 2);
    equal(board.tileState(mergeTile), "merging");
    // 1 can't buy 2
    assertThrows(function () {
      board.mergeChains(Game.HOTELS[1], Game.HOTELS[2]);
    }, /InvalidOperation/);
    // 0 buys 2
    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[2]));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 7);
    equal(board.chainSize(Game.HOTELS[1]), 0);
    equal(board.chainSize(Game.HOTELS[2]), 0);
    equal(board.tileState(mergeTile), Game.HOTELS[0]);

    // three chains, one biggest
    // - could do this, but not sure i need more

    // three chains, one smallest
    // - could do this, but not sure i need more

    // four chains, 2 biggest, 2 smallest
    // - could do this, but not sure i need more

    // four chains, all different sizes
    board = create5432();
    ok(board.placeTile(mergeTile));
    ok(TestUtils.listsEquivalent(board._conflict.hotels,
      Game.HOTELS.slice(0, 4)));

    assertThrows(function () {
      board.mergeChains(Game.HOTELS[0], Game.HOTELS[2]);
    }, /InvalidOperation/, "fail to purchase a chain that is smaller when " +
      "there are larger chain(s) that are also smaller than buyer");
    
    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[1]));
    ok(TestUtils.listsEquivalent(board._conflict.hotels,
      [Game.HOTELS[0], Game.HOTELS[2], Game.HOTELS[3]]), "missing 1");

    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[2]));
    ok(TestUtils.listsEquivalent(board._conflict.hotels,
      [Game.HOTELS[0], Game.HOTELS[3]]), "missing 1 and 2");

    ok(board.mergeChains(Game.HOTELS[0], Game.HOTELS[3]));
    equal(board.state(), "ready");
    equal(board.chainSize(Game.HOTELS[0]), 15);
    equal(board.chainSize(Game.HOTELS[1]), 0);
    equal(board.chainSize(Game.HOTELS[2]), 0);
    equal(board.chainSize(Game.HOTELS[2]), 0);
    equal(board.tileState(mergeTile), Game.HOTELS[0]);
  });

  test("_addToChain", function () {
    var board = new GameBoard();
    ok(board.placeTile("1A"));
    ok(board.placeTile("1B"));
    ok(board.foundChain(Game.HOTELS[0]));
    ok(board.placeTile("1D"));
    ok(board.placeTile("2C"));

    board._addToChain(Game.HOTELS[0], "1C");
    equal(board.tileState("1C"), Game.HOTELS[0]);
    equal(board.tileState("1D"), Game.HOTELS[0]);
    equal(board.chainSize(Game.HOTELS[0]), 5);

  });

  test("_setTileState", function() {
    var board = new GameBoard();
        
    var hotel = "Western";
    var hotel2 = "Hyatt";

    board._setTileState("1A", hotel);
    equal(board.tileState("1A"), hotel);
    ok(_.isEqual(board._chainCounts, {
      "Western" : 1
    }));
    
    board._setTileState("1B", hotel);
    equal(board.tileState("1B"), hotel);
    ok(_.isEqual(board._chainCounts, {
      "Western" : 2
    }));

    // flip to a different hotel
    board._setTileState("1B", hotel2);
    equal(board.tileState("1B"), hotel2);
    ok(_.isEqual(board._chainCounts, {
      "Western" : 1,
      "Hyatt" : 1
    }));

    // make a tile unclaimed
    board._setTileState("1C", "unclaimed");
    equal(board.tileState("1C"), "unclaimed");
    ok(_.isEqual(board._chainCounts, {
      "Western" : 1,
      "Hyatt" : 1
    }));

    // move it to a hotel
    board._setTileState("1C", hotel);
    equal(board.tileState("1C"), hotel);
    ok(_.isEqual(board._chainCounts, {
      "Western" : 2,
      "Hyatt" : 1
    }));

    // go from unclaimed to merging
    board._setTileState("1D", "unclaimed");
    equal(board.tileState("1D"), "unclaimed");
    board._setTileState("1D", "merging");
    equal(board.tileState("1D"), "merging");
    ok(_.isEqual(board._chainCounts, {
      "Western" : 2,
      "Hyatt" : 1
    }));

    // from merging to hotel
    board._setTileState("1D", hotel);
    equal(board.tileState("1D"), hotel);
    ok(_.isEqual(board._chainCounts, {
      "Western" : 3,
      "Hyatt" : 1
    }));

    // from non-zero size to zero size
    board._setTileState("1B", hotel);
    equal(board.tileState("1B"), hotel);
    ok(_.isEqual(board._chainCounts, {
      "Western" : 4,
      "Hyatt" : 0
    }));
    
  });

  test("serialize/deserialize", function () {
    var board1 = new GameBoard();

    // Make some changes
    var hyatt = ["1A", "2A"];
    var holiday = ["4A", "5A"];
    ok(board1.placeTile(hyatt[0]));
    ok(board1.placeTile(hyatt[1]));
    ok(board1.foundChain(Game.HOTELS[0]));

    ok(board1.placeTile(holiday[0]));
    ok(board1.placeTile(holiday[1]));
    ok(board1.foundChain(Game.HOTELS[1]));

    // Clone (TestUtils.clone does an equality assert for us)
    var board2 = TestUtils.clone(board1);
  });

  test("endGameReady", function () {
    function longChainBoard(size) {
      var inputs = {
        hotels: [
          {
            hotel: Game.HOTELS[0],
            tiles: gameCreation.allTiles.slice(0, size)
          },
          // we need second hotel so that all hotels arent safe
          {
            hotel: Game.HOTELS[1],
            tiles: gameCreation.allTiles.reverse().slice(0, 2)
          }
        ],
        players: ["Bob", "Mary", "George"]
      };
      var game = gameCreation.createGameFromInputs(inputs);
      equal(game.chainSize(Game.HOTELS[0]), size);
      return game;
    }

    var game = longChainBoard(40);
    equal(game._board.endGameReady(), false, "size of 40 is not yet game over");
    game = longChainBoard(41);
    equal(game._board.endGameReady(), true, "size of 41 is game over");

    // two hotels of size 11, one of size 10
    game = gameCreation.createGameFromInputs({
      hotels: [
        {
          hotel: Game.HOTELS[0],
          tiles: gameCreation.allTiles.slice(0, 11)
        },
        {
          hotel: Game.HOTELS[1],
          tiles: gameCreation.allTiles.slice(12 * 2, 12 * 2 + 11)
        },
        {
          hotel: Game.HOTELS[2],
          tiles: gameCreation.allTiles.slice(12 * 4, 12 * 4 + 10)
        }
      ],
      players: ["Bob", "Mary", "George"]
    });
    equal(game._board.endGameReady(), false, "one of the hotels is not safe");
    
    // three hotels, all size 11
    game = gameCreation.createGameFromInputs({
      hotels: [
        {
          hotel: Game.HOTELS[0],
          tiles: gameCreation.allTiles.slice(0, 11)
        },
        {
          hotel: Game.HOTELS[1],
          tiles: gameCreation.allTiles.slice(12 * 2, 12 * 2 + 11)
        },
        {
          hotel: Game.HOTELS[2],
          tiles: gameCreation.allTiles.slice(12 * 4, 12 * 4 + 11)
        }
      ],
      players: ["Bob", "Mary", "George"]
    });
    equal(game._board.endGameReady(), true, "all of the hotels are not safe");

    // same thing, but hack gameboard to have a size 0 for one of the chains
    // (which will be true if it is founded then acquired)
    var clone = TestUtils.clone(game);
    clone._board._chainCounts[Game.HOTELS[3]] = 0;
    equal(clone._board.endGameReady(), true, "all of the hotels are not safe");
  });

  test("end game on fresh board", function () {
    var game = gameCreation.createFromNames(["Bob", "May"]);
    game.start();

    equal(game.endGameReady(), false, "cant end game with no hotels placed");

    TestUtils.currentPlayerPlaceTile(game, "1A");
    TestUtils.doEndTurn(game);

    TestUtils.currentPlayerPlaceTile(game, "1B");
    TestUtils.doFoundChain(game, Game.HOTELS[0]);
    equal(game.chainSize(Game.HOTELS[0]), 2);

    equal(game.endGameReady(), false, "cant found after chain founded");

  });
  
});
