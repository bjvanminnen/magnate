require([
  "model/game",
  "controller/networkController",
  "server/src/serverController",
  "server/src/userManager",
  "testutils/TestDb",
  "testutils/TestUtils",
  "testutils/gameCreation",
  "testutils/SocketSim",
  "testutils/TestView",
  "testutils/Waiter",
  "testlib/async",
  "testutils/debugIdentities"  
], function (Game, NetworkController, ServerController, userManager, TestDb,
  TestUtils, gameCreation, SocketSim, TestView, Waiter, async, debugIdentities) {

  QUnit.module("integration tests");

  var exceptional = TestUtils.exceptional;

  asyncTest("integration", function () {
    var bob = {}, mary = {};

    bob.socket = new SocketSim();
    mary.socket = new SocketSim();

    bob.ident = debugIdentities.getIdentity(0);
    mary.ident = debugIdentities.getIdentity(1);

    var testDb = new TestDb("dbName", "collectionName");
    var server = new ServerController(testDb);
    server.addSocket(bob.socket.server);
    server.addSocket(mary.socket.server);
    
    // todo - should really be testing code in networkGame as well?

    var gameId;

    async.waterfall([
    exceptional(function (callback) {
      testDb.addItem(testDb.ItemType.Running,
        JSON.stringify(gameCreation.createGame("AsAndBsInHand")),
        function (err, id) {
          gameId = id;
          callback(null);
        }
      );
    }),
    // have bob get the game
    exceptional(function (callback) {
      bob.socket.client.emit("user-signin", bob.ident);

      bob.socket.client.emit("get-game", gameId, function (err, json) {
        if (err) {
          callback(err);
        }

        // todo - right now if we throw inside this function, it will not be
        // caught and test execution will stall

        bob.game = Game.createFromJSON(json);
        bob.view = new TestView(bob.game, bob.ident.pkey);
        bob.controller = new NetworkController(bob.game, bob.view, gameId,
          bob.socket.client, bob.ident.pkey);

        callback(null);
      });
    }),
    // have mary get the game
    exceptional(function (callback) {
      mary.socket.client.emit("user-signin", mary.ident);

      mary.socket.client.emit("get-game", gameId, function (err, json) {
        if (err) {
          callback(err);
        }
        mary.game = Game.createFromJSON(json);
        // todo - should prob store the pkeys somewhere 
        mary.view = new TestView(mary.game, mary.ident.pkey);
        mary.controller = new NetworkController(mary.game, mary.view, gameId,
          mary.socket.client, mary.ident.pkey);

        callback(null);
      });
    }),
    // validate they have the same gameboard.  have bob place a tile
    exceptional(function (callback) {
      // local models wont be identical because of privatized info
      equal(JSON.stringify(bob.game._board), JSON.stringify(mary.game._board),
        "have same gameboard");

      TestUtils.doPlaceTile(bob.game, "1A", bob.ident.pkey);
      equal(bob.game.currentPhase(), "buying");
      equal(mary.game.currentPhase(), "placing", "mary still placing");

      var waiter = new Waiter(callback);
      waiter.waitForNotification(mary.game.actionSucceeded);
    }),
    // validate mary applied the action.  have bob end turn
    exceptional(function (action, callback) {
      equal(action.verb, "place");
      equal(action.pkey, bob.ident.pkey);
      equal(mary.game.currentPhase(), "buying");

      TestUtils.doEndTurn(bob.game, bob.ident.pkey);
      equal(bob.game.currentPhase(), "awaitingDraw");
      var waiter = new Waiter(callback);
      waiter.waitForNotification(bob.game.actionSucceeded);
    }),
    exceptional(function (action, callback) {
      equal(action.verb, "update");
      equal(bob.game.players()[0].tiles().length, 6);
      equal(bob.game.players()[0].tiles()[5], action.arguments.info.drawnTile);
      equal(bob.game.currentPhase(), "placing");

      // todo - finish fleshing out test

      callback(null);
    })
    ], function (err, results) {
      equal(err, null, "finished");
      start();
    });
  });
});


require([
  "testutils/TestIO",
  "server/src/MagnateServer",
  "testutils/TestDb",
  "testlib/async",
  "testutils/debugIdentities"
], function (TestIO, MagnateServer, TestDb, async, debugIdentities) {
  
  QUnit.module("IO");
  
  
  asyncTest("IO test", function () {
    var io = new TestIO();
    var server = new MagnateServer(io, TestDb);

    var socket = io.connect("server");

    var user = debugIdentities.getIdentity(0);

    socket.emit("user-signin", user);
    
    async.waterfall([
      function (cb) {
        socket.emit("get-pending-games", cb);
      },
      function (myGames, otherGames, cb) {
        cb(null);
      }
    ], function (err, results) {
      equal(err, null);
      start();
    });
  });

});
