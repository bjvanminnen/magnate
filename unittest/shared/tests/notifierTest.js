require([
  'shared/notifier',
  'shared/exception',
  'shared/utils'
], function (Notifier, Exception) {
  QUnit.module("notifier tests");
  
  var Foo = function () {
    var foo = {};
    var x = 0;
    
    foo.getX = function () {
      return x;
    }

    foo.xChanged = new Notifier(foo);

    foo.incrementX = function () {
      x += 1;
      foo.xChanged.notify({value: x});
    }
    return foo;
  };

  asyncTest("simple notifier", function () {
    var foo = new Foo();

    equal(foo.getX(), 0);

    var listenersLeft = 2;
    var oneCalled = false;
    var twoCalled = false;

    var resume = function() {
      start();
      ok(oneCalled, "Called the first listener");
      ok(twoCalled, "Called the second listener");
    }

    foo.xChanged.subscribe(function (source, properties) {
      ok(true, "first listener called");
      equal(source, foo);
      equal(properties.value, 1);
      equal(this, window);
      listenersLeft--;
      oneCalled = true;
      if (listenersLeft === 0) {
        resume();
      }
    });

    foo.xChanged.subscribe(function (source, properties) {
      ok(true, "second listener called");
      equal(source, foo);
      equal(properties.value, 1);
      equal(this, foo);
      listenersLeft--;
      twoCalled = true;
      if (listenersLeft === 0) {
        resume();
      }
    }, foo);

    equal(foo.xChanged._listeners.length, 2, "We have 2 listeners");    
    
    foo.incrementX();    
  });

  test("unsubscribing", function () {
    var notifier = new Notifier(this);
    var subscriber1 = function () { };
    var subscriber2 = function () { };

    notifier.subscribe(subscriber1);
    notifier.subscribe(subscriber2);

    equal(notifier._listeners.length, 2, "there are two subscribers");
    equal(notifier._listeners[0].fn, subscriber1);
    equal(notifier._listeners[1].fn, subscriber2);

    notifier.unsubscribe(subscriber1);
    equal(notifier._listeners.length, 1, "one subscriber left");
    equal(notifier._listeners[0].fn, subscriber2);
  });

  asyncTest("creation notification", function () {
    var name = "TestException";
    var message = "being thrown";

    function exceptionWatcher(source, exception) {
      Exception.onCreate.unsubscribe(exceptionWatcher);
      start();
      equal(exception.name, name, "validate name");
      equal(exception.message, message, "validate message");
    }

    function thrower() {
      throw new Exception(name, message);
    }

    Exception.onCreate.subscribe(exceptionWatcher);
    assertThrows(thrower, new RegExp(name), "make sure we throw");
    expect(3);
  });

  test("nested notifications", function () {
    var grandParent = new Notifier(this);
    var parent1 = new Notifier(this);
    
    var log = [];    

    grandParent.subscribe(function () {
      log.push("parent1");
      parent1.notify();
    });

    grandParent.subscribe(function () {
      log.push("parent2");
    });

    parent1.subscribe(function () {
      log.push("child");
    });

    log.push("grandParent");
    grandParent.notify();

    deepEqual(log, ["grandParent", "parent1", "parent2", "child"]);


  });

});