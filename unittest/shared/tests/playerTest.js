require([
  "model/game",
  "model/player",
  "model/tilebag",
  "testutils/TestUtils"
], function (Game, Player, TileBag, TestUtils) {
  QUnit.module("player tests");

  var MAX_TILES = 6;

  test("getters", function () {
    var player = new Player("Bob", "123abc");

    equal(player.name(), "Bob");
    equal(player.key(), "123abc");
    equal(player.money(), 6000);

  });

  test("add/remove money", function () {
    var player = new Player("Bob", 0);

    var startMoney = player.money();
    equal(startMoney, 6000);
    player.addMoney(1000);
    equal(player.money(), startMoney + 1000);
    player.subtractMoney(2000);
    equal(player.money(), startMoney + 1000 - 2000);
    assertThrows(function () {
      player.subtractMoney(player.money() + 1000); 
    }, /InvalidOperation/);
  });
  
  test("add/remove stocks", function () {
    var player = new Player("Bob", 0);
    ok(player.addStock(Game.HOTELS[0], 3),
      "Give 3 Hyatt");
    equal(player.stockCount(Game.HOTELS[0]), 3,
      "Make sure they have the right amount of Hyatt");
    equal(player.stockCount(Game.HOTELS[1]), 0,
      "Player has no Holiday yet");
    ok(player.removeStock(Game.HOTELS[0], 1),
      "Able to remove 1 of 3 Hyatt");
    equal(player.stockCount(Game.HOTELS[0]), 2,
      "Still have 2 Hyatt");
    equal(player.removeStock(Game.HOTELS[0], 3), false,
      "Unable to remove more of a stock than we have");
    equal(player.removeStock(Game.HOTELS[1], 1), false,
      "Unable to remove a stock we don't have");

    ok(player.addStock(Game.HOTELS[1]), "add stock with no amount arg");
    equal(player.stockCount(Game.HOTELS[1]), 1);
  });

  test("add/remove tiles", function () {
    var player = new Player("Bob", 0);
    var tilebag = new TileBag();

    var tile = tilebag.getTile();
    equal(player.hasTile(tile), false, "Player doesn't have any tiles yet");
    ok(player.addTile(tile));
    equal(player.hasTile(tile), true, "Player has the tile we just added");
    ok(player.removeTile(tile));
    equal(player._tiles.length, 0, "Player has no tiles again");
    equal(player.hasTile(tile), false, "Player doesn't have tile we removed");

    assertThrows(function () { player.removeTile(tile); },
      /InvalidOperation/, "Throw when trying to remove a tile we don't have");

    for (var i = 0; i < MAX_TILES; i++) {
      player.addTile(tilebag.getTile());
    }
    assertThrows(function () { player.addTile(tilebag.getTile); },
      /InvalidOperation/, "Throw when trying to add too many tiles"); 
  });

  test("serialize/deserialize", function () {
    var player1 = new Player("Bob", 0);

    // modify some values
    player1.addMoney(2000);
    player1.addStock(Game.HOTELS[0], 1);
    player1.addStock(Game.HOTELS[0], 1);
    player1.addStock(Game.HOTELS[1], 1);
    player1.addTile("1A");
    player1.addTile("3B");
    player1.addTile("4A");

    // create a clone
    var player2 = TestUtils.clone(player1);
  });

  test("privatize", function () {
    var player1 = new Player("Bob", 0);
    var marker = "PM";

    // modify some values
    player1.addMoney(2000);
    player1.addStock(Game.HOTELS[0], 1);
    player1.addStock(Game.HOTELS[0], 1);
    player1.addStock(Game.HOTELS[1], 1);
    player1.addTile("1A");
    player1.addTile("3B");
    player1.addTile("4A");

    var priv1 = player1.privatized(marker, false);
    var priv2 = player1.privatized(marker, true);

    ok(_.every(priv1._tiles), function (tile) {
      return tile === marker;
    });
    ok(_.every(priv2._tiles), function (tile) {
      return tile === marker;
    });

    equal(priv1._money, 8000);
    equal(priv2._money, marker);
    
    // todo : figure out stock story

  });
});