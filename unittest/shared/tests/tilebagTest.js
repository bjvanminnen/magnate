require(["model/tilebag", "testutils/TestUtils"], function (TileBag, TestUtils) {

  QUnit.module("tilebag Test");

  test("create a small tilebag", function () {
    var tilebag = new TileBag("B", 2);

    ok(_.contains(tilebag._tiles, "1A"));
    ok(_.contains(tilebag._tiles, "2A"));
    ok(_.contains(tilebag._tiles, "1B"));
    ok(_.contains(tilebag._tiles, "2B"));

    equal(tilebag._tiles.length, 4);

    var tile = tilebag.getTile();
    equal(tilebag._tiles.length, 3);

    var tile2 = tilebag.getTile();
    var tile3 = tilebag.getTile();
    var tile4 = tilebag.getTile();

    equal(tilebag._tiles.length, 0);

    equal(tilebag.getTile(), TileBag.EmptyMarker);
    equal(tilebag.getTile(), TileBag.EmptyMarker);
  });

  test("create a default tilebag", function () {
    var tilebag = new TileBag();

    for (var i = 0; i < 12 * 9; i++) {
      tilebag.getTile();
    }

    equal(tilebag._tiles.length, 0);
  });

  test("serialize/deserialize", function () {
    var bag1 = new TileBag();
    var bag2 = TestUtils.clone(bag1);
  });

  test("privatize", function () {
    var tilebag = new TileBag();
    var marker = "PM";

    var privateBag = tilebag.privatized(marker);
    equal(privateBag._tiles.length, tilebag._tiles.length);
    ok(_.every(privateBag._tiles, function (tile) {
      return tile === marker;
    }));
  });
});