require([
  "testutils/Waiter",
  "shared/notifier",
  "testlib/async",
  "testutils/SocketSim"
], function (Waiter, Notifier, async, SocketSim) {

  QUnit.module("utils tests");

  asyncTest("Waiter test", function () {    
    var firstNotifier = new Notifier(this, "notifier one");
    var secondNotifier = new Notifier(this, "notifier two");

    async.waterfall([
      function (callback) {
        // wait on a single notifier with no id, and no contents
        var waiter = new Waiter(callback);
        waiter.waitForNotification(firstNotifier);
        firstNotifier.notify();
      },
      function (callback) {        
        // wait on two different notifications, one of which fires twice
        var waiter = new Waiter(callback);        
        waiter.waitForNotification(firstNotifier, "first", 2);
        waiter.waitForNotification(secondNotifier, "second");
        firstNotifier.notify("one");
        secondNotifier.notify("two");
        firstNotifier.notify("three");
      },
      function (results, callback) {
        ok(_.contains(results["first"], "one"));
        ok(_.contains(results["first"], "three"));
        equal(results["second"], "two");
        callback(null);
      }
    ], function (err, results) {
      start(); // waterfall start
      ok("done");
    });
  });

  asyncTest("SocketSim test", function () {
    var socketSim = new SocketSim();

    function ServerSim(socket) {
      var count = 0;
      var self = this;

      socket.on("increment", function (amount) {
        count += amount;
        socket.emit("did-increment", count);
        self.onIncrement.notify();  
      });

      socket.on("multiply", function (amount) {
        count *= amount;
        socket.emit("did-multiply", count);
      });

      this.count = function () {
        return count;
      }
      this.onIncrement = new Notifier(this);
      this.onMultiply = new Notifier(this);
    }

    function ClientSim(socket) {
      var self = this;

      socket.on("did-increment", function (count) {
        self.onDidIncrement.notify(count);
      });

      socket.on("did-multiply", function (count) {

      });

      this.increment = function (amount) {
        socket.emit("increment", amount);
      };

      this.onDidIncrement = new Notifier(this);
    }

    var server = new ServerSim(socketSim.server);
    var client = new ClientSim(socketSim.client);
    
    async.waterfall([
      function (callback) {
        var waiter = new Waiter(callback);
        waiter.waitForNotification(server.onIncrement);
        client.increment(1);
      },
      function (callback) {
        var waiter = new Waiter(callback);
        waiter.waitForNotification(client.onDidIncrement);
      },
      function (result, callback) {
        equal(result, 1);
        callback(null, null);
      }
    ], function (err, results) {
      start();
    });
  });

  asyncTest("SocketSim rooms", function () {
    var socketSim1 = new SocketSim();
    var socketSim2 = new SocketSim();
    socketSim1.server.join("SomeRoom");
    socketSim2.server.join("SomeRoom");

    var onBroadCast = new Notifier(this);
    socketSim1.client.on("bcast", function (msg) {
      ok(false, "sending socket pair shouldnt receive broadcast");
    });
    socketSim2.client.on("bcast", function (msg) {
      onBroadCast.notify(msg);
    });

    async.waterfall([
      function (callback) {
        var waiter = new Waiter(callback);
        waiter.waitForNotification(onBroadCast);
        socketSim1.server.broadcast.to("SomeRoom").emit("bcast", 12345);
      },
      function (result, callback) {
        equal(result, 12345);
        start();
      }
    ]);
  });
});