var isNode = (typeof exports !== "undefined");
console.log("isNode = " + isNode);

if (isNode) {
  // todo - smarter path
  var node_modules = process.env.HOME + "/magnate/server/node_modules";
  var requirejs = require(node_modules + "/requirejs");
  global._ = require(node_modules + "/underscore");

  requirejs.config({
    baseUrl: process.env.HOME + "/magnate"
  });
  
  exports.require = setRequirements;
  exports.add = add;
  exports.addAsync = addAsync;
  exports.exportTo = exportTo;
} else {
  define(function () {
    return {
      require: setRequirements,
      add: add,
      addAsync: addAsync,
      runTests: runTests
    }
  });
}

var tests = {};
var testRequirements = [];

function setRequirements(requirements) {
  testRequirements = requirements;
}

function add(name, fn) {
  if (tests[name]) {
    throw name + " already exists as a test";
  }
  tests[name] = function (mod) {
    var myRequire = isNode ? requirejs : require;
    if (!isNode) {
      mod = QUnit;
      mod.stop();
    }

   myRequire(["requirePaths"].concat(testRequirements), function () {
      try {
        fn.apply(this, [mod].concat(Array.prototype.slice.call(arguments, 1)));
      } catch (exc) {
        mod.ok(false, "Exception: " + exc);
      } finally {
        if (isNode) {
          mod.done();
        } else {
          mod.start();
        }
      }
    });
  }
}

// todo - combine this with regular add?
function addAsync(name, fn) {
  if (tests[name]) {
    throw name + " already exists as a test";
  }
  tests[name] = function (mod) {
    var myRequire = isNode ? requirejs : require;
    if (!isNode) {
      mod = QUnit;
      mod.stop();
    }

    function finishTest() {
      if (isNode) {
        mod.done();
      } else {
        mode.start();
      }
    }
     
    myRequire(["requirePaths"].concat(testRequirements), function () {
      try {
        fn.apply(this, [mod].concat(Array.prototype.slice.call(arguments, 1)
          .concat(finishTest)));
      } catch (exc) {
        mod.ok(false, "Exception: " + exc);
        finishTest();
      }
    });
  }
}

function exportTo(mod) {
  for (var item in tests) {
    mod[item] = tests[item]
  }
}

function runTests() {
  for (var item in tests) {
    QUnit.test(item, tests[item]);
  }
}
