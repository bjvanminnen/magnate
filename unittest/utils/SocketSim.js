define(["class", "shared/exception"],
function (Class, Exception) {
  var rooms = {}; // todo - should arguably be on IO?
  
  var channelId = 0;
  var Channel = Class.extend({
    init: function () {
      var self = this;
      this._listeners = [];
      this._pair = null;
      this.id = channelId++;

      this.broadcast = {
        to: function (name) {
          return self._broadcastTo(name);
        }
      };
    },

    setPair: function (channel) {
      this._pair = channel;
    },

    on: function (event, fn) {
      this._listeners.push({
        event: event,
        fn: fn
      });
    },

    emit: function (event, args) {
      var self = this;
      var channel = this._pair;
      for (var i = 0; i < channel._listeners.length; i++) {
        if (channel._listeners[i].event === event) {
          var fnArgs = Array.prototype.slice.call(arguments, 1);
          setTimeout(function (index, fnArgs) {
            channel._listeners[index].fn.apply(self, fnArgs);
          }, 100, i, fnArgs);
        }
      }
    },

    join: function (name) {
      if (!rooms[name]) {
        rooms[name] = {
          channels: []
        };
      }

      if (_.contains(rooms[name].channels, this)) {
        return;
      }

      rooms[name].channels.push(this);
    },

    _broadcastTo: function (name) {
      var self = this;
      return {
        emit: function (event, args) {
          if (!rooms[name]) {
            return;
          }
          for (var i = 0; i < rooms[name].channels.length; i++) {
            var channel = rooms[name].channels[i];
            if (channel !== self) {
              channel.emit.apply(channel, arguments);
            }
          }
        }
      }
    }
  });
  
  /**
   * A class meant to simulate a socket connection.  This consists of two
   * channels - one to send messages from the server (to the client), the
   * other to send messages from the client (to the server).
   */ 
  var SocketSim = Class.extend({
    init: function () {
      this.client = new Channel();
      this.server = new Channel();

      this.client.setPair(this.server);
      this.server.setPair(this.client);
    }
  });

  return SocketSim;
});