// todo - rename file?

define([
  "class",
  "model/game",
  "shared/exception",
  "testutils/TestUtils"
], function (Class, Game, Exception, TestUtils) {
  
  var ObjectID = Class.extend({
    init: function (string) {
      if (string) {
        if (string.length !== 24) {
          throw new Exception("InvalidArgument",
            "ObjectID requires 24 byte hex string");
        }
        this._id = string;
      } else {
        this._id = (Math.random().toString(16)+'000000000000').slice(2, 12+2);
      }
    },

    toString: function () {
      return this._id;
    }
  });


  // todo - we reference via array index.  i wonder if i should validate this
  // matches item._id
  var TestDb = Class.extend({
    init: function (db, collection) {
      this.itemList = {};
      this.pending = [];

      // we dont actually use these, but have this to make it more similar to 
      // our GameStore
      if (!_.isString(db)) {
        throw new Exception("InvalidArgument", "TestDb requires db name");
      }
      if (!_.isString(collection)) {
        throw new Exception("InvalidArgument", "TestDb requires collection name");
      }
    },

    /**
     */
    _empty: function (callback) {
      var numRemoved = _.pairs(this.itemList).length;
      this.itemList = {};

      // The node version of this seems to callback with numRemoved only if the
      // write concern is set.  For now, I'm going to always behave that way
      // but if I want to be able to also simulate a store without the write
      // concern set, I will need to have this configurable.
      callback(null, numRemoved);
    },

    /**
     * Asychronously get an item from our db.
     *
     * @param {number} id The game id
     * @param {function(err, result)} callback A callback for handling errors/results
     */
    getItemInfo: function (id, callback) {
      var self = this;
      // simulate async db operation
      setTimeout(function () {
        if (!self.itemList[id]) {
          callback("NoItem", null);
        } else {
          callback(null, self.itemList[id]);
        }
      }, 10);
    },

    /**
     * Update the db with the new game model.
     *
     * @param {number} id The item id
     * @param {number} expectedWaterline The waterline we got back from our call
     *   to getItemInfo.  If it doesn't match current waterline, update will fail
     * @param {string} type The new documetn type, or null if no change.
     * @param {string} data Json string of data to be stored
     * @param {function} callback A callback for handling errors/results
     * @returns via callback (err, numUpdated, ?)
     */
    updateItem: function (id, expectedWaterline, type, data, callback) {
      var self = this;
      if (_.isObject(data)) {
        throw new Exception("InvalidArgument", "updateItem expects json");
      }

      // todo - handle unknown ids

      // asynchronous, since we do a db query to get waterline
      // i suspect there's a way in mongodb to do this in a single statement (ie.
      // commit iff waterline is as expected)
      setTimeout(function () {
        if (expectedWaterline != self.itemList[id].waterline) {
          callback("waterline moved", null);
        }

        // do our update
        self.itemList[id].data = data;
        if (type) {
          self.itemList[id].type = type;
        }
        self.itemList[id].waterline = expectedWaterline + 1;

        // another case where the callback returns data only if the write concern is set
        // i'm not entirely certain what the last item of the callback is (returning null
        // for now)
        callback(null, 1, null);
      }, 10);
    },

    /**
     * Adds an item to the db.
     *
     * @param {string} type Type of data stored
     * @param {string} data Json string of data to be stored
     * @param {function(err, itemId)} callback
     */
    addItem: function (type, data, callback) {
      var self = this;
      if (_.isObject(data)) {
        throw new Exception("InvalidArgument", "addItem expects json");
      }

      if (!callback) {
        throw new Exception("InvalidArgument", "addItem requires callback");
      }

      // simulate async
      setTimeout(function () {
        var id = new ObjectID();

        self.itemList[id] = {
          _id: id,
          waterline: 1,
          type: type,
          data: data
        };

        callback(null, id);
      }, 10);
    },

    /**
     * todo
     */
    getAllItems: function (callback) {
      callback(null, _.values(this.itemList));
    },

    ItemType: {
      Pending: "pending-game",
      Running: "running-game"
    }
  });

  return TestDb;
});
