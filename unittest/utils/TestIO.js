define([
  "class",
  "shared/exception",
  "testutils/SocketSim"
], function (Class, Exception, SocketSim) {
  var TestIO = Class.extend({
    init: function () {
      var self = this;
      this._onConnection = null;

      this.sockets = {
        on: function (event, callback) {
          return self._socketsOn(event, callback);
        }
      };
    },

    // we do it this way so that this points at TestIO instead of a sockets
    // child of TestIO
    _socketsOn: function (event, callback) {
      if (event !== "connection") {
        throw new Exception("InvalidOperation", "Not implemented");
      }
        
      this._onConnection = callback;
    },

    connect: function (serverName) {
      if (this._onConnection === null) {
        throw new Exception("InvalidOperation",
          "Must first create a test server to connect to");
      }

      var socket = new SocketSim();
      this._onConnection(socket.server);
      return socket.client;
    }
  });

  return TestIO;
});