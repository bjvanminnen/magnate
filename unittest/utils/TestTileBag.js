define(["model/tilebag", "shared/exception"],
function (TileBag, Exception) {
  var fromJSON = false;

  /**
   * A tilebag that allows you to make selections non-random.  For example,
   * before a 4-player game started, you could queue up the 24 tiles you 
   * want the players to start with.
   */
  var TestTileBag = TileBag.extend({
    init: function () {
      if (fromJSON) {
        return;
      }

      this._super.apply(this, arguments);
    },

    /**
     * Queue up a list of tiles.  Tiles in the queue are return before going to
     * the super tilebag.  A queued tile of ?? will return a tile from the super
     * tile bag.
     *
     * @param {string[]} tiles The list of tiles to queue up
     * @param {boolean} frontload Whether we should add to front (vs end) of queue
     */
    enqueue: function (tiles, frontload) {
      this._queue = this._queue || [];
      
      // Can only enqueue tiles that are still in the bag
      for (var i = 0; i < tiles.length; i++) {
        if (tiles[i] === "??") {
          continue;
        }
        var indexOf = _.indexOf(this._tiles, tiles[i]);
        if (indexOf === -1) {
          throw new Exception("TestTileBagError", "Tile not in bag (" + tiles[i] + ")");
        }
        // remove tile from bag
        this._tiles.splice(indexOf, 1);
      }

      if (frontload) {
        // add tiles to front
        this._queue = tiles.concat(this._queue);
      } else {
        // add to back
        this._queue = this._queue.concat(tiles);
      }
    },

    /**
     * Gets a tile from the queue if we have one, otherwise calls parent for 
     * random tile.
     */
    getTile: function () {
      if (this._queue && this._queue.length > 0) {
        var next = this._queue.splice(0,1)[0];
        if (next !== "??") {
          return next;
        }
      }

      return this._super();
    }
  });

  /**
    * Create a TestTileBag object from its json representation.
    *
    * @param {string} json The json to create from
    * @returns {TileBag}
    */
  TestTileBag.createFromJSON = function (json) {
    fromJSON = true;
    var tilebag = new TestTileBag();
    fromJSON = false;
    var obj = JSON.parse(json);
    _.extend(tilebag, obj);
    return tilebag;
  };

  return TestTileBag;
});