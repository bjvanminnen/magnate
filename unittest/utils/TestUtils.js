define([
  "model/game",
  "testutils/TestTileBag",
  "shared/exception"
], function (Game, TestTileBag, Exception) {
  
  var TestUtils = {};

  /**
   * Do a placeTile action on game
   *
   * @returns undefined
   */
  TestUtils.doPlaceTile = function (game, tile, pkey) {
    pkey = pkey || game.currentPlayer().key();
    var legalPlacement = game.legalPlacement(tile);
    if (tile === "None") {
      legalPlacement = true;
    }

    var result;
    try {
      result = game.performAction(Game.placeAction(pkey, tile));
    } catch (exc) {
      if (exc.name === "IllegalPlacement") {
        equal(legalPlacement, false,
          "placeTile failed.  legal placement should agree");
      }
      throw exc;
    }

    // place tile succeeded. legalPlacement should agree
    equal(legalPlacement, true);
  };

  /**
   * Do a merge action on game
   *
   * @returns undefined
   */
  TestUtils.doMerge = function (game, buyer, seller, pkey) {
    pkey = pkey || game.currentPlayer().key();
    game.performAction(Game.mergeAction(pkey, buyer, seller));
  };

  /**
   * Do a resolveMerge action on game
   *
   * @returns undefined
   */
  TestUtils.doResolveMerge = function (game, hold, sell, tradein, pkey) {
    if (pkey === undefined) {
      throw new Exception("require pkey for doResolveMerge");
    }
    game.performAction(Game.resolveMergeAction(pkey, hold, sell, tradein));
  };

  /**
   * Do a foundChain action on game
   *
   * @returns undefined
   */
  TestUtils.doFoundChain = function (game, hotel, pkey) {
    pkey = pkey || game.currentPlayer().key();
    game.performAction(Game.foundAction(pkey, hotel));
  };

  /**
   * Do a buy action on game
   *
   * @returns undefined
   */
  TestUtils.doBuy = function (game, hotel, pkey) {
    pkey = pkey || game.currentPlayer().key();
    game.performAction(Game.buyAction(pkey, hotel));
  };

  /**
   * Do a swap action on game
   *
   * @returns undefined
   */
  TestUtils.doSwapTiles = function (game, pkey) {
    pkey = pkey || game.currentPlayer().key();
    game.performAction(Game.swapAction(pkey));
  };

  /**
   * Do an endTurn action
   *
   * @returns undefined
   */
  TestUtils.doEndTurn = function (game, pkey) {
    pkey = pkey || game.currentPlayer().key();
    game.performAction(Game.endTurnAction(pkey));
  };

  /**
   * Do an endGame action
   *
   * @returns undefined
   */
  TestUtils.doEndGame = function (game, pkey) {
    pkey = pkey || game.currentPlayer().key();
    game.performAction(Game.endGameAction(pkey));
  };

  /**
  * Given a game model, hacks it so that the current player has the specified tile.
  * Throws if the tile is on the board already, in which case we can't do anything
  *
  * @param {Game} game The game model
  * @param {String} tile The tile the player should have
  * @returns {boolean} True if successfully swapped
  */
  TestUtils.ensureCurrentPlayerHasTile = function (game, tile) {
    if (game === undefined || tile === undefined) {
      throw new Exception("BadArgument", "BadArgument given to ensureCurrentPlayerHasTile");
    }
    
    // Is it on the board?
    if (game._board.tileState(tile) !== "none") {
      throw new Exception("InvalidOperation", "Tile is already on the board");
    }

    // Do we already have it?  Let's just move it around to ensure it's
    // the first tile
    var currentPlayer = game.currentPlayer();
    var currentPlayerTiles = currentPlayer.tiles();
    for (var j = 0; j < currentPlayerTiles.length; j++) {
      if (currentPlayerTiles[j] === tile) {
        currentPlayerTiles[j] = currentPlayerTiles[0];
        currentPlayerTiles[0] = tile;
      }
    }      

    // Is it in the bag?
    if (_.contains(game._tilebag._tiles, tile)) {
      // swap player's first tile with this one
      game._tilebag._tiles.splice(_.indexOf(game._tilebag._tiles, tile), 1);
      game._tilebag._tiles.push(currentPlayerTiles[0]);
      currentPlayerTiles[0] = tile;
      return true;
    }

    // Another player must have it
    // use the private version in case game has privateInfo enabled
    var players = game._players; 
    for (var i = 0; i < players.length; i++) {
      for (var j = 0; j < players[i]._tiles.length; j++) {
        if (players[i]._tiles[j] === tile) {
          players[i]._tiles[j] = currentPlayerTiles[0];
          currentPlayerTiles[0] = tile;
          return true;
        }
      }
    }

    // Not sure what happened if we got here
    throw new Exception("Unexpected", "Bug in TestUtils.ensureCurrentPlayerHasTile");
    return false;
  }

  /**
  * Have the current player ensure that they have the provided tile, and then
  * place it on the board, with some asserts along the way.  Put in a single
  * function for simplification
  *
  * @param {Game} game The game model
  * @param {String} tile The tile to play
  */
  TestUtils.currentPlayerPlaceTile = function (game, tile) {
    this.ensureCurrentPlayerHasTile(game, tile);
    equal(game.currentPlayer().tiles()[0], tile);
    TestUtils.doPlaceTile(game, tile);
  }

  /**
   * Creates a deep clone of a model object, by converting to json and then
   * recreating the object via createFromJSON
   *
   * @returns Object
   */
  TestUtils.clone = function (obj) {
    var json = JSON.stringify(obj);
    var clone = obj.constructor.createFromJSON ?
      obj.constructor.createFromJSON(json) : JSON.parse(json);

    // todo - better way of doing this?
    if (obj instanceof Game) {
      // excplicitly clone tilebag, so that if we start with a test tilebag, 
      // we also end up with one
      clone._tilebag = TestUtils.clone(obj._tilebag)
    }

    if (typeof QUnit !== "undefined" && QUnit.config.current) {
      ok(obj !== clone && _.isEqual(obj, clone), "validate clone");
    }
    return clone;
  }

  /**
   * Tests whether a pair of lists contain the same set of objects.  Doesn't
   * care about order
   *
   * @returns {boolean}
   */
  TestUtils.listsEquivalent = function (list1, list2) {
    if (list1.length != list2.length) {
      return false;
    }
    return _.union(list1, list2).length === list1.length;
  }

  TestUtils.exceptional = function (fn) {
    return function () {
      try {
        fn.apply(this, arguments);
      } catch (exc) {
        ok(false, "Exception: " + exc);
        var callback = arguments[arguments.length - 1];
        callback(exc, null);
      }
    }
  }

  return TestUtils;
});
