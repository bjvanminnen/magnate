define(["class", "shared/notifier"],
function (Class, Notifier) {
  /**
   * A TestView that can simulate various view events as if the user was
   * causing them.
   */
  var TestView = Class.extend({
    init: function (game) {
      this.gridTileClicked = new Notifier(this, "TestView.gridTileClicked");
      this.playerTileClicked = new Notifier(this, "TestView.playerTileClicked");
      this.hotelClicked = new Notifier(this, "TestView.hotelClicked");
      this.buyDialogCompleted = new Notifier(this, "TestView.buyDialogCompleted");
      this.mergeSelectDialogCompleted = new Notifier(this,
        "TestView.mergeSelectDialogCompleted");
      this.mergeResolveDialogCompleted = new Notifier(this,
        "TestView.mergeResolveDialogCompleted");
      this.endTurnDialogCompleted = new Notifier(this,
        "TestView.endTurnDialogCompleted");
      this.endGameClicked = new Notifier(this, "TestView.endGameClicked");
      this.swapClicked = new Notifier(this, "TestView.swapClicked");

      this.didUpdate = new Notifier(this, "TestView.didUpdate");
      this.didShowPurchaseDialog = new Notifier(this, "TestView.didShowPurchaseDialog");
    },

    simulatePlayerTileClick: function (tile) {
      this.playerTileClicked.notify({tile: tile});
    },

    simulateBuyDialogCompleted: function (data) {
      this.buyDialogCompleted.notify({hotels: data});
    },

    simulateHotelClick: function (hotel) {
      this.hotelClicked.notify({hotel: hotel});
    },
    
    update: function () {
      this.updated = true;
      this.didUpdate.notify();
    },

    showPurchaseDialog: function () {
      this.showedPurchaseDialog = true;
      this.didShowPurchaseDialog.notify();
    },

    showMergeSelectDialog: function () {

    },

    showMergeResolveDialog: function () {

    },

    showEndTurnDialog: function () {

    },

    showGameOverDialog: function () {

    },

    endingGame: function () {

    }
  });

  return TestView;
});