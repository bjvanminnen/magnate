define(["class", "shared/exception"],
function (Class, Exception) {
  
  /**
   * A test utility class used to write asyncronous tests in qunit using the
   * async waterfall pattern.
   */
  var Waiter = Class.extend({

    /**
     * Create a Waiter, stopping the testrunner.  Test runner is resumed by
     * adding notifications to wait for using waitForNotification, and then
     * having those notificatons fire.
     */
    init: function (callback) {
      this._callback = callback;
      this._listenerCount = 0;
      this._results = {};
      this._finished = false;
      stop();

      // todo - should this be configurable?
      if (this._checkForFailure()) {
        this._finished = true;
        ok(false, "Waiter short circuiting due to existing failure");
        this._callback("Test failure", null);
        start();
      }
    },

    /**
     * If we already have a failure in this unit test, we don't want to wait
     * around for a notification that may not ever happen.  This allows us
     * to check if we have failed, in which case we don't wait around.
     */
    _checkForFailure: function () {
      return _.where(QUnit.config.current.assertions, {result: false}).length > 0;
    },

    /**
     * Add a notifier to the list of those we want to wait for.  Once all
     * notifiers that we are waiting on have fired, we will resume the
     * testrunner.
     *
     * @param {Notifier} notifier The notifier we're waiting on
     * @param {string} id Optional string id.  Necessary if waiting on multiple
     *   notifiers
     * @param {number} times Optional.  Don't start until notifier has notified
     *   this number of times.
     */
    waitForNotification: function (notifier, id, times) {
      var self = this;
      if (this._finished) {
        return;
      }

      if (this._checkForFailure()) {
        // todo - unsubscribe listeners?
        if (this._listenerCount > 0) {
          throw new Exception("NotYetImplemented");
        }

        ok(false, "Waiter short circuiting due to existing failure");
        this._callback("Test failure", null);
        start();
        return;
      }

      if (id === undefined) {
        id = "anon";
      }

      if (self._results[id]) {
        throw new Exception("DuplicateId", "id " + id + " already exists");
      }
      
      self._results[id] = [];

      function generator () {
        var count = times || 1;
        var singleCount = count === 1;
        
        return function onNotification(source, data) {
          count -= 1;
          if (singleCount) {
            self._results[id] = data;
          } else {
            self._results[id].push(data);
          }
          if (count === 0) {
            notifier.unsubscribe(onNotification);
            self._listenerCount -= 1;
            if (self._listenerCount === 0) {
              start();
              // if we have a single notifier, and a single count we return the single result
              // if we have a single notifier, and multiple counts we return an array of results
              // if we have multiple notifiers, we return an object of arrays of results
              if (_.keys(self._results).length === 1) {
                if (data) {
                  self._callback(null, self._results[id]);
                } else {
                  self._callback(null);
                }
              } else {
                self._callback(null, self._results);
              }
            }
          }
        }
      }
      self._listenerCount += 1;
      notifier.subscribe(generator());
    }
  });

  return Waiter;
});