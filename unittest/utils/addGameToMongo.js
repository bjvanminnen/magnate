#!/usr/bin/env node

// todo - hacky where these live
var MODULES = process.env.HOME + "/magnate/server/node_modules/";
var requirejs = require(MODULES + "requirejs");

// todo - should really probably have this in requirePaths or something
global._ = require(MODULES + "underscore");

requirejs.config({
  baseUrl: process.env.HOME + "/magnate"
});
requirejs(process.env.HOME + "/magnate/requirePaths");

requirejs([
  "server/src/GameStore",
  "testutils/gameCreation"
], function (GameStore, gameCreation) {
  var store = new GameStore("magnate", "games");

  // todo - get this from command line
  var gameName = "endableGame";
  var game = gameCreation.createGame(gameName);

  store.addItem(store.ItemType.Running, JSON.stringify(game), function (err, result) {
    console.log("added gameId: " + result + " with err: " + err);
    process.exit();
  });

});
