// and now the stuff we want by name
define(function () {
  var debugGame = {
    created: false
  };

  // Read a page's GET URL variables and return them as an associative array.
  var urlvars = function ()
  {
    var vars = [], hash;
    var hashes = window.location.href.slice(
      window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  }();  

  var game;
  if (urlvars.game) {
    require([  
      'model/game',
      'view/domview',
      'controller/localController',
      'testutils/gameCreation',
      'text!templates/game.html'
    ], function (Game, DomView, LocalController, gameCreation, html) {
      $("body").empty();
      $("body").append(html);

      game = gameCreation.createGame(urlvars.game);
      debugGame.created = true;
        
      // todo - should i always have view creation in controller? probably
      var view = new DomView(game); 
      var controller = new LocalController(game, view);

      // todo: this is a hack so that i can access game globally for debugging
      // purposes.  it should be removed
      window.__Game = Game;
      window.__game = game;
      window.__view = view;
      window.__controller = controller;
    });
  }

  return debugGame;
});