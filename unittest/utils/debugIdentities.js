define([
  "model/game",
  "testutils/TestUtils",
  "testutils/TestTileBag",
  "shared/exception",
  "sha512",
], function (Game, TestUtils, TestTileBag, Exception, jsSHA) {
  
  var debugIdentityList = [
    {name: "Bob", id: "00000000"},
    {name: "Mary", id: "11111111"},
    {name: "Abdullah", id: "22222222"},
    {name: "Michaelangelo", id: "33333333"},
    {name: "MrPoopyPants", id: "44444444"},
    {name: "Randy", id: "55555555"}
  ];

  // set pkeys to hash
  _.each(debugIdentityList, function (ident) {
    var shaObj = new jsSHA(ident.id, "TEXT");
    var hash = shaObj.getHash("SHA-512", "HEX");
    ident.pkey = hash;
  });

  return {
    getIdentity: function (index) {
      return debugIdentityList[index];
    }
  }
});
