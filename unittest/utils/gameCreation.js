define([
  "model/game",
  "testutils/TestUtils",
  "testutils/TestTileBag",
  "shared/exception",
  "testutils/debugIdentities"
], function (Game, TestUtils, TestTileBag, Exception, debugIdentities) {
  
  if (typeof equal === "undefined") {
    equal = function () { }; // noop
  }

  var gameCreation = {}

  gameCreation.allTiles = [];
    for (var letter = 'A'; letter <= 'I'; letter = letter.increment(1)) {
      for (var number = 1; number <= 12; number++) {
        gameCreation.allTiles.push(number + letter);
      }
    }

  gameCreation.library = {};

  gameCreation.library["swappable"] = {
      hotels: [
        {
          hotel: Game.HOTELS[0],
          tiles: ["1A", "2A", "3A", "4A", "5A", "6A", "7A", "8A", "9A", "10A", "11A", "12A"]
        },
        {
          hotel: Game.HOTELS[1],
          tiles: ["1C", "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "11C", "12C"]
        },
      ],
      players: [
        debugIdentities.getIdentity(0),
        debugIdentities.getIdentity(1)
      ],
      otherTilesInHand: ["12I", "1B", "2B", "3B", "4B", "5B", "6B", "7B", "8B", "9B", "10B", "11B"]
    };

  /**
   * Game with three hotels, each of length 11 thus safe
   */
  gameCreation.library["endableGame"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: gameCreation.allTiles.slice(0, 11)
      },
      {
        hotel: Game.HOTELS[1],
        tiles: gameCreation.allTiles.slice(12 * 2, 12 * 2 + 11)
      },
      {
        hotel: Game.HOTELS[2],
        tiles: gameCreation.allTiles.slice(12 * 4, 12 * 4 + 11)
      }
    ],
    players: [
      debugIdentities.getIdentity(0),
      debugIdentities.getIdentity(1)
    ],
    otherTilesInHand: ["12I"],
  };

  /**
   * Two chains, one bigger than the other.  Merge tile in hand.
   */
  gameCreation.library["sevenChains"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1A", "2A"]
      },
      {
        hotel: Game.HOTELS[1],
        tiles: ["1C", "2C"]
      },
      {
        hotel: Game.HOTELS[2],
        tiles: ["1E", "2E"]
      },
      {
        hotel: Game.HOTELS[3],
        tiles: ["1G", "2G"]
      },
      {
        hotel: Game.HOTELS[4],
        tiles: ["1I", "2I"]
      },
      {
        hotel: Game.HOTELS[5],
        tiles: ["4A", "5A"]
      },
      {
        hotel: Game.HOTELS[6],
        tiles: ["4C", "5C"]
      },
    ],    
    players: ["Bob", "Mary", "George"],
    otherTilesPlayed: [],
    otherTilesInHand: ["4E", "5E"],
    purchases: [
      [],
      [],
      []
    ]
  };

  /**
   * Create a game with the following chains:
   * HOTELS[0]: 1A, 2A
   * HOTELS[1]: 1C, 2C, 3C
   * HOTELS[2]: 1E, 2E
   * HOTELS[3]: 1G, 2G, 3G, 4G, 5G
   * HOTELS[4]: none
   * HOTELS[5]: 1I, 2I
   * HOTELS[6]: none
   *
   * Players have the following stock (from founding):
   * Bob: HOTELS[2], HOTELS[3]
   * Mary: HOTELS[0], HOTELS[1], HOTELS[5]
   */
  gameCreation.library["fiveChains"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1A", "2A"]
      },
      {
        hotel: Game.HOTELS[1],
        tiles: ["1C", "2C", "3C"]
      },
      {
        hotel: Game.HOTELS[2],
        tiles: ["1E", "2E"]
      },
      {
        hotel: Game.HOTELS[3],
        tiles: ["1G", "2G", "3G", "4G", "5G"]
      },
      {
        hotel: Game.HOTELS[5],
        tiles: ["1I", "2I"]
      }
    ],
    players: ["Bob", "Mary"],
    otherTilesPlayed: [],
    otherTilesInHand: [],
    purchases: [
      [],
      []
    ],
    asserts: function (game) {
      equal(game.chainSize(Game.HOTELS[0]), 2);
      equal(game.chainSize(Game.HOTELS[1]), 3);
      equal(game.chainSize(Game.HOTELS[2]), 2);
      equal(game.chainSize(Game.HOTELS[3]), 5);
      equal(game.chainSize(Game.HOTELS[4]), 0);
      equal(game.chainSize(Game.HOTELS[5]), 2);
      equal(game.chainSize(Game.HOTELS[6]), 0);
      equal(game.players()[0].stockCount(Game.HOTELS[2]), 1);
      equal(game.players()[0].stockCount(Game.HOTELS[3]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[1]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[5]), 1);
    }
  };

  /**
  * Two chains, one bigger than the other.  Merge tile in hand.
  */
  gameCreation.library["merge32"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1A", "2A"]
      },
      {
        hotel: Game.HOTELS[3],
        tiles: ["1C", "2C", "3C"]
      }
    ],    
    players: ["Bob", "Mary", "George"],
    otherTilesPlayed: [],
    otherTilesInHand: ["2B"],
    purchases: [
      [Game.HOTELS[0], Game.HOTELS[0], Game.HOTELS[0]],
      [],
      []
    ]
  };

  /** 
   * Three hotels, each of size two.
   * HOTELS[0] is owned by all players
   * HOTELS[1] is owned by Bob and George
   * HOTELS[2] is owned by George and Mary
   * It's Mary's turn, and she has merge tile 3A
   */
  gameCreation.library["merge222"] = {
    hotels: [
      {
        // founded by Mary
        hotel: Game.HOTELS[0], 
        tiles: ["1A", "2A"]
      },
      {
        // founded by Bob
        hotel: Game.HOTELS[1],
        tiles: ["4A", "5A"]
      },
      {
        // founded by George
        hotel: Game.HOTELS[2],
        tiles: ["3B", "3C"]
      }
    ],
    players: ["Bob", "Mary", "George"],
    // extra tiles played so that mary has time to buy
    otherTilesPlayed: ["12I", "10I", "8I", "6I"],
    otherTilesInHand: ["3A"],
    purchases: [
      [Game.HOTELS[0]],
      [Game.HOTELS[2]],
      [Game.HOTELS[0], Game.HOTELS[1]]
    ],
    asserts: function (game) {
      equal(game.currentPlayer().name(), "Mary");
      equal(game.players()[0].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[0]), 1);

      equal(game.players()[0].stockCount(Game.HOTELS[1]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[1]), 0);
      equal(game.players()[2].stockCount(Game.HOTELS[1]), 1);

      equal(game.players()[0].stockCount(Game.HOTELS[2]), 0);
      equal(game.players()[1].stockCount(Game.HOTELS[2]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[2]), 1);
    }
  }

  /**
  * Three hotels of three different sizes
  * HOTELS[0]: 1A, 2A, 3A, 4A
  * HOTELS[1]: 2C, 3C, 4C
  * HOTELS[2]: 5B, 6B
  *
  * Players have the following stock (from founding):
  * Mary: HOTELS[0]
  * George: HOTELS[1], HOTELS[2]
  *
  * Bob also has tile 4B
  */
  gameCreation.library["merge432"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1A", "2A", "3A", "4A"]
      },
      {
        hotel: Game.HOTELS[1],
        tiles: ["2C", "3C", "4C"]
      },
      {
        hotel: Game.HOTELS[2],
        tiles: ["5B", "6B"]
      }
    ],
    players: ["Bob", "Mary", "George"],
    otherTilesPlayed: [],
    otherTilesInHand: ["4B"],
    purchases: [
      [],
      [],
      [],
    ],
    asserts: function (game) {
      equal(game.chainSize(Game.HOTELS[0]), 4);
      equal(game.chainSize(Game.HOTELS[1]), 3);
      equal(game.chainSize(Game.HOTELS[2]), 2);
      equal(game.players()[1].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[1]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[2]), 1);

      ok(_.contains(game.players()[0].tiles(), "4B"));
    }
  };

  /**
  * Four hotels, two different sizes
  * HOTELS[0]: 1C, 2C, 3C, 4C
  * HOTELS[1]: 5A, 5B
  * HOTELS[2]: 6C, 7C, 8C, 9C
  * HOTELS[3]: 5D, 5E
  *
  * Players have the following stock (from founding):
  * Bob: 
  * Mary: HOTELS[0], HOTELS[2]
  * George: HOTELS[1], HOTELS[3]
  *
  * Bob also has tile 5C
  */
  gameCreation.library["merge4422"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1C", "2C", "3C", "4C"]
      },
      {
        hotel: Game.HOTELS[1],
        tiles: ["5A", "5B"]
      },
      {
        hotel: Game.HOTELS[2],
        tiles: ["6C", "7C", "8C", "9C"]
      },
      {
        hotel: Game.HOTELS[3],
        tiles: ["5D", "5E"]
      }
    ],
    players: ["Bob", "Mary", "George"],
    otherTilesPlayed: ["12I", "10I", "8I"],
    otherTilesInHand: ["5C"],
    purchases: [
      [],
      [],
      [],
    ],
    asserts: function (game) {
      equal(game.chainSize(Game.HOTELS[0]), 4);
      equal(game.chainSize(Game.HOTELS[1]), 2);
      equal(game.chainSize(Game.HOTELS[2]), 4);
      equal(game.chainSize(Game.HOTELS[3]), 2);
      equal(game.players()[1].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[2]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[1]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[3]), 1);

      ok(_.contains(game.players()[0].tiles(), "5C"));
    }
  };

  /**
   * Game with two hotels, each of size 2.
   * Mary and George end up with 1 share of HOTELS[0].
   * Everyone ends up with 1 share of HOTELS[3].
   * It's Mary's turn and she has tile 2B.
   */
  gameCreation.library["merge22"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1A", "2A"]
      },
      {
        hotel: Game.HOTELS[3],
        tiles: ["1C", "2C"]
      }
    ],
    players: ["Bob", "Mary", "George"],
    // add a couple extra tiles so we have time to buy HOTELS[3]
    otherTilesPlayed: ["12I", "10I", "8I"],
    otherTilesInHand: ["2B"],
    purchases: [
      [],
      [Game.HOTELS[3]],
      [Game.HOTELS[0], Game.HOTELS[3]],
    ],
    asserts: function (game) {
      equal(game.chainSize(Game.HOTELS[0]), 2);
      equal(game.chainSize(Game.HOTELS[3]), 2);
      equal(game.players()[0].stockCount(Game.HOTELS[0]), 0);
      equal(game.players()[1].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[0]), 1);
      equal(game.players()[0].stockCount(Game.HOTELS[3]), 1);
      equal(game.players()[1].stockCount(Game.HOTELS[3]), 1);
      equal(game.players()[2].stockCount(Game.HOTELS[3]), 1);

      equal(game.currentPlayer().name(), "Mary");
      ok(_.contains(game.players()[1].tiles(), "2B"));
    }
  };

  /**
  * Potential merger for two chains that are both safe.
  */
  gameCreation.library["merge13_11"] = {
    hotels: [
      {
        hotel: Game.HOTELS[0],
        tiles: ["1A", "2A", "3A", "4A", "5A", "6A", "7A", "8A", "9A", "10A", "11A"]
      },
      {
        hotel: Game.HOTELS[3],
        tiles: ["1C", "2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "10C", "11C", "12C", "12D"]
      }
    ],
    players: ["Bob", "Mary", "George"],
    otherTilesPlayed: [],
    otherTilesInHand: ["11B"],
    purchases: [
      [],
      [],
      []
    ]
  };

  /**
   * No tiles played, all A's in hand.
   */
  gameCreation.library["AsInHand"] = {
    hotels: [
    ],
    players: ["Bob", "Mary", "George"],
    otherTilesPlayed: [],
    otherTilesInHand: ["1A", "2A", "3A", "4A", "5A", "6A", "7A", "8A", "9A",
      "10A", "11A", "12A"],
    purchases: [
      [],
      [],
      []
    ]
  };

  /**
   * No tiles played.  Bob has 1-6A, Mary has 1-6B.
   */
  gameCreation.library["AsAndBsInHand"] = {
    hotels: [],
    players: [
      debugIdentities.getIdentity(0),
      debugIdentities.getIdentity(1)
    ],
    otherTilesPlayed: [],
    otherTilesInHand: ["1A", "1B", "2A", "2B", "3A", "3B", "4A", "4B", "5A",
      "5B", "6A", "6B"],
    purchases: [
      [],
      [],
      []
    ]
  };

  // todo - do i need a way to allow you to queue up which tiles will be drawn next
  /**
   * Create a game.  Will place all tiles in the hotels, followed by
   * otherTilesPlayed.  We will then distribute otherTilesInHand to players
   * such that they could be played in order.
   */
  gameCreation.createGame = function (libraryName, options) {
    return gameCreation.createGameFromInputs(gameCreation.library[libraryName], options);
  };
    
  gameCreation.createGameFromInputs = function (inputs, options) {
    var asserts = inputs.asserts;
    
    // clone so that we dont have to worry about overwriting
    inputs = JSON.parse(JSON.stringify(inputs));

    inputs.otherTilesPlayed = inputs.otherTilesPlayed || [];
    inputs.otherTilesInHand = inputs.otherTilesInHand || [];
    if (!inputs.purchases) {
      inputs.purchases = [];
      for (var i = 0; i < inputs.players.length; i++) {
        inputs.purchases.push([]);
      }
    }

    var tilesToGive = _.flatten([_.pluck(inputs.hotels, "tiles"),
      inputs.otherTilesPlayed, inputs.otherTilesInHand]);

    var playerTiles = _.map(inputs.players, function (player) {
      return [];
    });

    for (var i = 0; i < tilesToGive.length; i++) {
      playerTiles[i % playerTiles.length].push(tilesToGive[i]);
    }
    
    // if players have fewer than 6 tiles, add random tiles
    for (var i = 0; i < playerTiles.length; i++) {
      while (playerTiles[i].length < 6) {
        playerTiles[i].push("??");
      }
    }

    // create our game
    var game;
    if (inputs.players[0].pkey === undefined) {
     game = gameCreation.createFromNames(inputs.players, options);
    } else {
      game = new Game(inputs.players, options);
    }
    game._tilebag = new TestTileBag();

    // add initial tiles to tile bag
    for (var i = 0; i < playerTiles.length; i++) {
      game._tilebag.enqueue(playerTiles[i].splice(0, 6));
    }

    // todo - make sure this bit gets tested
    var remainingTiles = _.flatten(_.zip.apply(this, playerTiles));
    remainingTiles = _.without(remainingTiles, undefined);
    if (remainingTiles.length > 0) {
      game._tilebag.enqueue(remainingTiles);
    }

    game.start();

    var tilesToPlace = _.flatten([_.pluck(inputs.hotels, "tiles"),
      inputs.otherTilesPlayed]);
    var hotelsToFound = _.pluck(inputs.hotels, "hotel");
    for (var i = 0; i < tilesToPlace.length; i++) {
      var pindex = i % inputs.players.length;      
      var pkey = game.players()[pindex].key();
      // place a tile
      TestUtils.doPlaceTile(game, tilesToPlace[i], pkey);
      // found a chain if necessary
      if (game.currentPhase() === "founding") {
        TestUtils.doFoundChain(game, hotelsToFound.splice(0, 1)[0], pkey);
      }
      // make some purchases
      for (var j = 0; j < 3; j++) {
        if (inputs.purchases[pindex].length !== 0) {
          var hotel = inputs.purchases[pindex][0];
          if (game.chainSize(hotel) > 0) {
            inputs.purchases[pindex].splice(0, 1);
            TestUtils.doBuy(game, hotel, pkey);
          }
        }
      }
      TestUtils.doEndTurn(game, pkey);
    }

    if (typeof QUnit !== "undefined") {
      for (var i = 0; i < inputs.hotels.length; i++) {
        var hotel = inputs.hotels[i];
        equal(game.chainSize(hotel.hotel), hotel.tiles.length,
          "Correct size for hotel " + hotel.hotel);

        _.each(hotel.tiles, function (tile) {
          equal(game.tileState(tile), hotel.hotel, hotel.hotel + " has tile " + tile);
        });
      }

      var playerIndex = game._currentPlayerIndex;
      for (var i = 0; i < inputs.otherTilesInHand.length; i++) {
        ok(_.contains(game.players()[playerIndex].tiles(),
          inputs.otherTilesInHand[i]));
        playerIndex += 1;
        playerIndex %= game.players().length;
      }

      // todo - assert for otherTilesPlayed?
      // todo - assert for purchases (also account for founding grants)
      
    }

    if (asserts && (typeof QUnit !== "undefined")) {
      asserts(game);
    }

    return game;
  };

  /**
   * todo
   */
  gameCreation.createFromNames = function (names, options) {
    var game = new Game(_.map(names, function (name, index) {
      return {
        name: name,
        pkey: index
      };
    }), options);
    return game;
  };

  return gameCreation;
});
