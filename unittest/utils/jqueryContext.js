/**
  * As long as context is attached, all calls to $ will use that context.
  */
  // todo - should i have an attach count to try and ensure that we always detach?
jQuery.attachContext = function (context) {
  jQuery.$$ = jQuery.$$ || $;
  jQuery.$$.ctx = context;
  $ = function (arg, ctx) {
    return jQuery.$$(arg, ctx || context);
  };
};

// todo - test detach
/**
  * Remove the attached context, returning jQuery to normal.
  */
jQuery.detachContext = function () {
  $ = jQuery.$$;
};